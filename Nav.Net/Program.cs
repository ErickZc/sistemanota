﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.WinForms;

namespace SistemaGestionNotas
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //var f = new WinForms.Grado.frmGrado();
            //var f = new WinForms.FrmMenuPrincipal("Administrador");
            var f = new WinForms.Login.FrmLogin();
            //var f = new WinForms.Administrador.FrmAdministrador();
            //var f = new WinForms.Periodo.FrmPeriodo();
            //var f = new WinForms.Docente.Grado_Docente();
            //var f = new WinForms.Materia.FrmMaterias_Profesor();
            //var f = new WinForms.Reporte.frmNotaPorMateria();

            //var f = new WinForms.Notas.FrmNotas();
            f.ShowDialog();
        }
    }
}

