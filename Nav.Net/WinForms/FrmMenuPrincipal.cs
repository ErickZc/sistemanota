﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.WinForms.Administrador;
using SistemaGestionNotas.WinForms.Docente;
using SistemaGestionNotas.WinForms.Estudiante;
using SistemaGestionNotas.WinForms.Grado;
using SistemaGestionNotas.WinForms.Login;
using SistemaGestionNotas.WinForms.Materia;
using SistemaGestionNotas.WinForms.Notas;
using SistemaGestionNotas.WinForms.Periodo;
using SistemaGestionNotas.WinForms.Secciones;
using SistemaGestionNotas.Model;
using SistemaGestionNotas.WinForms.Reporte;

namespace SistemaGestionNotas.WinForms
{
    public partial class FrmMenuPrincipal : Form
    {
        private string nivelAcceso { get; set; }
        bool docEncargado = false;
        bool docMateria = false;
        Docente_Grado gradoEncargado = new Docente_Grado();
        Docente_Materia_Grado materiaEncargado = new Docente_Materia_Grado();

        public FrmMenuPrincipal(string nivel)
        {
            InitializeComponent();

            //if (nivel.Equals("Administrador"))
            //{
            //
            //    //Mantenimiento
            //    modulosToolStripMenuItem.Enabled = true;
            //    modulosToolStripMenuItem.Visible = true;
            //
            //    modulosToolStripMenuItem1.Enabled = true;
            //    modulosToolStripMenuItem1.Visible = true;
            //
            //    reporteToolStripMenuItem.Enabled = true;
            //    reporteToolStripMenuItem.Visible = true;
            //}
            //else
            //{
            //    modulosToolStripMenuItem.Enabled = false;
            //    modulosToolStripMenuItem.Visible = false;
            //
            //    reporteToolStripMenuItem.Enabled = false;
            //    reporteToolStripMenuItem.Visible = false;
            //
            //    //Modulos
            //
            //    modulosToolStripMenuItem1.Enabled = true;
            //    modulosToolStripMenuItem1.Visible = true;
            //
            //    ingresarNotasToolStripMenuItem.Visible = true;
            //    ingresarNotasToolStripMenuItem.Enabled = true;
            //
            //    matricularAlumnosToolStripMenuItem.Enabled = true;
            //    matricularAlumnosToolStripMenuItem.Visible = true;
            //
            //    cambiarContraseToolStripMenuItem.Enabled = true;
            //    cambiarContraseToolStripMenuItem.Visible = true;
            //
            //    asignarDocenteResponsableAGradoToolStripMenuItem.Enabled = false;
            //    asignarDocenteResponsableAGradoToolStripMenuItem.Visible = false;
            //
            //    //docente
            //    asignarMateriasAImpartirALosDocentesToolStripMenuItem.Enabled = false;
            //    asignarMateriasAImpartirALosDocentesToolStripMenuItem.Visible = false;
            //
            //    cambiarContraseToolStripMenuItem.Enabled = false;
            //    cambiarContraseToolStripMenuItem.Visible = false;
            //}
        }
        public bool docenteEncargado(Docente_Grado encargado)
        {

            if (encargado.id_docente != null)
            {
                docEncargado = true;
                this.matricularAlumnosToolStripMenuItem.Visible = true;
                gradoEncargado = encargado;
            }
            else
            {
                docEncargado = false;
                this.matricularAlumnosToolStripMenuItem.Visible = false;
            }

            return docEncargado;

        }

        public bool docenteMateria(Docente_Materia_Grado docenteMateriaa)
        {

            if (docenteMateriaa.id_docente > 0)
            {
                docMateria = true;
                this.ingresarNotasToolStripMenuItem.Visible = true;
                materiaEncargado = docenteMateriaa;
            }
            else
            {
                docMateria = false;
                this.ingresarNotasToolStripMenuItem.Visible = false;
            }

            return docMateria;

        }

        private void docentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDocente mdiDocente = new FrmDocente();
            mdiDocente.MdiParent = this;
            mdiDocente.Show();
        }

        private void modulosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estudianteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEstudiante mdiEstudiante = new FrmEstudiante();
            mdiEstudiante.MdiParent = this;
            mdiEstudiante.Show();
        }

        private void ingresarNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNotas mdiNota = new FrmNotas(materiaEncargado);
            mdiNota.MdiParent = this;
            mdiNota.Show();
        }

        private void asignarDocenteResponsableAGradoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Grado_Docente mdiDocente = new Grado_Docente();
            mdiDocente.MdiParent = this;
            mdiDocente.Show();
        }

        private void cambiarContraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRecuperarPassword mdi = new FrmRecuperarPassword();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void FrmMenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void reporteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void matricularAlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMatricula mdiMatricula = new FrmMatricula(gradoEncargado);
            mdiMatricula.MdiParent = this;
            mdiMatricula.Show();
        }

        private void materiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMateria mdi = new frmMateria();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void administradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAdministrador mdi = new FrmAdministrador();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void gradoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGrado mdi = new frmGrado();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void periodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPeriodo mdi = new FrmPeriodo();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void seccionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSecciones mdi = new FrmSecciones();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void asignarMateriasAImpartirALosDocentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMaterias_Profesor mdi = new FrmMaterias_Profesor();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void encargadosResponsablesDelGradoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDocenteGrado mdi = new frmDocenteGrado();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void materiasPorDocenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMateriaDocente mdi = new frmMateriaDocente();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void docentesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmDocente mdi = new frmDocente();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void boletaDeNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBoletaPorEstudiante mdi = new frmBoletaPorEstudiante();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void alumnosMatriculadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEstudianteMatriculado mdi = new frmEstudianteMatriculado();
            mdi.MdiParent = this;
            mdi.Show();
        }

        private void notasDelPeriodoPorGradoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNotaPorMateria mdi = new frmNotaPorMateria();
            mdi.MdiParent = this;
            mdi.Show();
        }
    }
}
