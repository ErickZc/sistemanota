﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.WinForms.Reporte;
using SistemaGestionNotas.BLO.Grado;
using CrystalDecisions.Shared;

namespace SistemaGestionNotas.WinForms.Reporte
{
    public partial class frmEstudianteMatriculado : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();

        public frmEstudianteMatriculado()
        {
            InitializeComponent();
            llenarCmbGrado();
            this.crystalReportViewer1.Enabled = false;
            this.crystalReportViewer1.Visible = false;
        }

        private void frmEstudianteMatriculado_Load(object sender, EventArgs e)
        {

        }

        public void llenarCmbGrado()
        {
            DaoGrado grado = new DaoGrado();
            Array lista = null;

            lista = grado.listarGradoSeccion();

            if (lista.Length > 0)
            {
                cmbGrado.DataSource = lista;
                cmbGrado.DisplayMember = "grado";
                cmbGrado.ValueMember = "id_grado";

                if (cmbGrado.Items.Count >= 1)
                {
                    cmbGrado.SelectedIndex = -1;
                }

            }

        }

        public bool validar()
        {

            if (cmbGrado.Text.Equals(""))
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {
            if (validar())
            {
                this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
                this.miParametro.Name = "@idGrado";
                this.valor.Value = int.Parse(cmbGrado.SelectedValue.ToString());
                this.miParametro.CurrentValues.Add(valor);
                this.parametros.Add(miParametro);

                this.crystalReportViewer1.ParameterFieldInfo = parametros;

                reporteEstudianteMatriculado rpt = new reporteEstudianteMatriculado();
                //rpt.SetDatabaseLogon("sa", "1234","DESKTOP-8FLDK6E\SQLEXPRESS","SistemaNota");
                this.crystalReportViewer1.ReportSource = rpt;
                this.crystalReportViewer1.Zoom(75);
                this.crystalReportViewer1.Enabled = true;
                this.crystalReportViewer1.Visible = true;
            }
            else
            {
                MessageBox.Show("Llene los registros solicitados");
            }
        }

    }
}
