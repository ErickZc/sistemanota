﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.BLO.Estudiante;
using SistemaGestionNotas.BLO.Secciones;
using CrystalDecisions.Shared;

namespace SistemaGestionNotas.WinForms.Reporte
{
    public partial class frmBoletaPorEstudiante : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();

        public frmBoletaPorEstudiante()
        {
            InitializeComponent();
            llenarCmbGrado();
            this.crystalReportViewer1.Enabled = false;
            this.crystalReportViewer1.Visible = false;
        }

        private void frmBoletaPorEstudiante_Load(object sender, EventArgs e)
        {

        }

        public void llenarCmbGrado() {
            DaoGrado grado = new DaoGrado();
            Array lista = null;

            lista = grado.listarGradoSeccion();

            if (lista.Length > 0)
            {
                cmbGrado.DataSource = lista;
                cmbGrado.DisplayMember = "grado";
                cmbGrado.ValueMember = "id_grado";

                if (cmbGrado.Items.Count >= 1)
                {
                    cmbGrado.SelectedIndex = -1;
                }

            }

        }

        private void txtBuscar_Click(object sender, EventArgs e)
        {
            if (validar())
            {
                this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
                this.miParametro.Name = "@idEstudiante";
                this.valor.Value = int.Parse(cmbEstudiante.SelectedValue.ToString());
                this.miParametro.CurrentValues.Add(valor);
                this.parametros.Add(miParametro);

                this.crystalReportViewer1.ParameterFieldInfo = parametros;

                reporteBoletaPorEstudiante rpt = new reporteBoletaPorEstudiante();
                //rpt.SetDatabaseLogon("sa", "1234");
                this.crystalReportViewer1.ReportSource = rpt;
                this.crystalReportViewer1.Zoom(75);
                this.crystalReportViewer1.Enabled = true;
                this.crystalReportViewer1.Visible = true;
            }
            else {
                MessageBox.Show("Llene los registros solicitados");
            }
        }

        public bool validar() {

            if (cmbGrado.Text.Equals("")) {
                return false;
            }
            else if (cmbEstudiante.Text.Equals(""))
            {
                return false;
            }
            else {
                return true;
            }

        }

        private void cmbGrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbGrado.Text.Equals(""))
            {
                cargarComboEstudiante(cmbGrado.SelectedIndex + 1);
                
            }
            else
            {
                cmbEstudiante.DataSource = null;
                cmbEstudiante.Items.Clear();
            }
        }

        public void cargarComboEstudiante(int valor)
        {
            DaoEstudiante estudiante = new DaoEstudiante();
            Array lista = null;

            lista = estudiante.cmbListarEstudiante(valor);
            if (lista.Length > 0)
            {
                //cmbMateria[] = "";
                cmbEstudiante.DataSource = lista;
                cmbEstudiante.DisplayMember = "valor";
                cmbEstudiante.ValueMember = "id_estudiante";
            }

            if (cmbEstudiante.Items.Count >= 1)
            {
                cmbEstudiante.SelectedIndex = -1;
            }
        }

    }
}
