﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaGestionNotas.WinForms.Reporte
{
    public partial class frmDocente : Form
    {
        public frmDocente()
        {
            InitializeComponent();
            reporteDocente rpt = new reporteDocente();
            //rpt.SetDatabaseLogon("sa", "1234","DESKTOP-8FLDK6E\SQLEXPRESS","SistemaNota");
            this.crystalReportViewer1.ReportSource = rpt;
            this.crystalReportViewer1.Zoom(75);
            this.crystalReportViewer1.Enabled = true;
            this.crystalReportViewer1.Visible = true;
        }
    }
}
