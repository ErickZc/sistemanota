﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.BLO.Materia;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.BLO.Notas;

namespace SistemaGestionNotas.WinForms.Reporte
{
    public partial class frmNotaPorMateria : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro1 = new ParameterField();
        ParameterField miParametro2 = new ParameterField();
        ParameterField miParametro3 = new ParameterField();
        ParameterDiscreteValue valor1 = new ParameterDiscreteValue();
        ParameterDiscreteValue valor2 = new ParameterDiscreteValue();
        ParameterDiscreteValue valor3 = new ParameterDiscreteValue();

        public frmNotaPorMateria()
        {
            InitializeComponent();
            cargarComboDocente();
            this.crystalReportViewer1.Enabled = false;
            this.crystalReportViewer1.Visible = false;
            //this.crystalReportViewer1.Zoom(75);
        }

        public void cargarComboGrado(int valor)
        {
            DaoGrado grado = new DaoGrado();
            Array loadGrado = null;

            loadGrado = grado.cargarComboGrado(cmbMateria.SelectedIndex + 1, 1); //Tenes que validarlo el id_docente
            if (loadGrado.Length > 0)
            {
                //cmbMateria[] = "";
                cmbGrado.DataSource = loadGrado;
                cmbGrado.DisplayMember = "valor";
                cmbGrado.ValueMember = "id_grado";
            }

            if (cmbGrado.Items.Count >= 1)
            {
                cmbGrado.SelectedIndex = -1;
            }
        }

        public void cargarComboPeriodo(string combo1)
        {
            if (!combo1.Equals(""))
            {
                DaoPeriodo periodo = new DaoPeriodo();

                var loadPeriodo = periodo.listaPerido().ToList();

                if (loadPeriodo.Count > 0)
                {
                    cmbPeriodo.DataSource = loadPeriodo;
                    cmbPeriodo.DisplayMember = "periodo1";
                    cmbPeriodo.ValueMember = "id_periodo";
                }

                if (cmbPeriodo.Items.Count >= 1)
                {
                    cmbPeriodo.SelectedIndex = -1;
                }
            }

        }
        public void cargarComboMateria(int id_profesor)
        {
            DaoMateria materia = new DaoMateria();
            Array lista = null;

            lista = materia.listarMateriaById(id_profesor); //Validar el id_docente

            if (lista.Length > 0)
            {
                cmbMateria.DataSource = lista;
                cmbMateria.DisplayMember = "nombre_materia";
                cmbMateria.ValueMember = "id_materia";
            }

            if (cmbMateria.Items.Count >= 1)
            {
                cmbMateria.SelectedIndex = -1;
            }

        }
            
        public void cargarComboDocente() {
            DaoDocente docente = new DaoDocente();
            Array lista = null;
            lista = docente.listDocente();

            if (lista.Length > 0)
            {
                cmbProfesor.DataSource = lista;
                cmbProfesor.DisplayMember = "valor";
                cmbProfesor.ValueMember = "id_docente";
            }

            if (cmbProfesor.Items.Count >= 1)
            {
                cmbProfesor.SelectedIndex = -1;
            }

        }


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try { 
                if (validacionesFiltro())
                {
                    this.miParametro1.ParameterValueType = ParameterValueKind.StringParameter;
                    this.miParametro1.Name = "@idMateria";
                    this.valor1.Value = int.Parse(cmbMateria.SelectedValue.ToString());
                    this.miParametro1.CurrentValues.Add(valor1);
                    this.parametros.Add(miParametro1);

                    this.miParametro2.ParameterValueType = ParameterValueKind.StringParameter;
                    this.miParametro2.Name = "@idGrado";
                    this.valor2.Value = int.Parse(cmbGrado.SelectedValue.ToString());
                    this.miParametro2.CurrentValues.Add(valor2);
                    this.parametros.Add(miParametro2);

                    this.miParametro3.ParameterValueType = ParameterValueKind.StringParameter;
                    this.miParametro3.Name = "@idPeriodo";
                    this.valor3.Value = int.Parse(cmbPeriodo.SelectedValue.ToString());
                    this.miParametro3.CurrentValues.Add(valor3);
                    this.parametros.Add(miParametro3);
                
                    this.crystalReportViewer1.ParameterFieldInfo = parametros;
                
                    reporteNotaPorMateria rpt = new reporteNotaPorMateria();
                    //rpt.SetDatabaseLogon("sa", "1234","DESKTOP-8FLDK6E\SQLEXPRESS","SistemaNota");

                    this.crystalReportViewer1.ReportSource = rpt;
                    this.crystalReportViewer1.Zoom(75);
                    this.crystalReportViewer1.Enabled = true;
                    this.crystalReportViewer1.Visible = true;
                }
                else {
                    MessageBox.Show("Debe completar los datos");
                }
            }catch(Exception){
            
            }
        }

        private void cmbProfesor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbProfesor.Text.Equals(""))
            {
                cargarComboMateria(cmbProfesor.SelectedIndex + 1);
                cargarComboPeriodo("");
            }
            else
            {
                cmbGrado.DataSource = null;
                cmbPeriodo.DataSource = null;
                cmbMateria.DataSource = null;
                cmbGrado.Items.Clear();
                cmbPeriodo.Items.Clear();
                cmbMateria.Items.Clear();
            }
        }

        private void cmbMateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbMateria.Text.Equals(""))
            {
                cargarComboGrado(cmbMateria.SelectedIndex + 1);
                cargarComboPeriodo("");
            }
            else
            {
                cmbGrado.DataSource = null;
                cmbPeriodo.DataSource = null;
                cmbGrado.Items.Clear();
                cmbPeriodo.Items.Clear();
            }
        }

        private void cmbGrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbGrado.Text.Equals(""))
            {
                cargarComboPeriodo(cmbGrado.Text);
            }
            else
            {
                cmbPeriodo.DataSource = null;
                cmbPeriodo.Items.Clear();
            }
        }

        public bool validacionesFiltro()
        {

            if (cmbProfesor.Text.Equals(""))
            {
                return false;
            }

            if (cmbMateria.Text.Equals(""))
            {
                return false;
            }
            if(cmbGrado.Text.Equals("")){
                return false;
            }
            if(cmbPeriodo.Text.Equals("")){
                return false;
            }

            return true;
        }


    }
}
