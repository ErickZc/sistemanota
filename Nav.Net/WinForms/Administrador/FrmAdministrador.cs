﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Administrador;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Administrador
{
    public partial class FrmAdministrador : Form
    {
        DaoAdministradores daoAdmin = new DaoAdministradores();
        List<Administrador_Sistema> listadoAdmin = new List<Administrador_Sistema>();
        List<Administrador_Sistema> listadoAdminValidarUsuarios = new List<Administrador_Sistema>();
        BaseBlo blo = new BaseBlo();

        bool validar = false;
        string passwordEncriptado = "";
        string passwordDesEncriptado = "";
        string oldUsuario = "";
        string boton;
        bool camposVacios = true;
        string[] Columns = new[] { "ID Administrador", "Nombre", "Apellido", "Usuario","Contraseña",
                "Telefono", "Correo", "Dui"};

        public FrmAdministrador()
        {
            InitializeComponent();
            deshabilitar();
            cargarDatos();
            cargarFiltro();
        }

        public void deshabilitar()
        {
            txtNombre.Enabled = false;
            txtApellido.Enabled = false;
            txtUsuario.Enabled = false;
            txtPassword.Enabled = false;
            txtTelefono.Enabled = false;
            txtCorreo.Enabled = false;
            txtDui.Enabled = false;
            btnGuardar.Enabled = false;
            btnModificar.Enabled = false;
            btnEliminar.Enabled = false;

            //cmbFiltrar.Enabled = false;
            //txtValor.Enabled = false;
            //btnRecargar.Enabled = false;
            //btnBuscar.Enabled = false;
        }

        public void habilitar()
        {
            txtNombre.Enabled = true;
            txtApellido.Enabled = true;
            txtUsuario.Enabled = true;
            txtPassword.Enabled = true;
            txtTelefono.Enabled = true;
            txtCorreo.Enabled = true;
            txtDui.Enabled = true;

            btnGuardar.Enabled = boton == "Nuevo" ? true : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? false : false;
            btnModificar.Enabled = boton == "Nuevo" ? false : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? true : false;
            btnEliminar.Enabled = boton == "Nuevo" ? false : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? true : false;

            //cmbFiltrar.Enabled = true;
            //txtValor.Enabled = true;
            //btnRecargar.Enabled = true;
            //btnBuscar.Enabled = true;
        }

        public void limpiar()
        {
            try
            {
                txtId.Text = "";
                txtNombre.Text = "";
                txtApellido.Text = "";
                txtUsuario.Text = "";
                txtPassword.Text = "";
                txtTelefono.Text = "";
                txtCorreo.Text = "";
                txtDui.Text = "";
                boton = "";
                oldUsuario = "";
                cargarFiltro();
                txtValor.Text = "";
                errorProvider1.Clear();
                txtId.Focus();
            }
            catch (Exception)
            {

            }
        }

        public void cargarDatos()
        {
            DaoAdministradores daoAdmin = new DaoAdministradores();
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                listadoAdmin = daoAdmin.getAdministradores().Where(x => x.estado.Equals("OPEN")).Select(x => x).ToList();
                listadoAdminValidarUsuarios = daoAdmin.getAdministradores();
                //limpiar valores del DataGridView
                dgvDatos2.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDatos2.DataSource = blo.toDataTable(listadoAdmin.Select(x => new
                {
                    x.id_admin,
                    x.nombre,
                    x.apellido,
                    x.username,
                    x.password,
                    x.telefono,
                    x.correo,
                    x.dui
                }).ToList(), Columns);

                /// evita que se puedan agregar nuevas filas en los dataGridView
                dgvDatos2.AllowUserToAddRows = false;
                /// evita que se pueda editar en los dataGridView
                dgvDatos2.EditMode = DataGridViewEditMode.EditProgrammatically;
                //Marca todo el row del datagrid
                dgvDatos2.SelectionMode = DataGridViewSelectionMode.CellSelect;
                //Marcara toda la linea del DataGridView
                dgvDatos2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                //hace que las columnas se ajusten al texto de la celda
                dgvDatos2.AutoResizeColumns();
                //no permite add rows por medio del usuario
                dgvDatos2.AllowUserToAddRows = false;
                //Ajusta el tamaño de las celdas
                this.dgvDatos2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                //Ocultara el campo ID Docente del DataGridView
                dgvDatos2.Columns["ID Administrador"].Visible = false;
                dgvDatos2.CurrentCell = dgvDatos2.Rows[0].Cells[1];
            }
            catch (Exception)
            {

            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            limpiar();
            boton = "Nuevo";
            habilitar();
            txtNombre.Focus();
        }

        public Administrador_Sistema setearValores(bool flag)
        {
            try
            {
                Administrador_Sistema admin = new Administrador_Sistema();
                BaseBlo blo = new BaseBlo();

                admin.nombre = txtNombre.Text;
                admin.apellido = txtApellido.Text;
                admin.username = txtUsuario.Text;

                if (flag)
                {
                    try
                    {
                        if (passwordEncriptado.Equals(txtPassword.Text))
                            admin.password = passwordEncriptado;
                        else if (passwordDesEncriptado.Equals(txtPassword.Text))
                            admin.password = blo.Encriptar(txtPassword.Text);
                        else
                            admin.password = blo.Encriptar(txtPassword.Text);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                    admin.password = blo.Encriptar(txtPassword.Text);

                admin.telefono = txtTelefono.Text;
                admin.correo = txtCorreo.Text;
                admin.dui = txtDui.Text;
                admin.estado = "OPEN";

                return admin;

            }
            catch (Exception)
            {
                boton = "";
                return null;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                validarCamposVacios();

                if (camposVacios)
                {
                    errorProvider1.SetError(btnGuardar, "No se permite guardar si no a ingresado datos.");
                    return;
                }

                boton = "Guardar";
                //false poque es insertar
                Administrador_Sistema admin = new Administrador_Sistema();
                admin = setearValores(false);

                if (admin != null)
                    if (daoAdmin.insertarAdministrador(admin))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                limpiar();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                boton = "Modificar";
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea modificar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    //true porque es modificar
                    Administrador_Sistema admin = new Administrador_Sistema();
                    admin = setearValores(true);

                    if (admin != null)
                        if (daoAdmin.modificarAdministrador(admin, Convert.ToInt32(txtId.Text)))
                            MessageBox.Show("Información Modificada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show("Error al Modificar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                        MessageBox.Show("Error al Modificar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiar();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                boton = "Eliminar";
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoAdmin.eliminarAdministrador(Convert.ToInt32(txtId.Text)))
                        MessageBox.Show("Información Eliminada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiar();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void FrmAdministrador_Load(object sender, EventArgs e)
        {

        }

        public void validarCampos(Object sender, CancelEventArgs e, TextBox textBox, string campo)
        {
            if (textBox.Text.Equals(""))
            {
                //e.Cancel = true;
                textBox.Select(0, textBox.Text.Length);
                errorProvider1.SetError(textBox, "Debe introducir datos en el campo: " + campo);
            }
            else
            {
                errorProvider1.SetError(textBox, "");
            }
        }

        public void validarCamposConMascara(Object sender, CancelEventArgs e, MaskedTextBox textBox, string campo)
        {
            if (!textBox.MaskFull)
            {
                //e.Cancel = true;
                textBox.Select(0, textBox.Text.Length);
                errorProvider1.SetError(textBox, "Debe introducir datos en el campo: " + campo);
            }
            else
            {
                errorProvider1.SetError(textBox, "");
            }
        }

        public void validarCamposVacios()
        {
            try
            {
                if (txtNombre.Text.Equals("") || txtApellido.Text.Equals("") || txtUsuario.Text.Equals("")
                    || txtPassword.Text.Equals("") || txtTelefono.MaskFull == false || txtCorreo.Text.Equals("")
                    || txtDui.Equals(""))
                {
                    camposVacios = true;
                }
                else camposVacios = false;
            }
            catch (Exception)
            {

            }
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            if (listadoAdminValidarUsuarios != null)
            {
                if (oldUsuario.Equals(txtUsuario.Text))
                {
                    if (txtId.Text != "")
                        btnModificar.Enabled = true;
                    errorProvider1.SetError(txtUsuario, "");
                }
                else
                {
                    var listado = listadoAdminValidarUsuarios.Where(x => x.username.Equals(txtUsuario.Text)).Select(s => s.username).ToList();

                    if (listado.Count > 0)
                        foreach (var item in listado)
                        {
                            if (item.Equals(txtUsuario.Text))
                            {
                                btnModificar.Enabled = false;
                                errorProvider1.SetError(txtUsuario, "El nombre de usuario " + txtUsuario.Text + " no está disponible.");
                            }
                            else
                            {
                                if (txtId.Text != "")
                                    btnModificar.Enabled = true;
                                errorProvider1.SetError(txtUsuario, "");
                            }
                        }
                    else
                    {
                        if (txtId.Text != "")
                            btnModificar.Enabled = true;
                        errorProvider1.SetError(txtUsuario, "");
                    }
                }
            }
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtNombre, "Nombre");
        }

        private void txtApellido_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtApellido, "Apellido");
        }

        private void txtUsuario_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtUsuario, "Usuario");
        }

        private void txtPassword_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtPassword, "Password");
        }

        private void txtTelefono_Validating(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, txtTelefono, "Telefono");
        }

        private void txtCorreo_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtCorreo, "Correo");
        }

        private void txtDui_Validating(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, txtDui, "Dui");
        }

        public void filtarPor(string columna, string valor)
        {
            List<Administrador_Sistema> lista = new List<Administrador_Sistema>();

            if (columna.Equals("ID Administrador"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.id_admin.ToString().Contains(valor)).Select(c => c).ToList();
            if (columna.Equals("Nombre"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.nombre.Contains(valor) || x.nombre.StartsWith(valor) || x.nombre.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Apellido"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.apellido.Contains(valor) || x.apellido.StartsWith(valor) || x.apellido.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Usuario"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.username.Contains(valor) || x.username.StartsWith(valor) || x.username.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Telefono"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.telefono.Contains(valor) || x.telefono.StartsWith(valor) || x.telefono.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Correo"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.correo.Contains(valor) || x.correo.StartsWith(valor) || x.correo.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Dui"))
                lista = listadoAdmin.Where(x => x.estado.Equals("OPEN") && x.dui.Contains(valor) || x.dui.StartsWith(valor) || x.dui.EndsWith(valor)).Select(c => c).ToList();

            //limpiar valores del DataGridView
            dgvDatos2.Columns.Clear();
            //Asignacion de valores al DataGridView
            dgvDatos2.DataSource = blo.toDataTable(lista.Select(x => new
            {
                x.id_admin,
                x.nombre,
                x.apellido,
                x.username,
                x.password,
                x.telefono,
                x.correo,
                x.dui
            }).ToList(), Columns);

            /// evita que se puedan agregar nuevas filas en los dataGridView
            dgvDatos2.AllowUserToAddRows = false;
            /// evita que se pueda editar en los dataGridView
            dgvDatos2.EditMode = DataGridViewEditMode.EditProgrammatically;
            /// negrita para las cabezeras de los dataGridView
            dgvDatos2.ColumnHeadersDefaultCellStyle.Font = new Font(dgvDatos2.Font, FontStyle.Bold);
            /// remueve el espacio innecesario
            dgvDatos2.BackgroundColor = Color.FromArgb(182, 181, 180);
            //Marca todo el row del datagrid
            dgvDatos2.SelectionMode = DataGridViewSelectionMode.CellSelect;
            ///aplica formato a letras
            dgvDatos2.Font = new Font(new FontFamily("Times New Roman"), 10.0f, FontStyle.Bold);
            //Marcara toda la linea del DataGridView
            dgvDatos2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //hace que las columnas se ajusten al texto de la celda
            dgvDatos2.AutoResizeColumns();
            //no permite add rows por medio del usuario
            dgvDatos2.AllowUserToAddRows = false;
            //Ajusta el tamaño de las celdas
            this.dgvDatos2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            //Ocultara el campo ID Docente del DataGridView
            dgvDatos2.Columns["ID Administrador"].Visible = false;
            //dgvDatos2.CurrentCell = dgvDatos2.Rows[0].Cells[1];
        }

        public bool validacionesFiltro()
        {
            bool i = false;

            if (cmbFiltrar.Text.Equals("Seleccione un filtro"))
                return i;

            if (!txtValor.Text.Equals(""))
                i = true;

            return i;
        }

        public void cargarFiltro()
        {
            cmbFiltrar.Items.Add("Seleccione un filtro");
            //cmbFiltrar.Items.Add("ID Administrador");
            cmbFiltrar.Items.Add("Nombre");
            cmbFiltrar.Items.Add("Apellido");
            cmbFiltrar.Items.Add("Usuario");
            cmbFiltrar.Items.Add("Telefono");
            cmbFiltrar.Items.Add("Correo");
            cmbFiltrar.Items.Add("Dui");
            cmbFiltrar.SelectedIndex = 0;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (validacionesFiltro())
                filtarPor(cmbFiltrar.Text, txtValor.Text);

            if (txtValor.Text.Equals(""))
            {
                errorProvider1.SetError(txtValor, "Debe de llenar el campo");
            }
            else
            {
                errorProvider1.SetError(txtValor, "");
            }
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            cargarDatos();
            limpiar();
        }

        public void validarCamposString(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsLetter(e.KeyChar))
                e.Handled = true;
        }

        public void validarCamposNumerico(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (!Char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (!Char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposString(sender, e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposString(sender, e);
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar == Convert.ToChar(Keys.Space);
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposNumerico(sender, e);
        }

        private void txtDui_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposNumerico(sender, e);
        }

        private void txtDui_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void txtDui_KeyPress_1(object sender, KeyPressEventArgs e)
        {

        }

        private void txtTelefono_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            validarCamposNumerico(sender, e);
        }

        private void txtDui_KeyDown_1(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Subtract) || (e.KeyCode == Keys.Add))
                validar = true;
            else
                validar = false;
        }

        private void txtDui_KeyPress_2(object sender, KeyPressEventArgs e)
        {
            if (validar)
            {
                txtDui.Mask = "########-#";
            }
        }

        private void maskedTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Subtract) || (e.KeyCode == Keys.Add))
                validar = true;
            else
                validar = false;
        }

        private void maskedTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (validar)
            {
                txtTelefono.Text = "";
            }
        }

        private void dgvDatos_Click_1(object sender, EventArgs e)
        {
            try
            {
                boton = "modificarEliminar";
                errorProvider1.Clear();
                //Recuperacion de valores del DataGridView para asignarlos a los campos
                txtId.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellido.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[2].Value.ToString();

                oldUsuario = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[3].Value.ToString();
                txtUsuario.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[3].Value.ToString();

                txtPassword.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[4].Value.ToString();
                passwordEncriptado = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[4].Value.ToString();

                passwordDesEncriptado = blo.DesEncriptar(passwordEncriptado);

                txtTelefono.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[5].Value.ToString();
                txtCorreo.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[6].Value.ToString();
                txtDui.Text = dgvDatos2.Rows[dgvDatos2.CurrentRow.Index].Cells[7].Value.ToString();

                habilitar();
            }
            catch (Exception)
            {

            }
        }



    }
}
