﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.WinForms.Administrador;
using SistemaGestionNotas.WinForms.Docente;
using SistemaGestionNotas.WinForms.Estudiante;
using SistemaGestionNotas.WinForms.Materia;
using SistemaGestionNotas.WinForms.Grado;
using SistemaGestionNotas.WinForms.Periodo;
using SistemaGestionNotas.WinForms.Secciones;
using SistemaGestionNotas.WinForms.Notas;
using SistemaGestionNotas.WinForms.Login;
using SistemaGestionNotas.WinForms.Reporte;
using SistemaGestionNotas.Model;


namespace SistemaGestionNotas.WinForms
{
    public partial class FrmPrincipal : Form
    {
        private string nivelAcceso { get; set; }
        private Form activeForm = null;
        bool docEncargado = false;
        bool docMateria = false;
        Docente_Grado gradoEncargado = new Docente_Grado();
        Docente_Materia_Grado materiaEncargado = new Docente_Materia_Grado();
        public FrmPrincipal(string nivel)
        {
            InitializeComponent();
            Cuztomize();
            nivelAcceso=nivel;
            //visualizacion();
            this.label2.Text = ">>Inicio";
            openChildFormInPanel(new FrmInicio());
            hideSubMenu();
        }

        public bool docenteEncargado(Docente_Grado encargado)
        {

            if (encargado.idDocente_Grado != 0)
            {
                docEncargado = true;
                gradoEncargado = encargado;
            }
            else
            {
                docEncargado = false;
            }

            return docEncargado;

        }

        public bool docenteMateria(Docente_Materia_Grado docenteMateriaa)
        {

            if (docenteMateriaa.idDocente_Materia != 0)
            {
                docMateria = true;
                materiaEncargado = docenteMateriaa;
            }
            else
            {
                docMateria = false;
            }

            return docMateria;

        }

        public void visualizacion()
        {
            if (nivelAcceso.Equals("Administrador"))
            {
                btnAdministracion.Visible = true;
                btnReporteria.Visible = true;
                btnMantenimientoAdm.Visible = true;
                btnCambiarContra.Visible = true;
                btnInicio.Visible = true;
                btnCerrarSesion.Visible = true;
                btnMantenimientoDE.Visible = false;
                btnMantenimientoDoc.Visible = false;
                btnMantenimientoEnc.Visible = false;
            }
            else
            {
                btnAdministracion.Visible = false;
                btnReporteria.Visible = false;
                btnMantenimientoAdm.Visible = false;
                btnCambiarContra.Visible = true;
                btnInicio.Visible = true;
                btnCerrarSesion.Visible = true;

                if (gradoEncargado.idDocente_Grado != 0)
                {
                    if (materiaEncargado.idDocente_Materia !=0)
                    {
                        btnMantenimientoDE.Visible = true;
                        btnMantenimientoDoc.Visible = false;
                        btnMantenimientoEnc.Visible =  false;
                    }
                    else
                    {
                        btnMantenimientoEnc.Visible = true;
                        btnMantenimientoDE.Visible = false;
                        btnMantenimientoDoc.Visible = false;
                    }
                }
                else
                {
                    if (materiaEncargado.idDocente_Materia != 0)
                    {
                        btnMantenimientoDoc.Visible = true;
                        btnMantenimientoEnc.Visible = false;
                        btnMantenimientoDE.Visible = false;
                    }
                    else
                    {
                        btnMantenimientoDoc.Visible = false;
                        btnMantenimientoEnc.Visible = false;
                        btnMantenimientoDE.Visible = false;
                    }
                }

            }
        }

        private void Cuztomize()
        {
            panelMenuAdministracion.Visible = false;
            panelMenuMantenimiento.Visible = false;
            panelMenuReportes.Visible = false;
            panelDoc.Visible = false;
            panelDE.Visible = false;
            panelEnc.Visible = false;
        }

        private void hideSubMenu()
        {
            if (panelMenuAdministracion.Visible == true)
            {
                panelMenuAdministracion.Visible = false;
            }
            if (panelMenuMantenimiento.Visible == true)
            {
                panelMenuMantenimiento.Visible = false;
            }
            if (panelMenuReportes.Visible == true)
            {
                panelMenuReportes.Visible = false;
            }
            if (panelDoc.Visible == true)
            {
                panelDoc.Visible = false;
            }
            if (panelDE.Visible == true)
            {
                panelDE.Visible = false;
            }
            if (panelEnc.Visible == true)
            {
                panelEnc.Visible = false;
            }
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }

        private void openChildFormInPanel(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelFormularios.Controls.Add(childForm);
            panelFormularios.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdministracion_Click(object sender, EventArgs e)
        {
            showSubMenu(panelMenuAdministracion);
        }

        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            showSubMenu(panelMenuMantenimiento);
        }

        private void btnReporteria_Click(object sender, EventArgs e)
        {
            showSubMenu(panelMenuReportes);
        }

        private void btnAdmAdministradores_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Administradores";
            openChildFormInPanel(new FrmAdministrador());
            hideSubMenu();
        }

        private void btnAdmDocentes_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Docentes";
            openChildFormInPanel(new FrmDocente());
            hideSubMenu();
        }

        private void btnAdmEstudiantes_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Estudiantes";
            openChildFormInPanel(new FrmEstudiante());
            hideSubMenu();
        }

        private void btnMantAlumnos_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Matricula";
            openChildFormInPanel(new FrmMatricula(gradoEncargado));
            hideSubMenu();
        }

        private void btnMantEncargados_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Asignación de encargado por grado";
            openChildFormInPanel(new Grado_Docente());
            hideSubMenu();
        }

        private void btnAdmMaterias_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Materias";
            openChildFormInPanel(new frmMateria());
            hideSubMenu();
        }

        private void btnAdmGrados_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Grados";
            openChildFormInPanel(new frmGrado());
            hideSubMenu();
        }

        private void btnAdmSecciones_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Secciones";
            openChildFormInPanel(new FrmSecciones());
            hideSubMenu();
        }

        private void btnMantNotas_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Ingreso de Notas";
            openChildFormInPanel(new FrmNotas(materiaEncargado));
            hideSubMenu();
        }

        private void btnMantMaterias_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Asignación de materias a impartir";
            openChildFormInPanel(new FrmMaterias_Profesor());
            hideSubMenu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            this.Dispose();
            login.ShowDialog();
        }

        private void btnInicio_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Inicio";
            openChildFormInPanel(new FrmInicio());
            hideSubMenu();
        }

        private void btnMantenimientoDoc_Click(object sender, EventArgs e)
        {
            showSubMenu(panelDoc);
        }

        private void btnMantenimientoDE_Click(object sender, EventArgs e)
        {
            showSubMenu(panelDE);
        }

        private void btnMantenimientoEnc_Click(object sender, EventArgs e)
        {
            showSubMenu(panelEnc);
        }

        private void btnCambiarContra_Click(object sender, EventArgs e)
        {
            FrmRecuperarPassword cambiarPassword = new FrmRecuperarPassword();
            cambiarPassword.ShowDialog();
        }

        private void btnRepNotas_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Reporte de notas";
            openChildFormInPanel(new frmNotaPorMateria());
            hideSubMenu();
        }

        private void btnRepBoletas_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Boleta de notas por estudiante";
            openChildFormInPanel(new frmBoletaPorEstudiante());
            hideSubMenu();
        }

        private void btnRepDocentes_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Reporte de docentes";
            openChildFormInPanel(new frmDocente());
            hideSubMenu();
        }

        private void btnRepAlumnos_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Reporte de estudiantes";
            openChildFormInPanel(new frmEstudianteMatriculado());
            hideSubMenu();
        }

        private void btnRepMaterias_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Reporte de materias impartidas";
            openChildFormInPanel(new frmMateriaDocente());
            hideSubMenu();
        }

        private void btnRepEncargados_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Reporte de encargados";
            openChildFormInPanel(new frmDocenteGrado());
            hideSubMenu();
        }

        private void btnMantNotasDoc_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Ingreso de notas";
            openChildFormInPanel(new FrmNotas(materiaEncargado));
            hideSubMenu();
        }

        private void btnMantNotasDC_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Ingreso de notas";
            openChildFormInPanel(new FrmNotas(materiaEncargado));
            hideSubMenu();
        }

        private void btnMantAlumnosDC_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Matricula de Estudiantes";
            openChildFormInPanel(new FrmMatricula(gradoEncargado));
            hideSubMenu();
        }

        private void btnManAlumnosEnc_Click(object sender, EventArgs e)
        {
            this.label2.Text = ">>Matricula de Estudiantes";
            openChildFormInPanel(new FrmMatricula(gradoEncargado));
            hideSubMenu();
        }

        private void panelSuperior_Paint(object sender, PaintEventArgs e)
        {

        }




    }
}
