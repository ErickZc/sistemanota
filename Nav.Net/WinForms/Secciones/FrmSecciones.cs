﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Secciones;

namespace SistemaGestionNotas.WinForms.Secciones
{
    public partial class FrmSecciones : Form
    {
        bool agregar = false;
        bool modificar = false;
        DaoSecciones daoSecciones = new DaoSecciones();
        BaseBlo blo = new BaseBlo();

        private string[] Columns = new[] { "ID Sección", "Sección" };
        public FrmSecciones()
        {
            InitializeComponent();
            cargarDataGrid();
        }

        #region

        public void estadoBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }

            else
            {
                btnGuardar.Enabled = false;
                btnEliminar.Enabled = true;
                btnModificar.Enabled = true;
                btnAgregar.Enabled = true;
            }
        }


        public void habilitarCaja(bool i)
        {
            txtSeccion.Enabled = i;

        }

        public Model.Seccion setearValores()
        {
            try
            {
                Model.Seccion seccion = new Model.Seccion();
                seccion.seccion1 = txtSeccion.Text;
                seccion.estado="OPEN";
                return seccion;

            }
            catch (Exception)
            {
                return new Model.Seccion();
            }
        }

        public void habilitarFiltro(bool i)
        {
            txtValor.Enabled = i;
            cmbFiltrar.Enabled = i;
            btnBuscar.Enabled = i;
            btnRecargar.Enabled = i;
        }

        public void limpiarFiltro()
        {
            txtValor.Clear();
            cmbFiltrar.SelectedIndex = -1; ;
        }

        public bool validarCampos()
        {
            bool estado = false;

            if (!txtSeccion.Text.Equals(""))
            {
                estado = true;
            }
            else
            {
                estado = false;
            }

            return estado;
        }

        public void limpiar()
        {
            txtIdSeccion.Clear();
            txtSeccion.Clear();
        }


        public void cargarDataGrid()
        {
            List<Model.Seccion> lista = new List<Model.Seccion>();

            lista = daoSecciones.listarSecciones();
            dgvDatos.Columns.Clear();
            dgvDatos.DataSource = lista.Select(x => new
            {
                x.id_seccion,
                x.seccion1,
            }).ToList();

            for (int i = 0; i < Columns.Count(); i++)
            {
                dgvDatos.Columns[i].HeaderText = Columns[i];
            }

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }

        }

        public void cargarResultados(string campo, string valor)
        {
            List<Model.Seccion> lista = new List<Model.Seccion>();
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                lista = daoSecciones.filtrarSecciones(campo, valor);
                //limpiar valores del DataGridView
                dgvDatos.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDatos.DataSource = blo.toDataTable(lista.Select(x => new
                {
                    x.id_seccion,
                    x.seccion1,
                }).ToList(), Columns);


                if (dgvDatos.RowCount > 0)
                {
                    dgvDatos.Rows[0].Selected = false;

                }
            }
            catch (Exception)
            {

            }
        }

        public void cargarCajas()
        {

            if (dgvDatos.RowCount > 0)
            {
                txtIdSeccion.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[0].Value.ToString();
                txtSeccion.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[1].Value.ToString();

            }

        }

        #endregion

        private void FrmSecciones_Load(object sender, EventArgs e)
        {
            estadoBotones("Nuevo");
            habilitarCaja(false);
            limpiar();

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            limpiarFiltro();
            habilitarFiltro(false);
            estadoBotones("Agregar");
            limpiar();
            habilitarCaja(true);
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitarCaja(true);
                limpiarFiltro();
                habilitarFiltro(false);
                estadoBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoSecciones.eliminarSecciones(int.Parse(txtIdSeccion.Text)))
                        MessageBox.Show("Registro eliminado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                limpiar();
                cargarDataGrid();
                limpiarFiltro();
                habilitarFiltro(true);
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                if (agregar == true)
                {
                    if (daoSecciones.insertarSecciones(setearValores()))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }
                else if (modificar == true)
                {
                    if (daoSecciones.modificarSecciones(setearValores(), int.Parse(txtIdSeccion.Text)))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }

                limpiar();
                cargarDataGrid();
                habilitarCaja(false);
                limpiarFiltro();
                habilitarFiltro(true);
                estadoBotones("Nuevo");
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            cargarCajas();
            limpiarFiltro();
            habilitarFiltro(false);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (!cmbFiltrar.Text.Equals("") && !txtValor.Text.Equals(""))
            {
                cargarResultados(cmbFiltrar.Text, txtValor.Text);
            }
            else
            {
                MessageBox.Show("Debe completar todos los campos de filtro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            limpiarFiltro();
            cargarDataGrid();
        }



    }
}
