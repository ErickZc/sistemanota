﻿
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.WinForms;
using SistemaGestionNotas.BLO.Grados;
using SistemaGestionNotas.BLO.docente__grado;

namespace SistemaGestionNotas.WinForms.Docente
{
    public partial class Grado_Docente : Form
    {
        bool agregar = false;
        bool modificar = false;
        DaoDocente daoDocente = new DaoDocente();
        DaoGrados daoGrado = new DaoGrados();
        DaoGradoDocente daoDocentegrado = new DaoGradoDocente();

        public Grado_Docente()
        {
            InitializeComponent();
            cargarcmbDocente();
            cargarcmbGrado();
            cargardatos();
            limpiar();
            deshabilitar();
            
        }

       

        private void cargarcmbGrado()
        {

            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.listGrados();

                foreach (Model.Grado p in listadoGrados)
                {
                    lista.Add(p.id_grado,  p.grado1 + " " + p.Seccion.seccion1);
                }

                cmbGrado.DataSource = new BindingSource(lista, null);
                cmbGrado.DisplayMember = "Value";
                cmbGrado.ValueMember = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }

        public void cargarcmbDocente()
        {
            Dictionary<int, string> listaD = new Dictionary<int, string>();
            List<Model.Docente> listadoDocente = new List<Model.Docente>();
            try
            {
                listadoDocente = daoDocente.listDocentes();
                foreach (Model.Docente x in listadoDocente)
                {
                    listaD.Add(x.id_docente,  x.nombre + " " + x.apellido);
                }

                //BindingSource ds1 = new BindingSource(listaD, null);
                cmbProfesor.DataSource = new BindingSource(listaD, null);
                cmbProfesor.DisplayMember = "Value";
                cmbProfesor.ValueMember = "Key";

                
            } catch(Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error" + ex.Message);
            }
        }


        private void cargardatos()
        {
            try
            {
                using (ModeloDB model = new ModeloDB())
                {
                    //int grado = int.Parse(cmbGrado.SelectedValue.ToString());
                    var cargar = (from n in model.Docente_Grado
                                  join d in model.Docente on n.id_docente
                                  equals d.id_docente
                                  join g in model.Grado on n.id_grado equals g.id_grado
                                  join s in model.Seccion on g.id_seccion equals s.id_seccion
                                  where n.estado == "OPEN" && d.estado.Equals("OPEN") && g.estado.Equals("OPEN") && s.estado.Equals("OPEN")
                                  // && n.id_grado == grado
                                  select new
                                  {
                                      n.idDocente_Grado,
                                      n.id_docente,
                                      d.nombre,
                                      d.apellido,
                                      g.grado1,
                                      s.seccion1,
                                      n.fecha


                                  }).ToList();

                    dgvdatos.DataSource = cargar;
                    dgvdatos.Columns["idDocente_Grado"].Visible = false;
                    dgvdatos.Columns["id_docente"].Visible = false;

                    if (dgvdatos.RowCount > 0)
                    {
                        dgvdatos.Rows[0].Selected = false;
                    }
                }


            }
            catch (Exception)
            {

            }




        }


        private void llenar()
        {
            if (dgvdatos.RowCount > 0)
            {
                cmbProfesor.SelectedIndex = cmbProfesor.FindStringExact(dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells["nombre"].Value.ToString() + " " + dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells["apellido"].Value.ToString());
                cmbGrado.SelectedIndex = cmbGrado.FindStringExact(dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells["grado1"].Value.ToString() + " " + dgvdatos.Rows[dgvdatos.CurrentRow.Index].Cells["seccion1"].Value.ToString());
            }
        }


        private void habilitar()
        {
            cmbProfesor.Enabled = true;
            cmbGrado.Enabled = true;
        }

        private void deshabilitar()
        {
            cmbProfesor.Enabled = false;
            cmbGrado.Enabled = false;
        }


        private Docente_Grado setear()
        {
            try
            {
                Docente_Grado dg = new Docente_Grado();

               // KeyValuePair<int, string> a = cmbProfesor.SelectedItem;

                dg.id_docente = int.Parse(cmbProfesor.SelectedValue.ToString());
                dg.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                dg.estado = "OPEN";
                dg.fecha = DateTime.Now.ToString("dd/MM/yyyy");
                return dg;

            }
            catch (Exception)
            {
                return new Docente_Grado();
            }

        }


        private bool validarcampos()
        {
            bool estado = false;
            if (!cmbProfesor.Equals("") && !cmbGrado.Equals(""))
            {
                estado = true;
            }
            else
            {
                estado = false;
            }
            return estado;
        }



        private void limpiar()
        {
            cmbProfesor.Text = "";
            cmbGrado.SelectedIndex = -1;
        }


        private void estadoBtn(string texto)
        {
            if (texto == "Agregar")
            {
                btnAgregar.Enabled = true;
                btnEliminar.Enabled = false;
                btnmodificar.Enabled = false;
                btnNuevo.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnmodificar.Enabled = false;
                btnAgregar.Enabled = true;
                btnEliminar.Enabled = false;
                btnNuevo.Enabled = false;


            }

            else
            {
                btnAgregar.Enabled = false;
                btnEliminar.Enabled = true;
                btnmodificar.Enabled = true;
                btnNuevo.Enabled = true;
            }

        }


        private void Grado_Docente_Load(object sender, EventArgs e)
        {

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            estadoBtn("Agregar");
            limpiar();
            habilitar();
        }

        private void dgvdatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            llenar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (validarcampos()) 
            {
                if (agregar == true)
                {
                    if (daoDocentegrado.insertarDocenteGrado(setear()))
                        MessageBox.Show("informacion registrada con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al tratar de registrar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }
                else if (modificar == true)
                {
                   int id = int.Parse( dgvdatos.SelectedRows[0].Cells[0].Value.ToString());
                           
                    if (daoDocentegrado.modificarDocenteGrado(setear(), id))
                        MessageBox.Show("Dato modificado con exito", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al modificar el dato", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;


                }
                limpiar();
                cargardatos();
                deshabilitar();
                estadoBtn("Nuevo");
            }
           
            else
            {
                MessageBox.Show("Los campos son requeridos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }    
                
            
            
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            if (dgvdatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitar();
                estadoBtn("Modificar");
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro para modificar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvdatos.SelectedRows.Count > 0)
            {
                DialogResult dialog = MessageBox.Show("Estas seguro de eliminar el registro?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    int id = int.Parse(dgvdatos.SelectedRows[0].Cells[0].Value.ToString());
                    if (daoDocentegrado.eliminarDocenteGrado(id))
                        MessageBox.Show("Eliminado satisfactoriamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al eliminar registro", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);


                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("Se ha cancelado la eliminacion de registros", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
                limpiar();
                cargardatos();
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void habilitarbtn()
        {
            btnAgregar.Enabled = false;
            btnCancelar.Enabled = true; 
            btnEliminar.Enabled = true;
            btnmodificar.Enabled = true;
            btnNuevo.Enabled = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            deshabilitar();
            habilitarbtn();
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
