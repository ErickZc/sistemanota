﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.BLO.Roles;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Docente
{
    public partial class FrmDocente : Form
    {
        private string[] Columns = new[] { "ID Docente", "Nombre", "Apellido", "Genero","Direccion", "Dui", "Nit",
            "Correo", "Telefono", "Estado", "Usuario", "Contraseña","Rol" };

        bool validar = false;
        string passwordEncriptado = "";
        string passwordDesEncriptado = "";
        string oldUsuario = "";
        string boton;
        bool camposVacios = true;

        DaoDocente daoDocente = new DaoDocente();
        List<Model.Docente> listadoDocentes = new List<Model.Docente>();
        List<Model.Docente> listadoDocentesValidarUsuarios = new List<Model.Docente>();
        DaoRoles roles = new DaoRoles();
        BaseBlo blo = new BaseBlo();

        public FrmDocente()
        {
            InitializeComponent();
            cargarDatos();
            deshabilitarCampos();
            cargarFiltro();
        }

        public void cargarDatos()
        {
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                listadoDocentes = daoDocente.listDocentes().Where(x => x.estado.Equals("OPEN")).Select(x => x).ToList();
                listadoDocentesValidarUsuarios = daoDocente.listDocentes();
                //limpiar valores del DataGridView
                dgvDocente.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDocente.DataSource = blo.toDataTable(listadoDocentes.Select(x => new
                {
                    x.id_docente,
                    x.nombre,
                    x.apellido,
                    x.genero,
                    x.direccion,
                    x.dui,
                    x.nit,
                    x.correo,
                    x.telefono,
                    x.estado,
                    x.usuario,
                    x.password,
                    x.id_rol.Value
                }).ToList(), Columns);

                /// evita que se puedan agregar nuevas filas en los dataGridView
                dgvDocente.AllowUserToAddRows = false;
                /// evita que se pueda editar en los dataGridView
                dgvDocente.EditMode = DataGridViewEditMode.EditProgrammatically;
                //Marca todo el row del datagrid
                dgvDocente.SelectionMode = DataGridViewSelectionMode.CellSelect;
                //Marcara toda la linea del DataGridView
                dgvDocente.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                //hace que las columnas se ajusten al texto de la celda
                dgvDocente.AutoResizeColumns();
                //no permite add rows por medio del usuario
                dgvDocente.AllowUserToAddRows = false;
                //Ajusta el tamaño de las celdas
                this.dgvDocente.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                //Ocultara el campo ID Docente del DataGridView
                dgvDocente.Columns["ID Docente"].Visible = false;
                dgvDocente.CurrentCell = dgvDocente.Rows[0].Cells[1];
            }
            catch (Exception)
            {

            }
        }

        private void dgvDocente_Click(object sender, EventArgs e)
        {
            string rol = "";
            Rol_Usuario rolUsuario = new Rol_Usuario();
            try
            {
                boton = "modificarEliminar";
                errorProvider1.Clear();
                //Recuperacion de valores del DataGridView para asignarlos a los campos
                txtID.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellido.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[2].Value.ToString();

                if (dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[3].Value.ToString() == "Masculino")
                    rbMasculino.Checked = true;
                else
                    rbFemenino.Checked = true;

                txtDireccion.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[4].Value.ToString();
                mktDui.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[5].Value.ToString();
                mktnit.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[6].Value.ToString();
                txtCorreo.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[7].Value.ToString();
                txtTelefono.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[8].Value.ToString();

                oldUsuario = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[10].Value.ToString();
                txtUsuario.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[10].Value.ToString();

                mktPassword.Text = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[11].Value.ToString();
                passwordEncriptado = dgvDocente.Rows[dgvDocente.CurrentRow.Index].Cells[11].Value.ToString();

                passwordDesEncriptado = blo.DesEncriptar(passwordEncriptado);

                habilitarCampos();
            }
            catch (Exception)
            {

            }
        }

        public void habilitarCampos()
        {
            txtNombre.Enabled = true;
            txtApellido.Enabled = true;
            mktDui.Enabled = true;
            mktnit.Enabled = true;
            txtCorreo.Enabled = true;
            txtTelefono.Enabled = true;
            txtUsuario.Enabled = true;
            mktPassword.Enabled = true;
            txtDireccion.Enabled = true;
            rbFemenino.Enabled = true;
            rbMasculino.Enabled = true;

            btnGuardar.Enabled = boton == "Nuevo" ? true : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? false : false;
            btnModificar.Enabled = boton == "Nuevo" ? false : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? true : false;
            btnEliminar.Enabled = boton == "Nuevo" ? false : boton == "Guardar" ? false : boton == "Modificar" ? false : boton == "Eliminar" ? false : boton == "modificarEliminar" ? true : false;

            //cmbFiltrar.Enabled = true;
            //txtValor.Enabled = true;
            //btnRecargar.Enabled = true;
            //btnBuscar.Enabled = true;
        }

        public void deshabilitarCampos()
        {
            txtNombre.Enabled = false;
            txtApellido.Enabled = false;
            mktDui.Enabled = false;
            mktnit.Enabled = false;
            txtCorreo.Enabled = false;
            txtTelefono.Enabled = false;
            txtUsuario.Enabled = false;
            mktPassword.Enabled = false;
            txtDireccion.Enabled = false;
            btnGuardar.Enabled = false;
            btnModificar.Enabled = false;
            btnEliminar.Enabled = false;
            rbFemenino.Enabled = false;
            rbMasculino.Enabled = false;

            //cmbFiltrar.Enabled = false;
            //txtValor.Enabled = false;
            //btnRecargar.Enabled = false;
            //btnBuscar.Enabled = false;
        }

        public Model.Docente setearValores(bool flag)
        {
            try
            {
                Model.Docente docente = new Model.Docente();

                docente.nombre = txtNombre.Text;
                docente.apellido = txtApellido.Text;
                docente.correo = txtCorreo.Text;
                docente.direccion = txtDireccion.Text;
                docente.dui = mktDui.Text;
                docente.nit = mktnit.Text;

                if (flag)
                {
                    try
                    {
                        if (passwordEncriptado.Equals(mktPassword.Text))
                            docente.password = passwordEncriptado;
                        else if (passwordDesEncriptado.Equals(mktPassword.Text))
                            docente.password = blo.Encriptar(mktPassword.Text);
                        else
                            docente.password = blo.Encriptar(mktPassword.Text);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                    docente.password = blo.Encriptar(mktPassword.Text);

                try
                {
                    docente.id_rol = roles.getRolSeleccionado().id_rol;
                }
                catch (Exception)
                {

                }

                docente.telefono = txtTelefono.Text;
                docente.usuario = txtUsuario.Text;
                if (rbMasculino.Checked)
                    docente.genero = "Masculino";
                else
                    docente.genero = "Femenino";
                docente.estado = "OPEN";
                docente.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy");

                return docente;

            }
            catch (Exception)
            {
                boton = "";
                return null;
            }
        }

        public void limpiarCampos()
        {
            try
            {
                txtID.Text = "";
                txtNombre.Text = "";
                txtApellido.Text = "";
                mktDui.Text = "";
                mktnit.Text = "";
                txtCorreo.Text = "";
                txtTelefono.Text = "";
                txtUsuario.Text = "";
                mktPassword.Text = "";
                txtDireccion.Text = "";
                if (rbMasculino.Checked)
                    rbMasculino.Checked = false;
                else
                    rbFemenino.Checked = false;
                boton = "";
                oldUsuario = "";
                cargarFiltro();
                txtValor.Text = "";
                errorProvider1.Clear();
                txtID.Focus();
            }
            catch (Exception)
            {

                throw ;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                limpiarCampos();
                boton = "Nuevo";
                habilitarCampos();
                txtNombre.Focus();
            }
            catch (Exception)
            {

            }
        }

        public void validarCamposVacios()
        {
            try
            {
                if (txtNombre.Text.Equals("") || txtApellido.Text.Equals("") || txtDireccion.Text.Equals("")
                    || txtUsuario.Text.Equals("") || txtTelefono.Text.Equals("") || mktDui.MaskFull == false
                    || mktnit.MaskFull == false || txtCorreo.Text.Equals("")
                    || txtUsuario.Text.Equals("") || mktPassword.Text.Equals(""))
                {
                    camposVacios = true;
                }
                else camposVacios = false;
            }
            catch (Exception)
            {

            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                validarCamposVacios();

                if (camposVacios)
                {
                    errorProvider1.SetError(btnGuardar, "No se permite guardar si no a ingresado datos.");
                    return;
                }

                boton = "Guardar";
                //false poque es insertar
                Model.Docente admin = new Model.Docente();
                admin = setearValores(false);

                if (admin != null)
                    if (daoDocente.insertarDocente(admin))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                    MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                limpiarCampos();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                boton = "Modificar";
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea modificar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    //true porque es modificar
                    Model.Docente admin = new Model.Docente();
                    admin = setearValores(true);

                    if (admin != null)
                        if (daoDocente.modificarDocente(admin, Convert.ToInt32(txtID.Text)))
                            MessageBox.Show("Información Modificada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show("Error al Modificar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                        MessageBox.Show("Error al Modificar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiarCampos();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                boton = "Eliminar";
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoDocente.eliminarDocente(Convert.ToInt32(txtID.Text)))
                        MessageBox.Show("Información Eliminada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiarCampos();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void FrmDocente_Load(object sender, EventArgs e)
        {

        }

        public void validarCampos(Object sender, CancelEventArgs e, TextBox textBox, string campo)
        {
            if (textBox.Text.Equals(""))
            {
                //e.Cancel = true;
                //camposVacios = true;
                textBox.Select(0, textBox.Text.Length);
                textBox.Focus();
                errorProvider1.SetError(textBox, "Debe introducir datos en el campo: " + campo);
            }
            else
            {
                //camposVacios = false;
                errorProvider1.SetError(textBox, "");
            }
        }

        public void validarCamposConMascara(Object sender, CancelEventArgs e, MaskedTextBox textBox, string campo)
        {
            if (!textBox.MaskFull)
            {
                //e.Cancel = true;
                //camposVacios = true;
                textBox.Select(0, textBox.Text.Length);
                textBox.Focus();
                errorProvider1.SetError(textBox, "Debe introducir datos en el campo: " + campo);
            }
            else
            {
                ///camposVacios = false;
                errorProvider1.SetError(textBox, "");
            }
        }

        private void txtNombre_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtNombre, "Nombre");
        }

        private void txtApellido_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtApellido, "Apellido");
        }

        private void txtDireccion_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtDireccion, "Direccion");
        }

        private void txtTelefono_Validating(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, txtTelefono, "Telefono");
        }

        private void mktDui_Validating(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, mktDui, "Dui");
        }

        private void mktnit_Validating(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, mktnit, "Nit");
        }

        private void txtCorreo_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtCorreo, "Correo");
        }

        private void txtUsuario_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, txtUsuario, "Usuario");
        }

        private void mktPassword_Validating(object sender, CancelEventArgs e)
        {
            validarCampos(sender, e, mktPassword, "Password");
        }

        private void txtTelefono_Validating_1(object sender, CancelEventArgs e)
        {
            validarCamposConMascara(sender, e, txtTelefono, "Telefono");
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = e.KeyChar == Convert.ToChar(Keys.Space);
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

            if (listadoDocentesValidarUsuarios != null)
            {
                if (oldUsuario.Equals(txtUsuario.Text))
                {
                    if (txtID.Text != "")
                        btnModificar.Enabled = true;
                    errorProvider1.SetError(txtUsuario, "");
                }
                else
                {
                    var listado = listadoDocentesValidarUsuarios.Where(x => x.usuario.Equals(txtUsuario.Text)).Select(s => s.usuario).ToList();

                    if (listado.Count > 0)
                        foreach (var item in listado)
                        {
                            if (item.Equals(txtUsuario.Text))
                            {
                                btnModificar.Enabled = false;
                                errorProvider1.SetError(txtUsuario, "El nombre de usuario " + txtUsuario.Text + " no está disponible.");
                            }
                            else
                            {
                                if (txtID.Text != "")
                                    btnModificar.Enabled = true;
                                errorProvider1.SetError(txtUsuario, "");
                            }
                        }
                    else
                    {
                        if (txtID.Text != "")
                            btnModificar.Enabled = true;
                        errorProvider1.SetError(txtUsuario, "");
                    }
                }
            }
        }

        public void filtarPor(string columna, string valor)
        {
            List<Model.Docente> lista = new List<Model.Docente>();

            if (columna.Equals("ID Docente"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.id_docente.ToString().Contains(valor) || x.nombre.StartsWith(valor) || x.nombre.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Nombre"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.nombre.Contains(valor) || x.nombre.StartsWith(valor) ||x.nombre.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Apellido"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.apellido.Contains(valor) || x.apellido.StartsWith(valor) || x.apellido.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Genero"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.genero.Contains(valor) || x.genero.StartsWith(valor) || x.genero.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Direccion"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.direccion.Contains(valor) || x.direccion.StartsWith(valor) || x.direccion.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Dui"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.dui.Contains(valor) || x.dui.StartsWith(valor) || x.dui.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Nit"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.nit.Contains(valor) || x.nit.StartsWith(valor) || x.nit.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Correo"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.correo.Contains(valor) || x.correo.StartsWith(valor) || x.correo.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Telefono"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.telefono.Contains(valor) || x.telefono.StartsWith(valor) || x.telefono.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("Usuario"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.usuario.Contains(valor) || x.usuario.StartsWith(valor) || x.usuario.EndsWith(valor)).Select(c => c).ToList();
            if (columna.Equals("ID Rol"))
                lista = listadoDocentes.Where(x => x.estado.Equals("OPEN") && x.id_rol.ToString().Contains(valor) || x.id_rol.ToString().StartsWith(valor) || x.id_rol.ToString().EndsWith(valor)).Select(c => c).ToList();

            //limpiar valores del DataGridView
            dgvDocente.Columns.Clear();
            //Asignacion de valores al DataGridView
            dgvDocente.DataSource = blo.toDataTable(lista.Select(x => new
            {
                x.id_docente,
                x.nombre,
                x.apellido,
                x.genero,
                x.direccion,
                x.dui,
                x.nit,
                x.correo,
                x.telefono,
                x.estado,
                x.usuario,
                x.password,
                x.id_rol.Value
            }).ToList(), Columns);

            /// evita que se puedan agregar nuevas filas en los dataGridView
            dgvDocente.AllowUserToAddRows = false;
            /// evita que se pueda editar en los dataGridView
            dgvDocente.EditMode = DataGridViewEditMode.EditProgrammatically;
            /// negrita para las cabezeras de los dataGridView
            dgvDocente.ColumnHeadersDefaultCellStyle.Font = new Font(dgvDocente.Font, FontStyle.Bold);
            /// remueve el espacio innecesario
            dgvDocente.BackgroundColor = Color.FromArgb(182, 181, 180);
            //Marca todo el row del datagrid
            dgvDocente.SelectionMode = DataGridViewSelectionMode.CellSelect;
            ///aplica formato a letras
            dgvDocente.Font = new Font(new FontFamily("Times New Roman"), 10.0f, FontStyle.Bold);
            //Marcara toda la linea del DataGridView
            dgvDocente.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //hace que las columnas se ajusten al texto de la celda
            dgvDocente.AutoResizeColumns();
            //no permite add rows por medio del usuario
            dgvDocente.AllowUserToAddRows = false;
            //Ajusta el tamaño de las celdas
            this.dgvDocente.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            //Ocultara el campo ID Docente del DataGridView
            dgvDocente.Columns["ID Docente"].Visible = false;
            //dgvDocente.CurrentCell = dgvDocente.Rows[0].Cells[1];
        }

        public bool validacionesFiltro()
        {
            bool i = false;

            if (cmbFiltrar.Text.Equals("Seleccione un filtro"))
                return i;

            if (!txtValor.Text.Equals(""))
                i = true;

            return i;
        }

        public void cargarFiltro()
        {
            cmbFiltrar.Items.Add("Seleccione un filtro");
            //cmbFiltrar.Items.Add("ID Docente");
            cmbFiltrar.Items.Add("Nombre");
            cmbFiltrar.Items.Add("Apellido");
            cmbFiltrar.Items.Add("Genero");
            cmbFiltrar.Items.Add("Direccion");
            cmbFiltrar.Items.Add("Dui");
            cmbFiltrar.Items.Add("Nit");
            cmbFiltrar.Items.Add("Correo");
            cmbFiltrar.Items.Add("Telefono");
            cmbFiltrar.Items.Add("Usuario");
            cmbFiltrar.Items.Add("ID Rol");
            cmbFiltrar.SelectedIndex = 0;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (validacionesFiltro())
                filtarPor(cmbFiltrar.Text, txtValor.Text);

            if(txtValor.Text.Equals("")){
                errorProvider1.SetError(txtValor,"Debe de llenar el campo");
            }
            else
            {
                errorProvider1.SetError(txtValor, "");
            }

        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            cargarDatos();
            limpiarCampos();
        }

        public void validarCamposString(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Space))
                e.Handled = true;
        }

        public void validarCamposNumerico(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (Char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (Char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposString(sender, e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            validarCamposString(sender, e);
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (validar)
            {
                txtTelefono.Clear();
            }
        }

        private void mktDui_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (validar)
            {
                mktDui.Clear();
            }
        }

        private void mktnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (validar)
            {
                mktnit.Clear();
            }
        }

        private void txtTelefono_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Subtract) || (e.KeyCode == Keys.Add))
                validar = true;
            else
                validar = false;
        }

        private void mktDui_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Subtract) || (e.KeyCode == Keys.Add))
                validar = true;
            else
                validar = false;
        }

        private void mktnit_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Subtract) || (e.KeyCode == Keys.Add))
                validar = true;
            else
                validar = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
