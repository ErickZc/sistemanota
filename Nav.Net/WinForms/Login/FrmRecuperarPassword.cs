﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO.Administrador;
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.BLO.Roles;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Login
{
    public partial class FrmRecuperarPassword : Form
    {
        public FrmRecuperarPassword()
        {
            InitializeComponent();
            cargarDatos();
        }

        public void cargarDatos()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Rol_Usuario> listadoRoles = new List<Rol_Usuario>();
            DaoRoles roles = new DaoRoles();
            try
            {
                listadoRoles = roles.listRoles();
                //lista.Add(0, "");
                foreach (Rol_Usuario p in listadoRoles)
                {
                    lista.Add(p.id_rol, p.rol);
                }


                cmbRol.DataSource = new BindingSource(lista, null);
                cmbRol.DisplayMember = "Value";
                cmbRol.ValueMember = "Key";

                cmbRol.SelectedIndex = -1;

            }
            catch (Exception)
            {

            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtUsuario.Text.Equals("") && !txtPassword.Text.Equals("") && !txtRepetirPassword.Text.Equals("") && !cmbRol.SelectedItem.Equals(""))
                {
                    if (txtPassword.Text.Equals(txtRepetirPassword.Text))
                    {
                        if (cmbRol.Text.Equals("Administrador"))
                        {
                            //opcion administrador
                            DaoAdministradores admin = new DaoAdministradores();
                            bool flag;

                            flag = admin.actualizarPassword(txtUsuario.Text, txtPassword.Text);

                            if (flag)
                                this.Dispose();
                            else
                                MessageBox.Show("ERROR AL ACTUALIZAR LA NUEVA CONTRASEÑA");
                        }
                        else
                        {
                            //opcion docente o invitado
                            DaoDocente admin = new DaoDocente();
                            bool flag;

                            flag = admin.actualizarPassword(txtUsuario.Text, txtPassword.Text, Convert.ToInt32(cmbRol.SelectedValue.ToString()));

                            if (flag)
                                this.Dispose();
                            else
                                MessageBox.Show("ERROR AL ACTUALIZAR LA NUEVA CONTRASEÑA");
                        }
                    }
                    else
                        MessageBox.Show("Las contraseñas no coinciden");
                }
                else
                    MessageBox.Show("Por favor no dejar campos en blanco.");

                limpiarCampos();
            }
            catch (Exception)
            {
                limpiarCampos();
            }
        }

        public void limpiarCampos()
        {
            txtUsuario.Text = "";
            txtPassword.Text = "";
            txtRepetirPassword.Text = "";
            cmbRol.SelectedIndex = 0;
        }



    }
}
