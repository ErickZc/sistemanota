﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO.Administrador;
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.BLO.Roles;
using SistemaGestionNotas.BLO.docente__grado;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Login
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            cargarDatos();
        }


        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validaciones())
                    return;


                if (cmbRol.Text.Equals("Administrador"))
                {
                    //opcion administrador
                    DaoAdministradores admin = new DaoAdministradores();
                    Administrador_Sistema administrador = new Administrador_Sistema();

                    administrador = admin.getAdministrador(txtUsuario.Text, txtPassword.Text);

                    if (administrador.id_admin != 0)
                    {
                        FrmPrincipal menuPrincipal = new FrmPrincipal(cmbRol.Text);
                        this.Dispose();
                        menuPrincipal.visualizacion();
                        menuPrincipal.ShowDialog();
                        
                    }
                    else
                    {
                        MessageBox.Show("ERROR DE INICIO DE SESION");
                    }
                }
                else
                {
                    //opcion docente o invitado
                    DaoDocente admin = new DaoDocente();
                    DaoGradoDocente enc = new DaoGradoDocente();
                    BLO.DocenteGradoMateria.DaoDGM daoDocente = new BLO.DocenteGradoMateria.DaoDGM();
                    Model.Docente administrador = new Model.Docente();
                    Model.Docente_Grado encargado = new Model.Docente_Grado();
                    Model.Docente_Materia_Grado dmg = new Model.Docente_Materia_Grado();
                    administrador = admin.getDocente(txtUsuario.Text, txtPassword.Text);


                    encargado=enc.listaDocenteGradosbyId(administrador.id_docente);
                    dmg = daoDocente.ListarDocenteMateriaGradoById(administrador.id_docente);


                    if (administrador.id_docente != 0)
                    {
                        FrmPrincipal menuPrincipal = new FrmPrincipal(cmbRol.Text);
                        this.Dispose();
                        menuPrincipal.docenteEncargado(encargado);

                        if (dmg != null)
                        {
                            menuPrincipal.docenteMateria(dmg);
                        }
                        else
                        {
                            menuPrincipal.docenteMateria(new Docente_Materia_Grado());

                        }
                        menuPrincipal.visualizacion();
                        menuPrincipal.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("ERROR DE INICIO DE SESION");
                    }
                }
                limpiarCampos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool validaciones()
        {
            if (txtUsuario.Text == "")
            {
                MessageBox.Show("\"Debe ingresar su usuario\"");
                return true;
            }
            else if (txtPassword.Text == "")
            {
                MessageBox.Show("\"Debe ingresar su contraseña\"");
                return true;
            }
            else if (cmbRol.Text.Equals(""))
            {
                MessageBox.Show("\"Debe seleccionar un rol\"");
                return true;
            }
            return false;
        }

        public void cargarDatos()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Rol_Usuario> listadoRoles = new List<Rol_Usuario>();
            DaoRoles roles = new DaoRoles();
            try
            {
                listadoRoles = roles.listRoles();
                //lista.Add(0, "");
                foreach (Rol_Usuario p in listadoRoles)
                {
                    lista.Add(p.id_rol, p.rol);
                }
                

                cmbRol.DataSource = new BindingSource(lista, null);
                cmbRol.DisplayMember = "Value";
                cmbRol.ValueMember = "Key";

                cmbRol.SelectedIndex = -1;

            }
            catch (Exception)
            {

            }
        }

        public void limpiarCampos()
        {
            txtUsuario.Text = "";
            txtPassword.Text = "";
            cmbRol.SelectedIndex = 0;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmRecuperarPassword cambiarPassword = new FrmRecuperarPassword();
            cambiarPassword.ShowDialog();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
