﻿namespace SistemaGestionNotas.WinForms.Notas
{
    partial class FrmNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmNotas));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMateria = new System.Windows.Forms.Label();
            this.cmbMateria = new System.Windows.Forms.ComboBox();
            this.cmbGrado = new System.Windows.Forms.ComboBox();
            this.lblGrado = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.cmbPeriodo = new System.Windows.Forms.ComboBox();
            this.lblPeriodo = new System.Windows.Forms.Label();
            this.PanelSecundario = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnRecargar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cmbFiltrar = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnModificar = new System.Windows.Forms.Button();
            this.grbDatosAlumno = new System.Windows.Forms.GroupBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.cmbEstudiante = new System.Windows.Forms.ComboBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtIdNota = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtExamen = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtActividad3 = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtActividad2 = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtActividad1 = new System.Windows.Forms.TextBox();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.lblPromedio = new System.Windows.Forms.Label();
            this.lblPromedioPeriodo = new System.Windows.Forms.Label();
            this.lblExamen = new System.Windows.Forms.Label();
            this.lblActividad3 = new System.Windows.Forms.Label();
            this.lblActividad2 = new System.Windows.Forms.Label();
            this.lblActividad1 = new System.Windows.Forms.Label();
            this.lblEstudiante = new System.Windows.Forms.Label();
            this.lblIdNota = new System.Windows.Forms.Label();
            this.grbDatosGrado = new System.Windows.Forms.GroupBox();
            this.rellenoPeriodo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rellenoMateria = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rellenogGrado = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PanelSecundario.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.grbDatosAlumno.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            this.grbDatosGrado.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(283, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Formulario para agregar notas";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblMateria
            // 
            this.lblMateria.AutoSize = true;
            this.lblMateria.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMateria.Location = new System.Drawing.Point(61, 126);
            this.lblMateria.Name = "lblMateria";
            this.lblMateria.Size = new System.Drawing.Size(61, 17);
            this.lblMateria.TabIndex = 2;
            this.lblMateria.Text = "Materia:";
            // 
            // cmbMateria
            // 
            this.cmbMateria.BackColor = System.Drawing.Color.White;
            this.cmbMateria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMateria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbMateria.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMateria.FormattingEnabled = true;
            this.cmbMateria.Location = new System.Drawing.Point(3, 2);
            this.cmbMateria.Name = "cmbMateria";
            this.cmbMateria.Size = new System.Drawing.Size(216, 25);
            this.cmbMateria.TabIndex = 3;
            this.cmbMateria.SelectedIndexChanged += new System.EventHandler(this.cmbMateria_SelectedIndexChanged);
            // 
            // cmbGrado
            // 
            this.cmbGrado.BackColor = System.Drawing.Color.White;
            this.cmbGrado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbGrado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGrado.FormattingEnabled = true;
            this.cmbGrado.Location = new System.Drawing.Point(4, 1);
            this.cmbGrado.Name = "cmbGrado";
            this.cmbGrado.Size = new System.Drawing.Size(121, 25);
            this.cmbGrado.TabIndex = 5;
            this.cmbGrado.SelectedIndexChanged += new System.EventHandler(this.cmbGrado_SelectedIndexChanged);
            // 
            // lblGrado
            // 
            this.lblGrado.AutoSize = true;
            this.lblGrado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrado.Location = new System.Drawing.Point(298, 126);
            this.lblGrado.Name = "lblGrado";
            this.lblGrado.Size = new System.Drawing.Size(118, 17);
            this.lblGrado.TabIndex = 4;
            this.lblGrado.Text = "Grado y Seccion:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(50, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(603, 51);
            this.label5.TabIndex = 7;
            this.label5.Text = resources.GetString("label5.Text");
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.btnSiguiente.FlatAppearance.BorderSize = 0;
            this.btnSiguiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiguiente.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiguiente.ForeColor = System.Drawing.Color.White;
            this.btnSiguiente.Location = new System.Drawing.Point(301, 195);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(140, 33);
            this.btnSiguiente.TabIndex = 8;
            this.btnSiguiente.Text = "Siguiente";
            this.btnSiguiente.UseVisualStyleBackColor = false;
            this.btnSiguiente.Click += new System.EventHandler(this.btnSiguiente_Click);
            // 
            // cmbPeriodo
            // 
            this.cmbPeriodo.BackColor = System.Drawing.Color.White;
            this.cmbPeriodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPeriodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPeriodo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPeriodo.FormattingEnabled = true;
            this.cmbPeriodo.Location = new System.Drawing.Point(3, 1);
            this.cmbPeriodo.Name = "cmbPeriodo";
            this.cmbPeriodo.Size = new System.Drawing.Size(157, 25);
            this.cmbPeriodo.TabIndex = 11;
            // 
            // lblPeriodo
            // 
            this.lblPeriodo.AutoSize = true;
            this.lblPeriodo.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeriodo.Location = new System.Drawing.Point(451, 126);
            this.lblPeriodo.Name = "lblPeriodo";
            this.lblPeriodo.Size = new System.Drawing.Size(62, 17);
            this.lblPeriodo.TabIndex = 10;
            this.lblPeriodo.Text = "Periodo:";
            // 
            // PanelSecundario
            // 
            this.PanelSecundario.Controls.Add(this.label2);
            this.PanelSecundario.Controls.Add(this.groupBox2);
            this.PanelSecundario.Controls.Add(this.groupBox1);
            this.PanelSecundario.Controls.Add(this.grbDatosAlumno);
            this.PanelSecundario.Controls.Add(this.grbDatosGrado);
            this.PanelSecundario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelSecundario.Location = new System.Drawing.Point(0, 0);
            this.PanelSecundario.Name = "PanelSecundario";
            this.PanelSecundario.Size = new System.Drawing.Size(752, 485);
            this.PanelSecundario.TabIndex = 12;
            this.PanelSecundario.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelSecundario_Paint);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Controls.Add(this.cmbPeriodo);
            this.panel3.Location = new System.Drawing.Point(451, 146);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(163, 29);
            this.panel3.TabIndex = 57;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.cmbGrado);
            this.panel1.Location = new System.Drawing.Point(297, 146);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(131, 29);
            this.panel1.TabIndex = 57;
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel9.Controls.Add(this.cmbMateria);
            this.panel9.Location = new System.Drawing.Point(53, 146);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(222, 29);
            this.panel9.TabIndex = 56;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SistemaGestionNotas.Properties.Resources.examen;
            this.pictureBox1.Location = new System.Drawing.Point(224, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(184, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(435, 24);
            this.label2.TabIndex = 23;
            this.label2.Text = "FORMULARIO DE AGREGACIÓN DE NOTAS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel16);
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.panel15);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(572, 291);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(167, 191);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filtro";
            // 
            // panel16
            // 
            this.panel16.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.recargar1;
            this.panel16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel16.Controls.Add(this.btnRecargar);
            this.panel16.Location = new System.Drawing.Point(31, 156);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(112, 29);
            this.panel16.TabIndex = 60;
            // 
            // btnRecargar
            // 
            this.btnRecargar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(160)))), ((int)(((byte)(154)))));
            this.btnRecargar.FlatAppearance.BorderSize = 0;
            this.btnRecargar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecargar.ForeColor = System.Drawing.Color.White;
            this.btnRecargar.Image = global::SistemaGestionNotas.Properties.Resources.recargar;
            this.btnRecargar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecargar.Location = new System.Drawing.Point(3, 3);
            this.btnRecargar.Name = "btnRecargar";
            this.btnRecargar.Size = new System.Drawing.Size(106, 24);
            this.btnRecargar.TabIndex = 20;
            this.btnRecargar.Text = "Recargar";
            this.btnRecargar.UseVisualStyleBackColor = false;
            this.btnRecargar.Click += new System.EventHandler(this.btnRecargar_Click);
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.txtValor);
            this.panel5.Location = new System.Drawing.Point(7, 87);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(154, 29);
            this.panel5.TabIndex = 57;
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.Color.White;
            this.txtValor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtValor.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.Location = new System.Drawing.Point(9, 6);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(135, 16);
            this.txtValor.TabIndex = 16;
            // 
            // panel15
            // 
            this.panel15.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.buscar;
            this.panel15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel15.Controls.Add(this.btnBuscar);
            this.panel15.Location = new System.Drawing.Point(31, 122);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(112, 29);
            this.panel15.TabIndex = 59;
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(215)))), ((int)(((byte)(90)))));
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Image = global::SistemaGestionNotas.Properties.Resources.lupa;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(3, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(106, 24);
            this.btnBuscar.TabIndex = 19;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Controls.Add(this.cmbFiltrar);
            this.panel4.Location = new System.Drawing.Point(4, 42);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(157, 28);
            this.panel4.TabIndex = 58;
            // 
            // cmbFiltrar
            // 
            this.cmbFiltrar.BackColor = System.Drawing.Color.White;
            this.cmbFiltrar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbFiltrar.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFiltrar.FormattingEnabled = true;
            this.cmbFiltrar.Location = new System.Drawing.Point(3, 1);
            this.cmbFiltrar.Name = "cmbFiltrar";
            this.cmbFiltrar.Size = new System.Drawing.Size(148, 25);
            this.cmbFiltrar.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Filtrar por: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Valor: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel8);
            this.groupBox1.Controls.Add(this.panel10);
            this.groupBox1.Controls.Add(this.panel6);
            this.groupBox1.Controls.Add(this.panel7);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(572, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(167, 183);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acciones";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.panel8.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Controls.Add(this.btnGuardar);
            this.panel8.Location = new System.Drawing.Point(28, 133);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(112, 31);
            this.panel8.TabIndex = 59;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnGuardar.Image = global::SistemaGestionNotas.Properties.Resources.disco_flexible__1_;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(3, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(106, 24);
            this.btnGuardar.TabIndex = 18;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.panel10.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.btnAgregar);
            this.panel10.Location = new System.Drawing.Point(28, 22);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(112, 31);
            this.panel10.TabIndex = 59;
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAgregar.Image = global::SistemaGestionNotas.Properties.Resources.boton_agregar__4_;
            this.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregar.Location = new System.Drawing.Point(3, 3);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(106, 24);
            this.btnAgregar.TabIndex = 15;
            this.btnAgregar.Text = "Nuevo";
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.panel6.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.btnEliminar);
            this.panel6.Location = new System.Drawing.Point(28, 96);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(112, 31);
            this.panel6.TabIndex = 58;
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEliminar.Image = global::SistemaGestionNotas.Properties.Resources.borrar;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(3, 4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(106, 24);
            this.btnEliminar.TabIndex = 17;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.panel7.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Controls.Add(this.btnModificar);
            this.panel7.Location = new System.Drawing.Point(28, 59);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(112, 31);
            this.panel7.TabIndex = 59;
            // 
            // btnModificar
            // 
            this.btnModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(99)))), ((int)(((byte)(177)))));
            this.btnModificar.FlatAppearance.BorderSize = 0;
            this.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnModificar.Image = global::SistemaGestionNotas.Properties.Resources.boligrafo;
            this.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificar.Location = new System.Drawing.Point(3, 3);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(106, 24);
            this.btnModificar.TabIndex = 16;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // grbDatosAlumno
            // 
            this.grbDatosAlumno.Controls.Add(this.panel18);
            this.grbDatosAlumno.Controls.Add(this.panel17);
            this.grbDatosAlumno.Controls.Add(this.panel14);
            this.grbDatosAlumno.Controls.Add(this.panel13);
            this.grbDatosAlumno.Controls.Add(this.panel12);
            this.grbDatosAlumno.Controls.Add(this.panel11);
            this.grbDatosAlumno.Controls.Add(this.dgvDatos);
            this.grbDatosAlumno.Controls.Add(this.lblPromedio);
            this.grbDatosAlumno.Controls.Add(this.lblPromedioPeriodo);
            this.grbDatosAlumno.Controls.Add(this.lblExamen);
            this.grbDatosAlumno.Controls.Add(this.lblActividad3);
            this.grbDatosAlumno.Controls.Add(this.lblActividad2);
            this.grbDatosAlumno.Controls.Add(this.lblActividad1);
            this.grbDatosAlumno.Controls.Add(this.lblEstudiante);
            this.grbDatosAlumno.Controls.Add(this.lblIdNota);
            this.grbDatosAlumno.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDatosAlumno.Location = new System.Drawing.Point(8, 103);
            this.grbDatosAlumno.Name = "grbDatosAlumno";
            this.grbDatosAlumno.Size = new System.Drawing.Size(559, 379);
            this.grbDatosAlumno.TabIndex = 14;
            this.grbDatosAlumno.TabStop = false;
            this.grbDatosAlumno.Text = "Datos de los estudiantes";
            // 
            // panel18
            // 
            this.panel18.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel18.Controls.Add(this.cmbEstudiante);
            this.panel18.Location = new System.Drawing.Point(103, 45);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(437, 29);
            this.panel18.TabIndex = 60;
            // 
            // cmbEstudiante
            // 
            this.cmbEstudiante.BackColor = System.Drawing.Color.White;
            this.cmbEstudiante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstudiante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEstudiante.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEstudiante.FormattingEnabled = true;
            this.cmbEstudiante.Location = new System.Drawing.Point(3, 3);
            this.cmbEstudiante.Name = "cmbEstudiante";
            this.cmbEstudiante.Size = new System.Drawing.Size(431, 25);
            this.cmbEstudiante.TabIndex = 3;
            // 
            // panel17
            // 
            this.panel17.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel17.Controls.Add(this.txtIdNota);
            this.panel17.Location = new System.Drawing.Point(21, 45);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(69, 29);
            this.panel17.TabIndex = 59;
            // 
            // txtIdNota
            // 
            this.txtIdNota.BackColor = System.Drawing.Color.White;
            this.txtIdNota.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIdNota.Enabled = false;
            this.txtIdNota.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdNota.Location = new System.Drawing.Point(7, 7);
            this.txtIdNota.Name = "txtIdNota";
            this.txtIdNota.Size = new System.Drawing.Size(53, 15);
            this.txtIdNota.TabIndex = 1;
            // 
            // panel14
            // 
            this.panel14.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel14.Controls.Add(this.txtExamen);
            this.panel14.Location = new System.Drawing.Point(413, 109);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(109, 29);
            this.panel14.TabIndex = 60;
            // 
            // txtExamen
            // 
            this.txtExamen.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtExamen.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExamen.Location = new System.Drawing.Point(7, 7);
            this.txtExamen.Name = "txtExamen";
            this.txtExamen.Size = new System.Drawing.Size(89, 15);
            this.txtExamen.TabIndex = 11;
            this.txtExamen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExamen_KeyPress);
            // 
            // panel13
            // 
            this.panel13.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel13.Controls.Add(this.txtActividad3);
            this.panel13.Location = new System.Drawing.Point(285, 109);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(109, 29);
            this.panel13.TabIndex = 60;
            // 
            // txtActividad3
            // 
            this.txtActividad3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtActividad3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActividad3.Location = new System.Drawing.Point(7, 7);
            this.txtActividad3.Name = "txtActividad3";
            this.txtActividad3.Size = new System.Drawing.Size(89, 15);
            this.txtActividad3.TabIndex = 9;
            this.txtActividad3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtActividad3_KeyPress);
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel12.Controls.Add(this.txtActividad2);
            this.panel12.Location = new System.Drawing.Point(158, 109);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(109, 29);
            this.panel12.TabIndex = 59;
            // 
            // txtActividad2
            // 
            this.txtActividad2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtActividad2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActividad2.Location = new System.Drawing.Point(7, 7);
            this.txtActividad2.Name = "txtActividad2";
            this.txtActividad2.Size = new System.Drawing.Size(89, 15);
            this.txtActividad2.TabIndex = 7;
            this.txtActividad2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtActividad2_KeyPress);
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.campo;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.Controls.Add(this.txtActividad1);
            this.panel11.Location = new System.Drawing.Point(23, 109);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(109, 29);
            this.panel11.TabIndex = 58;
            // 
            // txtActividad1
            // 
            this.txtActividad1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtActividad1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActividad1.Location = new System.Drawing.Point(7, 7);
            this.txtActividad1.Name = "txtActividad1";
            this.txtActividad1.Size = new System.Drawing.Size(93, 15);
            this.txtActividad1.TabIndex = 5;
            this.txtActividad1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtActividad1_KeyPress);
            this.txtActividad1.Validating += new System.ComponentModel.CancelEventHandler(this.txtActividad1_Validating);
            // 
            // dgvDatos
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvDatos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDatos.BackgroundColor = System.Drawing.Color.White;
            this.dgvDatos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDatos.GridColor = System.Drawing.Color.White;
            this.dgvDatos.Location = new System.Drawing.Point(21, 161);
            this.dgvDatos.MultiSelect = false;
            this.dgvDatos.Name = "dgvDatos";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvDatos.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDatos.Size = new System.Drawing.Size(517, 203);
            this.dgvDatos.TabIndex = 20;
            this.dgvDatos.Click += new System.EventHandler(this.dgvDatos_Click);
            // 
            // lblPromedio
            // 
            this.lblPromedio.AutoSize = true;
            this.lblPromedio.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromedio.Location = new System.Drawing.Point(165, 141);
            this.lblPromedio.Name = "lblPromedio";
            this.lblPromedio.Size = new System.Drawing.Size(25, 17);
            this.lblPromedio.TabIndex = 13;
            this.lblPromedio.Text = "0.0";
            // 
            // lblPromedioPeriodo
            // 
            this.lblPromedioPeriodo.AutoSize = true;
            this.lblPromedioPeriodo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromedioPeriodo.Location = new System.Drawing.Point(20, 141);
            this.lblPromedioPeriodo.Name = "lblPromedioPeriodo";
            this.lblPromedioPeriodo.Size = new System.Drawing.Size(140, 16);
            this.lblPromedioPeriodo.TabIndex = 12;
            this.lblPromedioPeriodo.Text = "Promedio del periodo:";
            // 
            // lblExamen
            // 
            this.lblExamen.AutoSize = true;
            this.lblExamen.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExamen.Location = new System.Drawing.Point(410, 87);
            this.lblExamen.Name = "lblExamen";
            this.lblExamen.Size = new System.Drawing.Size(92, 16);
            this.lblExamen.TabIndex = 10;
            this.lblExamen.Text = "Examen (40%)";
            // 
            // lblActividad3
            // 
            this.lblActividad3.AutoSize = true;
            this.lblActividad3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActividad3.Location = new System.Drawing.Point(282, 87);
            this.lblActividad3.Name = "lblActividad3";
            this.lblActividad3.Size = new System.Drawing.Size(112, 16);
            this.lblActividad3.TabIndex = 8;
            this.lblActividad3.Text = "Actividad 3 (20%)";
            // 
            // lblActividad2
            // 
            this.lblActividad2.AutoSize = true;
            this.lblActividad2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActividad2.Location = new System.Drawing.Point(155, 87);
            this.lblActividad2.Name = "lblActividad2";
            this.lblActividad2.Size = new System.Drawing.Size(112, 16);
            this.lblActividad2.TabIndex = 6;
            this.lblActividad2.Text = "Actividad 2 (20%)";
            // 
            // lblActividad1
            // 
            this.lblActividad1.AutoSize = true;
            this.lblActividad1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActividad1.Location = new System.Drawing.Point(20, 87);
            this.lblActividad1.Name = "lblActividad1";
            this.lblActividad1.Size = new System.Drawing.Size(112, 16);
            this.lblActividad1.TabIndex = 4;
            this.lblActividad1.Text = "Actividad 1 (20%)";
            // 
            // lblEstudiante
            // 
            this.lblEstudiante.AutoSize = true;
            this.lblEstudiante.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstudiante.Location = new System.Drawing.Point(100, 26);
            this.lblEstudiante.Name = "lblEstudiante";
            this.lblEstudiante.Size = new System.Drawing.Size(72, 16);
            this.lblEstudiante.TabIndex = 2;
            this.lblEstudiante.Text = "Estudiante: ";
            // 
            // lblIdNota
            // 
            this.lblIdNota.AutoSize = true;
            this.lblIdNota.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdNota.Location = new System.Drawing.Point(20, 26);
            this.lblIdNota.Name = "lblIdNota";
            this.lblIdNota.Size = new System.Drawing.Size(57, 16);
            this.lblIdNota.TabIndex = 0;
            this.lblIdNota.Text = "ID Nota: ";
            // 
            // grbDatosGrado
            // 
            this.grbDatosGrado.Controls.Add(this.rellenoPeriodo);
            this.grbDatosGrado.Controls.Add(this.label3);
            this.grbDatosGrado.Controls.Add(this.rellenoMateria);
            this.grbDatosGrado.Controls.Add(this.label4);
            this.grbDatosGrado.Controls.Add(this.rellenogGrado);
            this.grbDatosGrado.Controls.Add(this.label9);
            this.grbDatosGrado.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDatosGrado.Location = new System.Drawing.Point(8, 51);
            this.grbDatosGrado.Name = "grbDatosGrado";
            this.grbDatosGrado.Size = new System.Drawing.Size(559, 46);
            this.grbDatosGrado.TabIndex = 0;
            this.grbDatosGrado.TabStop = false;
            this.grbDatosGrado.Text = "Datos del Grado";
            // 
            // rellenoPeriodo
            // 
            this.rellenoPeriodo.AutoSize = true;
            this.rellenoPeriodo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rellenoPeriodo.Location = new System.Drawing.Point(454, 22);
            this.rellenoPeriodo.Name = "rellenoPeriodo";
            this.rellenoPeriodo.Size = new System.Drawing.Size(64, 17);
            this.rellenoPeriodo.TabIndex = 5;
            this.rellenoPeriodo.Text = "Relleno ...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(400, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Periodo: ";
            // 
            // rellenoMateria
            // 
            this.rellenoMateria.AutoSize = true;
            this.rellenoMateria.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rellenoMateria.Location = new System.Drawing.Point(217, 22);
            this.rellenoMateria.Name = "rellenoMateria";
            this.rellenoMateria.Size = new System.Drawing.Size(64, 17);
            this.rellenoMateria.TabIndex = 3;
            this.rellenoMateria.Text = "Relleno ...";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(164, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Materia: ";
            // 
            // rellenogGrado
            // 
            this.rellenogGrado.AutoSize = true;
            this.rellenogGrado.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rellenogGrado.Location = new System.Drawing.Point(60, 22);
            this.rellenogGrado.Name = "rellenogGrado";
            this.rellenogGrado.Size = new System.Drawing.Size(64, 17);
            this.rellenogGrado.TabIndex = 1;
            this.rellenogGrado.Text = "Relleno ...";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Grado: ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.PanelSecundario);
            this.panel2.Controls.Add(this.btnSiguiente);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.lblGrado);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblPeriodo);
            this.panel2.Controls.Add(this.lblMateria);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(752, 485);
            this.panel2.TabIndex = 15;
            // 
            // FrmNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 485);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmNotas";
            this.Text = "Agregar Notas";
            this.Load += new System.EventHandler(this.frmAgregarNota_Load);
            this.PanelSecundario.ResumeLayout(false);
            this.PanelSecundario.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.grbDatosAlumno.ResumeLayout(false);
            this.grbDatosAlumno.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            this.grbDatosGrado.ResumeLayout(false);
            this.grbDatosGrado.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMateria;
        private System.Windows.Forms.ComboBox cmbMateria;
        private System.Windows.Forms.ComboBox cmbGrado;
        private System.Windows.Forms.Label lblGrado;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbPeriodo;
        private System.Windows.Forms.Label lblPeriodo;
        private System.Windows.Forms.Panel PanelSecundario;
        private System.Windows.Forms.GroupBox grbDatosGrado;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label rellenoPeriodo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label rellenoMateria;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label rellenogGrado;
        private System.Windows.Forms.GroupBox grbDatosAlumno;
        private System.Windows.Forms.TextBox txtIdNota;
        private System.Windows.Forms.Label lblIdNota;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.ComboBox cmbFiltrar;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblPromedio;
        private System.Windows.Forms.Label lblPromedioPeriodo;
        private System.Windows.Forms.TextBox txtExamen;
        private System.Windows.Forms.Label lblExamen;
        private System.Windows.Forms.TextBox txtActividad3;
        private System.Windows.Forms.Label lblActividad3;
        private System.Windows.Forms.TextBox txtActividad2;
        private System.Windows.Forms.Label lblActividad2;
        private System.Windows.Forms.TextBox txtActividad1;
        private System.Windows.Forms.Label lblActividad1;
        private System.Windows.Forms.ComboBox cmbEstudiante;
        private System.Windows.Forms.Label lblEstudiante;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnRecargar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
    }
}