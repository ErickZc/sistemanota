﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.Model;
using SistemaGestionNotas.BLO.Notas;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.BLO.Materia;
using SistemaGestionNotas.BLO.Estudiante;
using System.Reflection;
using SistemaGestionNotas.BLO;

namespace SistemaGestionNotas.WinForms.Notas
{
    public partial class FrmNotas : Form
    {

        int? idGrupo;

        public FrmNotas(Docente_Materia_Grado docente)
        {
            InitializeComponent();
            idGrupo = docente.id_docente;
            PanelSecundario.Visible = false;
            cargarComboMateria();
            cargarFiltro();
            habilitarBotones();
            habilitarCaja(false);
            Model.Docente d = new Model.Docente();
            

        }

        bool agregar = false;
        bool editar = false;
        

        

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmAgregarNota_Load(object sender, EventArgs e)
        {
            //panelPrincipal.Visible = true;
            PanelSecundario.Visible = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (!cmbMateria.Text.Equals("") && !cmbGrado.Text.Equals("") && !cmbPeriodo.Text.Equals(""))
            //if (cmbMateria.Text.Equals("") && cmbGrado.Text.Equals("") && cmbPeriodo.Text.Equals(""))
            {
                //panelPrincipal.Visible = false;
                PanelSecundario.Visible = true;

                cargarDatosGrado();
                cargarDataGrid();
                cargarComboEstudiante();
            }
            else
            {
                MessageBox.Show("Primero debe seleccionar registros");
            }
        }

        private void cmbMateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbMateria.Text.Equals(""))
            {
                cargarComboGrado(cmbMateria.SelectedIndex + 1);
                cargarComboPeriodo("");
            }
            else
            {
                cmbGrado.DataSource = null;
                cmbPeriodo.DataSource = null;
                cmbGrado.Items.Clear();
                cmbPeriodo.Items.Clear();
            }
        }

        private void cmbGrado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cmbGrado.Text.Equals(""))
            {
                cargarComboPeriodo(cmbGrado.Text);
            }
            else
            {
                cmbPeriodo.DataSource = null;
                cmbPeriodo.Items.Clear();
            }
        }

        public void filtarPor(string columna, string valor)
        {
            int periodo = int.Parse(cmbPeriodo.SelectedValue.ToString());
            int materia = int.Parse(cmbMateria.SelectedValue.ToString());
            int grado = int.Parse(cmbGrado.SelectedValue.ToString());

            DaoNotas notas = new DaoNotas();
            Array lista = null;

            lista = notas.filtrarPor(cmbFiltrar.Text, txtValor.Text, periodo, materia, grado);

            dgvDatos.DataSource = lista;
            dgvDatos.Columns["id_estudiante"].Visible = false;
            dgvDatos.Columns["id_nota"].Visible = false;

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            editar = false;
            deshabilitarBotones("Agregar");
            habilitarCaja(true);
            limpiar();
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            cargarCajas();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la modificación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                editar = true;
                habilitarCaja(true);
                deshabilitarBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la eliminación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                confirmarEliminacion();
                limpiar();
            }
            else
                MessageBox.Show("Debe seleccionar un registro");

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (validacionesFiltro())
                filtarPor(cmbFiltrar.Text, txtValor.Text);
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            cargarDataGrid();
            limpiar();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cantidadNota())
                {
                    if (agregar == true)
                    {
                        agregarNota();
                        agregar = false;
                        editar = false;
                    }
                    else if (editar == true)
                    {
                        modificarNota();
                        agregar = false;
                        editar = false;
                    }
                    limpiar();
                    cargarDataGrid();
                    habilitarCaja(false);
                    habilitarBotones();
                }
                else
                    MessageBox.Show("Las notas deben estar entre 0 a 10");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ha ocurrido el siguiente error: " + ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void txtActividad1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtActividad2_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtActividad3_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtExamen_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        #region

        public bool cantidadNota()
        {
            bool cantidad = false;

            if ((double.Parse(txtActividad1.Text) >= 0
                && double.Parse(txtActividad1.Text) <= 10)
                && (double.Parse(txtActividad2.Text) >= 0
                && double.Parse(txtActividad2.Text) <= 10)
                && (double.Parse(txtActividad3.Text) >= 0
                && double.Parse(txtActividad3.Text) <= 10)
                && (double.Parse(txtExamen.Text) >= 0
                && double.Parse(txtExamen.Text) <= 10))
            {
                cantidad = true;
            }

            return cantidad;

        }

        public void deshabilitarBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
            }
        }

        public void habilitarBotones()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnModificar.Enabled = true;
            btnAgregar.Enabled = true;
            //btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnModificar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnEliminar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnAgregar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnGuardar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
        }

        public void habilitarCaja(bool i)
        {
            cmbEstudiante.Enabled = i;
            txtActividad1.Enabled = i;
            txtActividad2.Enabled = i;
            txtActividad3.Enabled = i;
            txtExamen.Enabled = i;

        }

        public void cargarComboMateria()
        {
            DaoMateria materia = new DaoMateria();
            Array lista = null;

            lista = materia.listarMateriaById(idGrupo); //Validar el id_docente

            if (lista.Length > 0)
            {
                cmbMateria.DataSource = lista;
                cmbMateria.DisplayMember = "nombre_materia";
                cmbMateria.ValueMember = "id_materia";
            }

            if (cmbMateria.Items.Count >= 1)
            {
                cmbMateria.SelectedIndex = -1;
            }

        }

        public bool validacionesFiltro()
        {
            bool i = false;

            if (cmbFiltrar.Text.Equals("Seleccione un filtro"))
            {
                return i;
            }

            if (!txtValor.Text.Equals(""))
            {
                i = true;
            }

            return i;
        }

        public void confirmarEliminacion()
        {
            // Envía confirmación al usuario para eliminar el registro
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Estas seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    DaoNotas notas = new DaoNotas();
                    notas.eliminarNota(int.Parse(txtIdNota.Text));
                    cargarDataGrid();
                    MessageBox.Show("Se ha eliminado correctamente");
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro");
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void limpiar()
        {
            txtIdNota.Clear();
            cmbEstudiante.SelectedIndex = -1;
            txtActividad1.Clear();
            txtActividad2.Clear();
            txtActividad3.Clear();
            txtExamen.Clear();
            cmbFiltrar.SelectedIndex = 0;
            txtValor.Clear();
        }

        public void modificarNota()
        {
            DaoNotas notas = new DaoNotas();

            int id_estudiante = int.Parse(cmbEstudiante.SelectedValue.ToString());
            int id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
            double act1 = double.Parse(txtActividad1.Text);
            double act2 = double.Parse(txtActividad2.Text);
            double act3 = double.Parse(txtActividad3.Text);
            double examen = double.Parse(txtExamen.Text);
            int id_periodo = int.Parse(cmbPeriodo.SelectedValue.ToString());
            int id_nota = int.Parse(txtIdNota.Text);

            notas.modificarNota(id_nota, id_estudiante, id_materia, act1, act2, act3, examen, id_periodo);
            cargarDataGrid();

        }

        public void agregarNota()
        {
            DaoNotas notas = new DaoNotas();

            int id_estudiante = int.Parse(cmbEstudiante.SelectedValue.ToString());
            int id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
            double act1 = double.Parse(txtActividad1.Text);
            double act2 = double.Parse(txtActividad2.Text);
            double act3 = double.Parse(txtActividad3.Text);
            double examen = double.Parse(txtExamen.Text);
            int id_periodo = int.Parse(cmbPeriodo.SelectedValue.ToString());

            notas.agregarNota(id_estudiante, id_materia, act1, act2, act3, examen, id_periodo);
            cargarDataGrid();

        }

        public void cargarDataGrid()
        {
        
            int periodo = int.Parse(cmbPeriodo.SelectedValue.ToString());
            int materia = int.Parse(cmbMateria.SelectedValue.ToString());
            int grado = int.Parse(cmbGrado.SelectedValue.ToString());
        
            DaoNotas notas = new DaoNotas();
            Array load = null;
        
            load = notas.verNotasDocente(periodo, materia, grado);

            dgvDatos.DataSource = load;
            dgvDatos.Columns["id_estudiante"].Visible = false;
            dgvDatos.Columns["id_nota"].Visible = false;
        
            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;
        
            }
        }

        public void cargarComboEstudiante()
        {
            Model.Nota n = new Model.Nota();
            DaoEstudiante estudiante = new DaoEstudiante();
            Array loadEstudiante = null;

            int _grado = int.Parse(cmbGrado.SelectedValue.ToString());
            loadEstudiante = estudiante.cmbListarEstudiante(_grado);

            if (loadEstudiante.Length > 0)
            {
                cmbEstudiante.DataSource = loadEstudiante;
                cmbEstudiante.DisplayMember = "valor";
                cmbEstudiante.ValueMember = "id_estudiante";
            }

            if (cmbEstudiante.Items.Count >= 1)
            {
                cmbEstudiante.SelectedIndex = -1;
            }

        }

        public void cargarCajas()
        {

            if (dgvDatos.RowCount > 0)
            {
                txtIdNota.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_nota"].Value.ToString();
                txtActividad1.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["actividad1"].Value.ToString();
                txtActividad2.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["actividad2"].Value.ToString();
                txtActividad3.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["actividad3"].Value.ToString();
                txtExamen.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["examen"].Value.ToString();
                //cmbEstudiante.SelectedIndex = int.Parse(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_estudiante"].Value.ToString()) -1;
                //cmbEstudiante.SelectedIndex = cmbEstudiante.Items.IndexOf(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_nota"].Value.ToString());
                cmbEstudiante.SelectedIndex = cmbEstudiante.FindStringExact(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["nombre"].Value.ToString() + " " + dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["apellido"].Value.ToString());
            }

        }

        public void cargarComboGrado(int valor)
        {
            DaoGrado grado = new DaoGrado();
            Array loadGrado = null;

            loadGrado = grado.cargarComboGrado(cmbMateria.SelectedIndex + 1,1); //Tenes que validarlo el id_docente
            if (loadGrado.Length > 0)
            {
                //cmbMateria[] = "";
                cmbGrado.DataSource = loadGrado;
                cmbGrado.DisplayMember = "valor";
                cmbGrado.ValueMember = "id_grado";
            }

            if (cmbGrado.Items.Count >= 1)
            {
                cmbGrado.SelectedIndex = -1;
            }
        }

        public void cargarDatosGrado()
        {
            rellenoMateria.Text = cmbMateria.Text;
            rellenogGrado.Text = cmbGrado.Text;
            rellenoPeriodo.Text = cmbPeriodo.Text;
        }

        public void cargarComboPeriodo(string combo1)
        {
            if (!combo1.Equals(""))
            {
                DaoPeriodo periodo = new DaoPeriodo();

                var loadPeriodo = periodo.listaPerido().ToList();

                if (loadPeriodo.Count > 0)
                {
                    cmbPeriodo.DataSource = loadPeriodo;
                    cmbPeriodo.DisplayMember = "periodo1";
                    cmbPeriodo.ValueMember = "id_periodo";
                }

                if (cmbPeriodo.Items.Count >= 1)
                {
                    cmbPeriodo.SelectedIndex = -1;
                }
            }

        }

        public void cargarFiltro()
        {
            cmbFiltrar.Items.Add("Seleccione un filtro");
            cmbFiltrar.Items.Add("nombre");
            cmbFiltrar.Items.Add("apellido");
            cmbFiltrar.SelectedIndex = 0;
        }

        #endregion

        private void txtActividad1_Validating(object sender, CancelEventArgs e)
        {
        }

        private void PanelSecundario_Paint(object sender, PaintEventArgs e)
        {

        }


    }
}
