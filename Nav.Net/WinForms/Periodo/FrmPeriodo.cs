﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Notas;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Periodo
{
    public partial class FrmPeriodo : Form
    {
        DaoPeriodo daoAdmin = new DaoPeriodo();
        BaseBlo blo = new BaseBlo();

        public FrmPeriodo()
        {
            InitializeComponent();
            deshabilitar();
            cargarDatos();
        }

        public void deshabilitar()
        {
            txtNombre.Enabled = false;
            btnGuardar.Enabled = false;
            btnModificar.Enabled = false;
            btnEliminar.Enabled = false;
        }

        public void habilitar()
        {
            txtNombre.Enabled = true;
            btnGuardar.Enabled = true;
            btnModificar.Enabled = true;
            btnEliminar.Enabled = true;
        }

        public void limpiar()
        {
            txtId.Text = "";
            txtNombre.Text = "";
        }

        public void cargarDatos()
        {
            List<Model.Periodo> admin = new List<Model.Periodo>();
            string[] Columns = new[] { "ID Periodo", "Nombre"};
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                admin = daoAdmin.listaPerido();
                //limpiar valores del DataGridView
                dgvDatos.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvDatos.DataSource = blo.toDataTable(admin.Select(x => new
                {
                    x.id_periodo,
                    x.periodo1,
                }).ToList(), Columns);

                /// evita que se puedan agregar nuevas filas en los dataGridView
                dgvDatos.AllowUserToAddRows = false;
                /// evita que se pueda editar en los dataGridView
                dgvDatos.EditMode = DataGridViewEditMode.EditProgrammatically;
                /// negrita para las cabezeras de los dataGridView
                dgvDatos.ColumnHeadersDefaultCellStyle.Font = new Font(dgvDatos.Font, FontStyle.Bold);
                /// remueve el espacio innecesario
                dgvDatos.BackgroundColor = Color.FromArgb(182, 181, 180);
                //Marca todo el row del datagrid
                dgvDatos.SelectionMode = DataGridViewSelectionMode.CellSelect;
                ///aplica formato a letras
                dgvDatos.Font = new Font(new FontFamily("Times New Roman"), 10.0f, FontStyle.Bold);
                //Marcara toda la linea del DataGridView
                dgvDatos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                //hace que las columnas se ajusten al texto de la celda
                dgvDatos.AutoResizeColumns();
                //no permite add rows por medio del usuario
                dgvDatos.AllowUserToAddRows = false;
                //Ajusta el tamaño de las celdas
                this.dgvDatos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                //Ocultara el campo ID Docente del DataGridView
                this.dgvDatos.Columns["ID Periodo"].Visible = false;
                dgvDatos.CurrentCell = dgvDatos.Rows[0].Cells[1];
            }
            catch (Exception)
            {

            }
        }

        public Model.Periodo setearValores()
        {
            try
            {
                Model.Periodo admin = new Model.Periodo();

                admin.periodo1 = txtNombre.Text;
                admin.estado = "OPEN";

                return admin;

            }
            catch (Exception)
            {
                return new Model.Periodo();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            limpiar();
            habilitar();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (daoAdmin.insertarPeriodo(setearValores()))
                    MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                limpiar();
                cargarDatos();
            }
            catch (Exception)
            {

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea modificar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoAdmin.modificarPeriodo(setearValores(), Convert.ToInt32(txtId.Text)))
                        MessageBox.Show("Información Modificada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Modificar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiar();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoAdmin.eliminarPeriodo(Convert.ToInt32(txtId.Text)))
                        MessageBox.Show("Información Eliminada correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    limpiar();
                    cargarDatos();
                }
            }
            catch (Exception)
            {

            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            try
            {

                //Recuperacion de valores del DataGridView para asignarlos a los campos
                txtId.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombre.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells[1].Value.ToString();
            }
            catch (Exception)
            {

            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void FrmPeriodo_Load(object sender, EventArgs e)
        {

        }
    }
}
