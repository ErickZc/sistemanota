﻿namespace SistemaGestionNotas.WinForms
{
    partial class FrmMenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenuPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.modulosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.docentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudianteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administradorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gradoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seccionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modulosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingresarNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.matricularAlumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarDocenteResponsableAGradoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarContraseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notasDelPeriodoPorGradoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosMatriculadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boletaDeNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.docentesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.materiasPorDocenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encargadosResponsablesDelGradoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modulosToolStripMenuItem,
            this.modulosToolStripMenuItem1,
            this.reporteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // modulosToolStripMenuItem
            // 
            this.modulosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.docentesToolStripMenuItem,
            this.materiaToolStripMenuItem,
            this.estudianteToolStripMenuItem,
            this.administradorToolStripMenuItem,
            this.gradoToolStripMenuItem,
            this.periodoToolStripMenuItem,
            this.seccionToolStripMenuItem});
            this.modulosToolStripMenuItem.Name = "modulosToolStripMenuItem";
            this.modulosToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
            this.modulosToolStripMenuItem.Text = "Mantenimiento";
            this.modulosToolStripMenuItem.Click += new System.EventHandler(this.modulosToolStripMenuItem_Click);
            // 
            // docentesToolStripMenuItem
            // 
            this.docentesToolStripMenuItem.Name = "docentesToolStripMenuItem";
            this.docentesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.docentesToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.docentesToolStripMenuItem.Text = "Docente";
            this.docentesToolStripMenuItem.Click += new System.EventHandler(this.docentesToolStripMenuItem_Click);
            // 
            // materiaToolStripMenuItem
            // 
            this.materiaToolStripMenuItem.Name = "materiaToolStripMenuItem";
            this.materiaToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.materiaToolStripMenuItem.Text = "Materia";
            this.materiaToolStripMenuItem.Click += new System.EventHandler(this.materiaToolStripMenuItem_Click);
            // 
            // estudianteToolStripMenuItem
            // 
            this.estudianteToolStripMenuItem.Name = "estudianteToolStripMenuItem";
            this.estudianteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.estudianteToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.estudianteToolStripMenuItem.Text = "Estudiante";
            this.estudianteToolStripMenuItem.Click += new System.EventHandler(this.estudianteToolStripMenuItem_Click);
            // 
            // administradorToolStripMenuItem
            // 
            this.administradorToolStripMenuItem.Name = "administradorToolStripMenuItem";
            this.administradorToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.administradorToolStripMenuItem.Text = "Administrador";
            this.administradorToolStripMenuItem.Click += new System.EventHandler(this.administradorToolStripMenuItem_Click);
            // 
            // gradoToolStripMenuItem
            // 
            this.gradoToolStripMenuItem.Name = "gradoToolStripMenuItem";
            this.gradoToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.gradoToolStripMenuItem.Text = "Grado";
            this.gradoToolStripMenuItem.Click += new System.EventHandler(this.gradoToolStripMenuItem_Click);
            // 
            // periodoToolStripMenuItem
            // 
            this.periodoToolStripMenuItem.Name = "periodoToolStripMenuItem";
            this.periodoToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.periodoToolStripMenuItem.Text = "Periodo";
            this.periodoToolStripMenuItem.Click += new System.EventHandler(this.periodoToolStripMenuItem_Click);
            // 
            // seccionToolStripMenuItem
            // 
            this.seccionToolStripMenuItem.Name = "seccionToolStripMenuItem";
            this.seccionToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.seccionToolStripMenuItem.Text = "Seccion";
            this.seccionToolStripMenuItem.Click += new System.EventHandler(this.seccionToolStripMenuItem_Click);
            // 
            // modulosToolStripMenuItem1
            // 
            this.modulosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingresarNotasToolStripMenuItem,
            this.matricularAlumnosToolStripMenuItem,
            this.asignarDocenteResponsableAGradoToolStripMenuItem,
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem,
            this.cambiarContraseToolStripMenuItem});
            this.modulosToolStripMenuItem1.Name = "modulosToolStripMenuItem1";
            this.modulosToolStripMenuItem1.Size = new System.Drawing.Size(66, 20);
            this.modulosToolStripMenuItem1.Text = "Modulos";
            // 
            // ingresarNotasToolStripMenuItem
            // 
            this.ingresarNotasToolStripMenuItem.Name = "ingresarNotasToolStripMenuItem";
            this.ingresarNotasToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.ingresarNotasToolStripMenuItem.Text = "Ingresar notas";
            this.ingresarNotasToolStripMenuItem.Click += new System.EventHandler(this.ingresarNotasToolStripMenuItem_Click);
            // 
            // matricularAlumnosToolStripMenuItem
            // 
            this.matricularAlumnosToolStripMenuItem.Name = "matricularAlumnosToolStripMenuItem";
            this.matricularAlumnosToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.matricularAlumnosToolStripMenuItem.Text = "Matricular alumnos";
            this.matricularAlumnosToolStripMenuItem.Click += new System.EventHandler(this.matricularAlumnosToolStripMenuItem_Click);
            // 
            // asignarDocenteResponsableAGradoToolStripMenuItem
            // 
            this.asignarDocenteResponsableAGradoToolStripMenuItem.Name = "asignarDocenteResponsableAGradoToolStripMenuItem";
            this.asignarDocenteResponsableAGradoToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.asignarDocenteResponsableAGradoToolStripMenuItem.Text = "Asignar docente responsable a un grado";
            this.asignarDocenteResponsableAGradoToolStripMenuItem.Click += new System.EventHandler(this.asignarDocenteResponsableAGradoToolStripMenuItem_Click);
            // 
            // asignarMateriasAImpartirALosDocentesToolStripMenuItem
            // 
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem.Name = "asignarMateriasAImpartirALosDocentesToolStripMenuItem";
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem.Text = "Asignar materias a impartir a los docentes";
            this.asignarMateriasAImpartirALosDocentesToolStripMenuItem.Click += new System.EventHandler(this.asignarMateriasAImpartirALosDocentesToolStripMenuItem_Click);
            // 
            // cambiarContraseToolStripMenuItem
            // 
            this.cambiarContraseToolStripMenuItem.Name = "cambiarContraseToolStripMenuItem";
            this.cambiarContraseToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.cambiarContraseToolStripMenuItem.Text = "Cambiar contraseña";
            this.cambiarContraseToolStripMenuItem.Click += new System.EventHandler(this.cambiarContraseToolStripMenuItem_Click);
            // 
            // reporteToolStripMenuItem
            // 
            this.reporteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notasDelPeriodoPorGradoToolStripMenuItem,
            this.alumnosMatriculadosToolStripMenuItem,
            this.boletaDeNotasToolStripMenuItem,
            this.docentesToolStripMenuItem1,
            this.materiasPorDocenteToolStripMenuItem,
            this.encargadosResponsablesDelGradoToolStripMenuItem});
            this.reporteToolStripMenuItem.Name = "reporteToolStripMenuItem";
            this.reporteToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.reporteToolStripMenuItem.Text = "Reporte";
            this.reporteToolStripMenuItem.Click += new System.EventHandler(this.reporteToolStripMenuItem_Click);
            // 
            // notasDelPeriodoPorGradoToolStripMenuItem
            // 
            this.notasDelPeriodoPorGradoToolStripMenuItem.Name = "notasDelPeriodoPorGradoToolStripMenuItem";
            this.notasDelPeriodoPorGradoToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.notasDelPeriodoPorGradoToolStripMenuItem.Text = "Notas del periodo por grado";
            this.notasDelPeriodoPorGradoToolStripMenuItem.Click += new System.EventHandler(this.notasDelPeriodoPorGradoToolStripMenuItem_Click);
            // 
            // alumnosMatriculadosToolStripMenuItem
            // 
            this.alumnosMatriculadosToolStripMenuItem.Name = "alumnosMatriculadosToolStripMenuItem";
            this.alumnosMatriculadosToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.alumnosMatriculadosToolStripMenuItem.Text = "Alumnos matriculados";
            this.alumnosMatriculadosToolStripMenuItem.Click += new System.EventHandler(this.alumnosMatriculadosToolStripMenuItem_Click);
            // 
            // boletaDeNotasToolStripMenuItem
            // 
            this.boletaDeNotasToolStripMenuItem.Name = "boletaDeNotasToolStripMenuItem";
            this.boletaDeNotasToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.boletaDeNotasToolStripMenuItem.Text = "Boleta de notas por estudiante";
            this.boletaDeNotasToolStripMenuItem.Click += new System.EventHandler(this.boletaDeNotasToolStripMenuItem_Click);
            // 
            // docentesToolStripMenuItem1
            // 
            this.docentesToolStripMenuItem1.Name = "docentesToolStripMenuItem1";
            this.docentesToolStripMenuItem1.Size = new System.Drawing.Size(259, 22);
            this.docentesToolStripMenuItem1.Text = "Docentes";
            this.docentesToolStripMenuItem1.Click += new System.EventHandler(this.docentesToolStripMenuItem1_Click);
            // 
            // materiasPorDocenteToolStripMenuItem
            // 
            this.materiasPorDocenteToolStripMenuItem.Name = "materiasPorDocenteToolStripMenuItem";
            this.materiasPorDocenteToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.materiasPorDocenteToolStripMenuItem.Text = "Materias por docente";
            this.materiasPorDocenteToolStripMenuItem.Click += new System.EventHandler(this.materiasPorDocenteToolStripMenuItem_Click);
            // 
            // encargadosResponsablesDelGradoToolStripMenuItem
            // 
            this.encargadosResponsablesDelGradoToolStripMenuItem.Name = "encargadosResponsablesDelGradoToolStripMenuItem";
            this.encargadosResponsablesDelGradoToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.encargadosResponsablesDelGradoToolStripMenuItem.Text = "Encargados responsables del grado";
            this.encargadosResponsablesDelGradoToolStripMenuItem.Click += new System.EventHandler(this.encargadosResponsablesDelGradoToolStripMenuItem_Click);
            // 
            // FrmMenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMenuPrincipal";
            this.Text = "FrmMenuPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMenuPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem modulosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem docentesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudianteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modulosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ingresarNotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem matricularAlumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarDocenteResponsableAGradoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarMateriasAImpartirALosDocentesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarContraseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notasDelPeriodoPorGradoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnosMatriculadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boletaDeNotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem docentesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem materiasPorDocenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem encargadosResponsablesDelGradoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administradorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gradoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seccionToolStripMenuItem;
    }
}