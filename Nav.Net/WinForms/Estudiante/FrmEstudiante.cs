﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Estudiante;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.WinForms.Estudiante
{
    public partial class FrmEstudiante : Form
    {
        bool agregar = false;
        bool modificar = false;
        private string[] Columns = new[] { "ID", "Nombres", "Apellidos", "Dirección", "Responsable",
        "Telefono","Estado de promedio","Fecha de creación","Estado","Grado"};
        DaoEstudiante daoEstudiantes = new DaoEstudiante();
        DaoGrado daoGrado = new DaoGrado();
        BaseBlo blo = new BaseBlo();

        public FrmEstudiante()
        {
            InitializeComponent();
            cargarDatos();
            cargarCombo();
        }

        public void cargarCombo()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.listaGrado();

                foreach (Model.Grado p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado1 + " " + p.Seccion.seccion1);
                }

                cmbGrado.DataSource = new BindingSource(lista, null);
                cmbGrado.DisplayMember = "Value";
                cmbGrado.ValueMember = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }
        }


        public void cargarDatos()
        {
            List<Model.Estudiante> estudiante = new List<Model.Estudiante>();
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                estudiante = daoEstudiantes.listEstudiantes();
                //limpiar valores del DataGridView
                dgvEstudiantes.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvEstudiantes.DataSource = blo.toDataTable(estudiante.Select(x => new
                {
                    x.id_estudiante,
                    x.nombre,
                    x.apellido,
                    x.direccion,
                    x.padreResponsable,
                    x.telefono,
                    x.estado_promedio,
                    x.fecha_creacion,
                    x.estado,
                    
                    grado = x.Grado.grado1 + " " + x.Grado.Seccion.seccion1
                }).ToList(),Columns);


                if (dgvEstudiantes.RowCount > 0)
                {
                    dgvEstudiantes.Rows[0].Selected = false;

                }
            }
            catch (Exception)
            {

            }
        }

        public void cargarResultados(string campo, string valor)
        {
            List<Model.Estudiante> estudiante = new List<Model.Estudiante>();
            try
            {
                //Recupera una lista de la base de datos de todos los docentes
                estudiante = daoEstudiantes.filtrarEstudiantes(campo,valor);
                //limpiar valores del DataGridView
                dgvEstudiantes.Columns.Clear();
                //Asignacion de valores al DataGridView
                dgvEstudiantes.DataSource = blo.toDataTable(estudiante.Select(x => new
                {
                    x.id_estudiante,
                    x.nombre,
                    x.apellido,
                    x.direccion,
                    x.padreResponsable,
                    x.telefono,
                    x.estado_promedio,
                    x.fecha_creacion,
                    x.estado,
                    grado = x.Grado.grado1 + " " + x.Grado.Seccion.seccion1
                }).ToList(), Columns);


                if (dgvEstudiantes.RowCount > 0)
                {
                    dgvEstudiantes.Rows[0].Selected = false;

                }
            }
            catch (Exception)
            {

            }
        }

        public void llenarDatos()
        {
            if (dgvEstudiantes.RowCount > 0)
            {
                txtID.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[0].Value.ToString();
                txtNombres.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[1].Value.ToString();
                txtApellidos.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[2].Value.ToString();
                txtDireccion.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[3].Value.ToString();
                txtResponsable.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[4].Value.ToString();
                txtTelefono.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[5].Value.ToString();
                cmbGrado.Text = dgvEstudiantes.Rows[dgvEstudiantes.CurrentRow.Index].Cells[9].Value.ToString();
            }
        }

        private void FrmEstudiante_Load(object sender, EventArgs e)
        {
            estadoBotones("Nuevo");
            deshabilitarCampos();
            limpiar();

            if (dgvEstudiantes.RowCount > 0)
            {
                dgvEstudiantes.Rows[0].Selected = false;

            }
        }

        public void habilitarCampos()
        {
            txtNombres.Enabled = true;
            txtApellidos.Enabled = true;
            txtDireccion.Enabled = true;
            txtTelefono.Enabled = true;
            txtResponsable.Enabled = true;
            cmbGrado.Enabled = true;
        }
        public void deshabilitarCampos()
        {
            txtNombres.Enabled = false;
            txtApellidos.Enabled = false;
            txtDireccion.Enabled = false;
            txtTelefono.Enabled = false;
            txtResponsable.Enabled = false;
            cmbGrado.Enabled = false;
        }

        public void habilitarFiltro(bool i)
        {
            txtValor.Enabled = i;
            cmbFiltrar.Enabled = i;
            btnBuscar.Enabled = i;
            btnRecargar.Enabled = i;
        }

        public void limpiarFiltro()
        {
            txtValor.Clear();
            cmbFiltrar.SelectedIndex = -1; ;
        }


        public Model.Estudiante setearValores()
        {
            try
            {
                Model.Estudiante estudiante = new Model.Estudiante();

                estudiante.nombre = txtNombres.Text;
                estudiante.apellido = txtApellidos.Text;
                estudiante.direccion = txtDireccion.Text;
                estudiante.padreResponsable = txtResponsable.Text;
                estudiante.telefono = txtTelefono.Text;
                estudiante.estado_promedio = "EN CURSO";
                estudiante.fecha_creacion = DateTime.Now.ToString("dd/MM/yyyy");
                estudiante.estado = "OPEN";
                estudiante.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());

                return estudiante;

            }
            catch (Exception)
            {
                return new Model.Estudiante();
            }
        }

        public bool validarCantidadEstudiantes(int grado)
        {
            List<Model.Estudiante> estudiante = new List<Model.Estudiante>();
            List<Model.Grado> grados = new List<Model.Grado>();
            var ingreso = false;

            estudiante = daoEstudiantes.listarWhereGrado(grado);
            var cant = daoGrado.listGradoById(grado).Select(x => x.cantidad_alumno).FirstOrDefault();

            if (estudiante.Count() >= cant)
            {
                ingreso = false;
            }
            else
            {
                ingreso = true;
            }

            return ingreso;

        }

        public bool validarCampos()
        {
            bool estado = false;

            if (!cmbGrado.Text.Equals("") && !txtNombres.Text.Equals("") && !txtApellidos.Text.Equals("") && !txtDireccion.Text.Equals("") && !txtResponsable.Text.Equals("") && !txtTelefono.Text.Equals(""))
            {
                estado = true;
            }
            else
            {
                estado = false;
            }

            return estado;
        }

        public void limpiar()
        {
            txtID.Clear();
            txtNombres.Clear();
            txtApellidos.Clear();
            txtDireccion.Clear();
            txtResponsable.Clear();
            txtTelefono.Clear();
            cmbGrado.SelectedIndex = -1;
        }

        public void estadoBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnNuevo.Enabled = false;


            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnNuevo.Enabled = false;
            }
            else
            {
                btnGuardar.Enabled = false;
                btnEliminar.Enabled = true;
                btnModificar.Enabled = true;
                btnNuevo.Enabled = true;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            estadoBotones("Agregar");
            limpiarFiltro();
            habilitarFiltro(false);
            limpiar();
            habilitarCampos();
        }

        private void dgvEstudiantes_Click(object sender, EventArgs e)
        {
            llenarDatos();
            limpiarFiltro();
            habilitarFiltro(false);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {   
                if (agregar == true)
                {
                    if (validarCantidadEstudiantes(int.Parse(cmbGrado.SelectedValue.ToString())))
                    {
                        if (daoEstudiantes.insertarEstudiante(setearValores()))
                            MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        agregar = false;
                        modificar = false;
                    }
                    else
                    {
                        MessageBox.Show("Ya no hay cupo en el grado seleccionado", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    
                }
                else if (modificar == true)
                {
                    if (daoEstudiantes.modificarEstudiante(setearValores(), int.Parse(txtID.Text)))
                        MessageBox.Show("Información registrado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al registrar la información", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }

                limpiar();
                cargarDatos();
                deshabilitarCampos();
                limpiarFiltro();
                habilitarFiltro(true);
                estadoBotones("Nuevo");
            }
            else
            {
                MessageBox.Show("Todos los campos son requeridos", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvEstudiantes.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                limpiarFiltro();
                habilitarFiltro(false);
                habilitarCampos();
                estadoBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvEstudiantes.SelectedRows.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (daoEstudiantes.eliminarEstudiante(int.Parse(txtID.Text)))
                        MessageBox.Show("Registro eliminado correctamente", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al Eliminar la información", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                limpiar();
                cargarDatos();
                limpiarFiltro();
                habilitarFiltro(true);
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            limpiarFiltro();
            habilitarFiltro(true);
            deshabilitarCampos();
            cargarDatos();
            estadoBotones("Nuevo");
        }

        private void btnNuevo_Click_1(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnModificar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {

        }

        private void dgvEstudiantes_Click_1(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtApellidos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtResponsable_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            limpiarFiltro();
            cargarDatos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(!cmbFiltrar.Text.Equals("") && !txtValor.Text.Equals(""))
            {
                cargarResultados(cmbFiltrar.Text, txtValor.Text);
            }
            else
            {
                MessageBox.Show("Debe completar todos los campos de filtro", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }


    }
}
