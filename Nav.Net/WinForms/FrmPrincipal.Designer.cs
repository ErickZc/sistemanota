﻿namespace SistemaGestionNotas.WinForms
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSuperior = new System.Windows.Forms.Panel();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.panelMenuSide = new System.Windows.Forms.Panel();
            this.panelOpciones = new System.Windows.Forms.Panel();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.panelEnc = new System.Windows.Forms.Panel();
            this.btnManAlumnosEnc = new System.Windows.Forms.Button();
            this.btnMantenimientoEnc = new System.Windows.Forms.Button();
            this.panelDE = new System.Windows.Forms.Panel();
            this.btnMantAlumnosDC = new System.Windows.Forms.Button();
            this.btnMantNotasDC = new System.Windows.Forms.Button();
            this.btnMantenimientoDE = new System.Windows.Forms.Button();
            this.panelDoc = new System.Windows.Forms.Panel();
            this.btnMantNotasDoc = new System.Windows.Forms.Button();
            this.btnMantenimientoDoc = new System.Windows.Forms.Button();
            this.btnCambiarContra = new System.Windows.Forms.Button();
            this.panelMenuReportes = new System.Windows.Forms.Panel();
            this.btnRepEncargados = new System.Windows.Forms.Button();
            this.btnRepMaterias = new System.Windows.Forms.Button();
            this.btnRepDocentes = new System.Windows.Forms.Button();
            this.btnRepBoletas = new System.Windows.Forms.Button();
            this.btnRepAlumnos = new System.Windows.Forms.Button();
            this.btnRepNotas = new System.Windows.Forms.Button();
            this.btnReporteria = new System.Windows.Forms.Button();
            this.panelMenuMantenimiento = new System.Windows.Forms.Panel();
            this.btnMantMaterias = new System.Windows.Forms.Button();
            this.btnMantEncargados = new System.Windows.Forms.Button();
            this.btnMantenimientoAdm = new System.Windows.Forms.Button();
            this.panelMenuAdministracion = new System.Windows.Forms.Panel();
            this.btnAdmSecciones = new System.Windows.Forms.Button();
            this.btnAdmGrados = new System.Windows.Forms.Button();
            this.btnAdmAdministradores = new System.Windows.Forms.Button();
            this.btnAdmEstudiantes = new System.Windows.Forms.Button();
            this.btnAdmMaterias = new System.Windows.Forms.Button();
            this.btnAdmDocentes = new System.Windows.Forms.Button();
            this.btnAdministracion = new System.Windows.Forms.Button();
            this.btnInicio = new System.Windows.Forms.Button();
            this.panelEspacio = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelDescripción = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelFormularios = new System.Windows.Forms.Panel();
            this.panelSuperior.SuspendLayout();
            this.panelMenuSide.SuspendLayout();
            this.panelOpciones.SuspendLayout();
            this.panelEnc.SuspendLayout();
            this.panelDE.SuspendLayout();
            this.panelDoc.SuspendLayout();
            this.panelMenuReportes.SuspendLayout();
            this.panelMenuMantenimiento.SuspendLayout();
            this.panelMenuAdministracion.SuspendLayout();
            this.panelDescripción.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSuperior
            // 
            this.panelSuperior.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(40)))), ((int)(((byte)(49)))));
            this.panelSuperior.Controls.Add(this.btnCerrar);
            this.panelSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperior.Location = new System.Drawing.Point(0, 0);
            this.panelSuperior.Name = "panelSuperior";
            this.panelSuperior.Size = new System.Drawing.Size(965, 31);
            this.panelSuperior.TabIndex = 1;
            this.panelSuperior.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSuperior_Paint);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCerrar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(40)))), ((int)(((byte)(49)))));
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Image = global::SistemaGestionNotas.Properties.Resources.cerrar;
            this.btnCerrar.Location = new System.Drawing.Point(912, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(53, 31);
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panelMenuSide
            // 
            this.panelMenuSide.AutoScroll = true;
            this.panelMenuSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.panelMenuSide.Controls.Add(this.panelOpciones);
            this.panelMenuSide.Controls.Add(this.panelEspacio);
            this.panelMenuSide.Controls.Add(this.label4);
            this.panelMenuSide.Controls.Add(this.panelLogo);
            this.panelMenuSide.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenuSide.Location = new System.Drawing.Point(0, 31);
            this.panelMenuSide.Name = "panelMenuSide";
            this.panelMenuSide.Size = new System.Drawing.Size(197, 553);
            this.panelMenuSide.TabIndex = 8;
            // 
            // panelOpciones
            // 
            this.panelOpciones.AutoScroll = true;
            this.panelOpciones.Controls.Add(this.btnCerrarSesion);
            this.panelOpciones.Controls.Add(this.panelEnc);
            this.panelOpciones.Controls.Add(this.btnMantenimientoEnc);
            this.panelOpciones.Controls.Add(this.panelDE);
            this.panelOpciones.Controls.Add(this.btnMantenimientoDE);
            this.panelOpciones.Controls.Add(this.panelDoc);
            this.panelOpciones.Controls.Add(this.btnMantenimientoDoc);
            this.panelOpciones.Controls.Add(this.btnCambiarContra);
            this.panelOpciones.Controls.Add(this.panelMenuReportes);
            this.panelOpciones.Controls.Add(this.btnReporteria);
            this.panelOpciones.Controls.Add(this.panelMenuMantenimiento);
            this.panelOpciones.Controls.Add(this.btnMantenimientoAdm);
            this.panelOpciones.Controls.Add(this.panelMenuAdministracion);
            this.panelOpciones.Controls.Add(this.btnAdministracion);
            this.panelOpciones.Controls.Add(this.btnInicio);
            this.panelOpciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOpciones.Location = new System.Drawing.Point(0, 127);
            this.panelOpciones.Name = "panelOpciones";
            this.panelOpciones.Size = new System.Drawing.Size(197, 426);
            this.panelOpciones.TabIndex = 3;
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCerrarSesion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnCerrarSesion.FlatAppearance.BorderSize = 0;
            this.btnCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnCerrarSesion.Image = global::SistemaGestionNotas.Properties.Resources.cerrar_sesion;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(0, 840);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(180, 35);
            this.btnCerrarSesion.TabIndex = 10;
            this.btnCerrarSesion.Text = "    Cerrar sesión";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.button1_Click);
            // 
            // panelEnc
            // 
            this.panelEnc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelEnc.Controls.Add(this.btnManAlumnosEnc);
            this.panelEnc.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEnc.Location = new System.Drawing.Point(0, 799);
            this.panelEnc.Name = "panelEnc";
            this.panelEnc.Size = new System.Drawing.Size(180, 41);
            this.panelEnc.TabIndex = 14;
            // 
            // btnManAlumnosEnc
            // 
            this.btnManAlumnosEnc.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnManAlumnosEnc.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnManAlumnosEnc.FlatAppearance.BorderSize = 0;
            this.btnManAlumnosEnc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnManAlumnosEnc.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManAlumnosEnc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnManAlumnosEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnManAlumnosEnc.Location = new System.Drawing.Point(0, 0);
            this.btnManAlumnosEnc.Name = "btnManAlumnosEnc";
            this.btnManAlumnosEnc.Size = new System.Drawing.Size(180, 41);
            this.btnManAlumnosEnc.TabIndex = 3;
            this.btnManAlumnosEnc.Text = "Matricular Alumnos";
            this.btnManAlumnosEnc.UseVisualStyleBackColor = true;
            this.btnManAlumnosEnc.Click += new System.EventHandler(this.btnManAlumnosEnc_Click);
            // 
            // btnMantenimientoEnc
            // 
            this.btnMantenimientoEnc.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantenimientoEnc.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnMantenimientoEnc.FlatAppearance.BorderSize = 0;
            this.btnMantenimientoEnc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantenimientoEnc.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimientoEnc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantenimientoEnc.Image = global::SistemaGestionNotas.Properties.Resources.usuario__4_;
            this.btnMantenimientoEnc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantenimientoEnc.Location = new System.Drawing.Point(0, 764);
            this.btnMantenimientoEnc.Name = "btnMantenimientoEnc";
            this.btnMantenimientoEnc.Size = new System.Drawing.Size(180, 35);
            this.btnMantenimientoEnc.TabIndex = 13;
            this.btnMantenimientoEnc.Text = "  Alumnos";
            this.btnMantenimientoEnc.UseVisualStyleBackColor = true;
            this.btnMantenimientoEnc.Click += new System.EventHandler(this.btnMantenimientoEnc_Click);
            // 
            // panelDE
            // 
            this.panelDE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelDE.Controls.Add(this.btnMantAlumnosDC);
            this.panelDE.Controls.Add(this.btnMantNotasDC);
            this.panelDE.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDE.Location = new System.Drawing.Point(0, 683);
            this.panelDE.Name = "panelDE";
            this.panelDE.Size = new System.Drawing.Size(180, 81);
            this.panelDE.TabIndex = 12;
            // 
            // btnMantAlumnosDC
            // 
            this.btnMantAlumnosDC.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantAlumnosDC.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnMantAlumnosDC.FlatAppearance.BorderSize = 0;
            this.btnMantAlumnosDC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantAlumnosDC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantAlumnosDC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantAlumnosDC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantAlumnosDC.Location = new System.Drawing.Point(0, 43);
            this.btnMantAlumnosDC.Name = "btnMantAlumnosDC";
            this.btnMantAlumnosDC.Size = new System.Drawing.Size(180, 40);
            this.btnMantAlumnosDC.TabIndex = 4;
            this.btnMantAlumnosDC.Text = "Matricular Alumnos";
            this.btnMantAlumnosDC.UseVisualStyleBackColor = true;
            this.btnMantAlumnosDC.Click += new System.EventHandler(this.btnMantAlumnosDC_Click);
            // 
            // btnMantNotasDC
            // 
            this.btnMantNotasDC.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantNotasDC.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnMantNotasDC.FlatAppearance.BorderSize = 0;
            this.btnMantNotasDC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantNotasDC.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantNotasDC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantNotasDC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantNotasDC.Location = new System.Drawing.Point(0, 0);
            this.btnMantNotasDC.Name = "btnMantNotasDC";
            this.btnMantNotasDC.Size = new System.Drawing.Size(180, 43);
            this.btnMantNotasDC.TabIndex = 3;
            this.btnMantNotasDC.Text = "Ingresar notas";
            this.btnMantNotasDC.UseVisualStyleBackColor = true;
            this.btnMantNotasDC.Click += new System.EventHandler(this.btnMantNotasDC_Click);
            // 
            // btnMantenimientoDE
            // 
            this.btnMantenimientoDE.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantenimientoDE.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnMantenimientoDE.FlatAppearance.BorderSize = 0;
            this.btnMantenimientoDE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantenimientoDE.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimientoDE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantenimientoDE.Image = global::SistemaGestionNotas.Properties.Resources.editar__3_;
            this.btnMantenimientoDE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantenimientoDE.Location = new System.Drawing.Point(0, 648);
            this.btnMantenimientoDE.Name = "btnMantenimientoDE";
            this.btnMantenimientoDE.Size = new System.Drawing.Size(180, 35);
            this.btnMantenimientoDE.TabIndex = 11;
            this.btnMantenimientoDE.Text = "        Mantenimiento";
            this.btnMantenimientoDE.UseVisualStyleBackColor = true;
            this.btnMantenimientoDE.Click += new System.EventHandler(this.btnMantenimientoDE_Click);
            // 
            // panelDoc
            // 
            this.panelDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelDoc.Controls.Add(this.btnMantNotasDoc);
            this.panelDoc.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDoc.Location = new System.Drawing.Point(0, 607);
            this.panelDoc.Name = "panelDoc";
            this.panelDoc.Size = new System.Drawing.Size(180, 41);
            this.panelDoc.TabIndex = 8;
            // 
            // btnMantNotasDoc
            // 
            this.btnMantNotasDoc.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantNotasDoc.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnMantNotasDoc.FlatAppearance.BorderSize = 0;
            this.btnMantNotasDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantNotasDoc.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantNotasDoc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantNotasDoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantNotasDoc.Location = new System.Drawing.Point(0, 0);
            this.btnMantNotasDoc.Name = "btnMantNotasDoc";
            this.btnMantNotasDoc.Size = new System.Drawing.Size(180, 41);
            this.btnMantNotasDoc.TabIndex = 3;
            this.btnMantNotasDoc.Text = "Ingresar notas";
            this.btnMantNotasDoc.UseVisualStyleBackColor = true;
            this.btnMantNotasDoc.Click += new System.EventHandler(this.btnMantNotasDoc_Click);
            // 
            // btnMantenimientoDoc
            // 
            this.btnMantenimientoDoc.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantenimientoDoc.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnMantenimientoDoc.FlatAppearance.BorderSize = 0;
            this.btnMantenimientoDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantenimientoDoc.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimientoDoc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantenimientoDoc.Image = global::SistemaGestionNotas.Properties.Resources.editar__3_;
            this.btnMantenimientoDoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantenimientoDoc.Location = new System.Drawing.Point(0, 572);
            this.btnMantenimientoDoc.Name = "btnMantenimientoDoc";
            this.btnMantenimientoDoc.Size = new System.Drawing.Size(180, 35);
            this.btnMantenimientoDoc.TabIndex = 7;
            this.btnMantenimientoDoc.Text = "        Notas";
            this.btnMantenimientoDoc.UseVisualStyleBackColor = true;
            this.btnMantenimientoDoc.Click += new System.EventHandler(this.btnMantenimientoDoc_Click);
            // 
            // btnCambiarContra
            // 
            this.btnCambiarContra.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnCambiarContra.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnCambiarContra.FlatAppearance.BorderSize = 2;
            this.btnCambiarContra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCambiarContra.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambiarContra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnCambiarContra.Image = global::SistemaGestionNotas.Properties.Resources.proteger;
            this.btnCambiarContra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCambiarContra.Location = new System.Drawing.Point(0, 875);
            this.btnCambiarContra.Name = "btnCambiarContra";
            this.btnCambiarContra.Padding = new System.Windows.Forms.Padding(1);
            this.btnCambiarContra.Size = new System.Drawing.Size(180, 39);
            this.btnCambiarContra.TabIndex = 6;
            this.btnCambiarContra.Text = "     Cambiar contraseña";
            this.btnCambiarContra.UseVisualStyleBackColor = true;
            this.btnCambiarContra.Click += new System.EventHandler(this.btnCambiarContra_Click);
            // 
            // panelMenuReportes
            // 
            this.panelMenuReportes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelMenuReportes.Controls.Add(this.btnRepEncargados);
            this.panelMenuReportes.Controls.Add(this.btnRepMaterias);
            this.panelMenuReportes.Controls.Add(this.btnRepDocentes);
            this.panelMenuReportes.Controls.Add(this.btnRepBoletas);
            this.panelMenuReportes.Controls.Add(this.btnRepAlumnos);
            this.panelMenuReportes.Controls.Add(this.btnRepNotas);
            this.panelMenuReportes.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenuReportes.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelMenuReportes.Location = new System.Drawing.Point(0, 385);
            this.panelMenuReportes.Name = "panelMenuReportes";
            this.panelMenuReportes.Size = new System.Drawing.Size(180, 187);
            this.panelMenuReportes.TabIndex = 5;
            // 
            // btnRepEncargados
            // 
            this.btnRepEncargados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepEncargados.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepEncargados.FlatAppearance.BorderSize = 0;
            this.btnRepEncargados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepEncargados.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepEncargados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepEncargados.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepEncargados.Location = new System.Drawing.Point(0, 153);
            this.btnRepEncargados.Name = "btnRepEncargados";
            this.btnRepEncargados.Size = new System.Drawing.Size(180, 30);
            this.btnRepEncargados.TabIndex = 10;
            this.btnRepEncargados.Text = "Encargados de grados";
            this.btnRepEncargados.UseVisualStyleBackColor = true;
            this.btnRepEncargados.Click += new System.EventHandler(this.btnRepEncargados_Click);
            // 
            // btnRepMaterias
            // 
            this.btnRepMaterias.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepMaterias.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepMaterias.FlatAppearance.BorderSize = 0;
            this.btnRepMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepMaterias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepMaterias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepMaterias.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepMaterias.Location = new System.Drawing.Point(0, 123);
            this.btnRepMaterias.Name = "btnRepMaterias";
            this.btnRepMaterias.Size = new System.Drawing.Size(180, 30);
            this.btnRepMaterias.TabIndex = 9;
            this.btnRepMaterias.Text = "Materias por docentes";
            this.btnRepMaterias.UseVisualStyleBackColor = true;
            this.btnRepMaterias.Click += new System.EventHandler(this.btnRepMaterias_Click);
            // 
            // btnRepDocentes
            // 
            this.btnRepDocentes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepDocentes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepDocentes.FlatAppearance.BorderSize = 0;
            this.btnRepDocentes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepDocentes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepDocentes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepDocentes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepDocentes.Location = new System.Drawing.Point(0, 93);
            this.btnRepDocentes.Name = "btnRepDocentes";
            this.btnRepDocentes.Size = new System.Drawing.Size(180, 30);
            this.btnRepDocentes.TabIndex = 8;
            this.btnRepDocentes.Text = "Docentes";
            this.btnRepDocentes.UseVisualStyleBackColor = true;
            this.btnRepDocentes.Click += new System.EventHandler(this.btnRepDocentes_Click);
            // 
            // btnRepBoletas
            // 
            this.btnRepBoletas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepBoletas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepBoletas.FlatAppearance.BorderSize = 0;
            this.btnRepBoletas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepBoletas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepBoletas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepBoletas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepBoletas.Location = new System.Drawing.Point(0, 62);
            this.btnRepBoletas.Name = "btnRepBoletas";
            this.btnRepBoletas.Size = new System.Drawing.Size(180, 31);
            this.btnRepBoletas.TabIndex = 7;
            this.btnRepBoletas.Text = "Boletas";
            this.btnRepBoletas.UseVisualStyleBackColor = true;
            this.btnRepBoletas.Click += new System.EventHandler(this.btnRepBoletas_Click);
            // 
            // btnRepAlumnos
            // 
            this.btnRepAlumnos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepAlumnos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepAlumnos.FlatAppearance.BorderSize = 0;
            this.btnRepAlumnos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepAlumnos.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepAlumnos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepAlumnos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepAlumnos.Location = new System.Drawing.Point(0, 31);
            this.btnRepAlumnos.Name = "btnRepAlumnos";
            this.btnRepAlumnos.Size = new System.Drawing.Size(180, 31);
            this.btnRepAlumnos.TabIndex = 6;
            this.btnRepAlumnos.Text = "Alumnos";
            this.btnRepAlumnos.UseVisualStyleBackColor = true;
            this.btnRepAlumnos.Click += new System.EventHandler(this.btnRepAlumnos_Click);
            // 
            // btnRepNotas
            // 
            this.btnRepNotas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRepNotas.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnRepNotas.FlatAppearance.BorderSize = 0;
            this.btnRepNotas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepNotas.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRepNotas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnRepNotas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepNotas.Location = new System.Drawing.Point(0, 0);
            this.btnRepNotas.Name = "btnRepNotas";
            this.btnRepNotas.Size = new System.Drawing.Size(180, 31);
            this.btnRepNotas.TabIndex = 5;
            this.btnRepNotas.Text = "Notas";
            this.btnRepNotas.UseVisualStyleBackColor = true;
            this.btnRepNotas.Click += new System.EventHandler(this.btnRepNotas_Click);
            // 
            // btnReporteria
            // 
            this.btnReporteria.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnReporteria.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnReporteria.FlatAppearance.BorderSize = 0;
            this.btnReporteria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReporteria.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporteria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnReporteria.Image = global::SistemaGestionNotas.Properties.Resources.grafico_de_barras;
            this.btnReporteria.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReporteria.Location = new System.Drawing.Point(0, 350);
            this.btnReporteria.Name = "btnReporteria";
            this.btnReporteria.Size = new System.Drawing.Size(180, 35);
            this.btnReporteria.TabIndex = 4;
            this.btnReporteria.Text = "Reportería";
            this.btnReporteria.UseVisualStyleBackColor = true;
            this.btnReporteria.Click += new System.EventHandler(this.btnReporteria_Click);
            // 
            // panelMenuMantenimiento
            // 
            this.panelMenuMantenimiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelMenuMantenimiento.Controls.Add(this.btnMantMaterias);
            this.panelMenuMantenimiento.Controls.Add(this.btnMantEncargados);
            this.panelMenuMantenimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenuMantenimiento.Location = new System.Drawing.Point(0, 282);
            this.panelMenuMantenimiento.Name = "panelMenuMantenimiento";
            this.panelMenuMantenimiento.Size = new System.Drawing.Size(180, 68);
            this.panelMenuMantenimiento.TabIndex = 3;
            // 
            // btnMantMaterias
            // 
            this.btnMantMaterias.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantMaterias.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnMantMaterias.FlatAppearance.BorderSize = 0;
            this.btnMantMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantMaterias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantMaterias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantMaterias.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantMaterias.Location = new System.Drawing.Point(0, 36);
            this.btnMantMaterias.Name = "btnMantMaterias";
            this.btnMantMaterias.Size = new System.Drawing.Size(180, 32);
            this.btnMantMaterias.TabIndex = 6;
            this.btnMantMaterias.Text = "Asignar materias";
            this.btnMantMaterias.UseVisualStyleBackColor = true;
            this.btnMantMaterias.Click += new System.EventHandler(this.btnMantMaterias_Click);
            // 
            // btnMantEncargados
            // 
            this.btnMantEncargados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantEncargados.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnMantEncargados.FlatAppearance.BorderSize = 0;
            this.btnMantEncargados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantEncargados.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantEncargados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantEncargados.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantEncargados.Location = new System.Drawing.Point(0, 0);
            this.btnMantEncargados.Name = "btnMantEncargados";
            this.btnMantEncargados.Size = new System.Drawing.Size(180, 36);
            this.btnMantEncargados.TabIndex = 5;
            this.btnMantEncargados.Text = "Asignación de encargado";
            this.btnMantEncargados.UseVisualStyleBackColor = true;
            this.btnMantEncargados.Click += new System.EventHandler(this.btnMantEncargados_Click);
            // 
            // btnMantenimientoAdm
            // 
            this.btnMantenimientoAdm.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMantenimientoAdm.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnMantenimientoAdm.FlatAppearance.BorderSize = 0;
            this.btnMantenimientoAdm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMantenimientoAdm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMantenimientoAdm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnMantenimientoAdm.Image = global::SistemaGestionNotas.Properties.Resources.editar__3_;
            this.btnMantenimientoAdm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMantenimientoAdm.Location = new System.Drawing.Point(0, 247);
            this.btnMantenimientoAdm.Name = "btnMantenimientoAdm";
            this.btnMantenimientoAdm.Size = new System.Drawing.Size(180, 35);
            this.btnMantenimientoAdm.TabIndex = 2;
            this.btnMantenimientoAdm.Text = "        Mantenimiento";
            this.btnMantenimientoAdm.UseVisualStyleBackColor = true;
            this.btnMantenimientoAdm.Click += new System.EventHandler(this.btnMantenimiento_Click);
            // 
            // panelMenuAdministracion
            // 
            this.panelMenuAdministracion.AutoScroll = true;
            this.panelMenuAdministracion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.panelMenuAdministracion.Controls.Add(this.btnAdmSecciones);
            this.panelMenuAdministracion.Controls.Add(this.btnAdmGrados);
            this.panelMenuAdministracion.Controls.Add(this.btnAdmAdministradores);
            this.panelMenuAdministracion.Controls.Add(this.btnAdmEstudiantes);
            this.panelMenuAdministracion.Controls.Add(this.btnAdmMaterias);
            this.panelMenuAdministracion.Controls.Add(this.btnAdmDocentes);
            this.panelMenuAdministracion.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenuAdministracion.Location = new System.Drawing.Point(0, 62);
            this.panelMenuAdministracion.Name = "panelMenuAdministracion";
            this.panelMenuAdministracion.Size = new System.Drawing.Size(180, 185);
            this.panelMenuAdministracion.TabIndex = 1;
            // 
            // btnAdmSecciones
            // 
            this.btnAdmSecciones.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmSecciones.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmSecciones.FlatAppearance.BorderSize = 0;
            this.btnAdmSecciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmSecciones.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmSecciones.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmSecciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmSecciones.Location = new System.Drawing.Point(0, 154);
            this.btnAdmSecciones.Name = "btnAdmSecciones";
            this.btnAdmSecciones.Size = new System.Drawing.Size(180, 31);
            this.btnAdmSecciones.TabIndex = 7;
            this.btnAdmSecciones.Text = "Secciones";
            this.btnAdmSecciones.UseVisualStyleBackColor = true;
            this.btnAdmSecciones.Click += new System.EventHandler(this.btnAdmSecciones_Click);
            // 
            // btnAdmGrados
            // 
            this.btnAdmGrados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmGrados.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmGrados.FlatAppearance.BorderSize = 0;
            this.btnAdmGrados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmGrados.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmGrados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmGrados.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmGrados.Location = new System.Drawing.Point(0, 124);
            this.btnAdmGrados.Name = "btnAdmGrados";
            this.btnAdmGrados.Size = new System.Drawing.Size(180, 30);
            this.btnAdmGrados.TabIndex = 5;
            this.btnAdmGrados.Text = "Grados";
            this.btnAdmGrados.UseVisualStyleBackColor = true;
            this.btnAdmGrados.Click += new System.EventHandler(this.btnAdmGrados_Click);
            // 
            // btnAdmAdministradores
            // 
            this.btnAdmAdministradores.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmAdministradores.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmAdministradores.FlatAppearance.BorderSize = 0;
            this.btnAdmAdministradores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmAdministradores.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmAdministradores.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmAdministradores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmAdministradores.Location = new System.Drawing.Point(0, 93);
            this.btnAdmAdministradores.Name = "btnAdmAdministradores";
            this.btnAdmAdministradores.Size = new System.Drawing.Size(180, 31);
            this.btnAdmAdministradores.TabIndex = 4;
            this.btnAdmAdministradores.Text = "Administradores";
            this.btnAdmAdministradores.UseVisualStyleBackColor = true;
            this.btnAdmAdministradores.Click += new System.EventHandler(this.btnAdmAdministradores_Click);
            // 
            // btnAdmEstudiantes
            // 
            this.btnAdmEstudiantes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmEstudiantes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmEstudiantes.FlatAppearance.BorderSize = 0;
            this.btnAdmEstudiantes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmEstudiantes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmEstudiantes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmEstudiantes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmEstudiantes.Location = new System.Drawing.Point(0, 62);
            this.btnAdmEstudiantes.Name = "btnAdmEstudiantes";
            this.btnAdmEstudiantes.Size = new System.Drawing.Size(180, 31);
            this.btnAdmEstudiantes.TabIndex = 3;
            this.btnAdmEstudiantes.Text = "Estudiantes";
            this.btnAdmEstudiantes.UseVisualStyleBackColor = true;
            this.btnAdmEstudiantes.Click += new System.EventHandler(this.btnAdmEstudiantes_Click);
            // 
            // btnAdmMaterias
            // 
            this.btnAdmMaterias.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmMaterias.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmMaterias.FlatAppearance.BorderSize = 0;
            this.btnAdmMaterias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmMaterias.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmMaterias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmMaterias.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmMaterias.Location = new System.Drawing.Point(0, 31);
            this.btnAdmMaterias.Name = "btnAdmMaterias";
            this.btnAdmMaterias.Size = new System.Drawing.Size(180, 31);
            this.btnAdmMaterias.TabIndex = 2;
            this.btnAdmMaterias.Text = "Materias";
            this.btnAdmMaterias.UseVisualStyleBackColor = true;
            this.btnAdmMaterias.Click += new System.EventHandler(this.btnAdmMaterias_Click);
            // 
            // btnAdmDocentes
            // 
            this.btnAdmDocentes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdmDocentes.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(149)))), ((int)(((byte)(183)))));
            this.btnAdmDocentes.FlatAppearance.BorderSize = 0;
            this.btnAdmDocentes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdmDocentes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmDocentes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdmDocentes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmDocentes.Location = new System.Drawing.Point(0, 0);
            this.btnAdmDocentes.Name = "btnAdmDocentes";
            this.btnAdmDocentes.Size = new System.Drawing.Size(180, 31);
            this.btnAdmDocentes.TabIndex = 1;
            this.btnAdmDocentes.Text = "Docentes";
            this.btnAdmDocentes.UseVisualStyleBackColor = true;
            this.btnAdmDocentes.Click += new System.EventHandler(this.btnAdmDocentes_Click);
            // 
            // btnAdministracion
            // 
            this.btnAdministracion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdministracion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnAdministracion.FlatAppearance.BorderSize = 0;
            this.btnAdministracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdministracion.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdministracion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnAdministracion.Image = global::SistemaGestionNotas.Properties.Resources.administracion;
            this.btnAdministracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdministracion.Location = new System.Drawing.Point(0, 29);
            this.btnAdministracion.Name = "btnAdministracion";
            this.btnAdministracion.Size = new System.Drawing.Size(180, 33);
            this.btnAdministracion.TabIndex = 0;
            this.btnAdministracion.Text = "         Administración";
            this.btnAdministracion.UseVisualStyleBackColor = true;
            this.btnAdministracion.Click += new System.EventHandler(this.btnAdministracion_Click);
            // 
            // btnInicio
            // 
            this.btnInicio.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInicio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(71)))), ((int)(((byte)(94)))));
            this.btnInicio.FlatAppearance.BorderSize = 0;
            this.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInicio.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.btnInicio.Image = global::SistemaGestionNotas.Properties.Resources.hogar;
            this.btnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.Location = new System.Drawing.Point(0, 0);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(180, 29);
            this.btnInicio.TabIndex = 9;
            this.btnInicio.Text = "Inicio";
            this.btnInicio.UseVisualStyleBackColor = true;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // panelEspacio
            // 
            this.panelEspacio.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEspacio.Location = new System.Drawing.Point(0, 105);
            this.panelEspacio.Name = "panelEspacio";
            this.panelEspacio.Size = new System.Drawing.Size(197, 22);
            this.panelEspacio.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.label4.Location = new System.Drawing.Point(0, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "    __________________________   ";
            // 
            // panelLogo
            // 
            this.panelLogo.BackgroundImage = global::SistemaGestionNotas.Properties.Resources.logo4;
            this.panelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(197, 92);
            this.panelLogo.TabIndex = 0;
            // 
            // panelDescripción
            // 
            this.panelDescripción.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.panelDescripción.Controls.Add(this.label2);
            this.panelDescripción.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDescripción.ForeColor = System.Drawing.Color.White;
            this.panelDescripción.Location = new System.Drawing.Point(197, 31);
            this.panelDescripción.Name = "panelDescripción";
            this.panelDescripción.Size = new System.Drawing.Size(768, 29);
            this.panelDescripción.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkGray;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = ">>Inicio";
            // 
            // panelFormularios
            // 
            this.panelFormularios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFormularios.Location = new System.Drawing.Point(197, 60);
            this.panelFormularios.Name = "panelFormularios";
            this.panelFormularios.Size = new System.Drawing.Size(768, 524);
            this.panelFormularios.TabIndex = 10;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 584);
            this.Controls.Add(this.panelFormularios);
            this.Controls.Add(this.panelDescripción);
            this.Controls.Add(this.panelMenuSide);
            this.Controls.Add(this.panelSuperior);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmPrincipal";
            this.panelSuperior.ResumeLayout(false);
            this.panelMenuSide.ResumeLayout(false);
            this.panelMenuSide.PerformLayout();
            this.panelOpciones.ResumeLayout(false);
            this.panelEnc.ResumeLayout(false);
            this.panelDE.ResumeLayout(false);
            this.panelDoc.ResumeLayout(false);
            this.panelMenuReportes.ResumeLayout(false);
            this.panelMenuMantenimiento.ResumeLayout(false);
            this.panelMenuAdministracion.ResumeLayout(false);
            this.panelDescripción.ResumeLayout(false);
            this.panelDescripción.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSuperior;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Panel panelMenuSide;
        private System.Windows.Forms.Panel panelOpciones;
        private System.Windows.Forms.Button btnCambiarContra;
        private System.Windows.Forms.Panel panelMenuReportes;
        private System.Windows.Forms.Button btnRepEncargados;
        private System.Windows.Forms.Button btnRepMaterias;
        private System.Windows.Forms.Button btnRepDocentes;
        private System.Windows.Forms.Button btnRepBoletas;
        private System.Windows.Forms.Button btnRepAlumnos;
        private System.Windows.Forms.Button btnRepNotas;
        private System.Windows.Forms.Button btnReporteria;
        private System.Windows.Forms.Panel panelMenuMantenimiento;
        private System.Windows.Forms.Button btnMantMaterias;
        private System.Windows.Forms.Button btnMantEncargados;
        private System.Windows.Forms.Button btnMantenimientoAdm;
        private System.Windows.Forms.Panel panelMenuAdministracion;
        private System.Windows.Forms.Button btnAdmSecciones;
        private System.Windows.Forms.Button btnAdmGrados;
        private System.Windows.Forms.Button btnAdmAdministradores;
        private System.Windows.Forms.Button btnAdmEstudiantes;
        private System.Windows.Forms.Button btnAdmMaterias;
        private System.Windows.Forms.Button btnAdmDocentes;
        private System.Windows.Forms.Button btnAdministracion;
        private System.Windows.Forms.Panel panelEspacio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelDescripción;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelFormularios;
        private System.Windows.Forms.Button btnMantenimientoDoc;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Panel panelDoc;
        private System.Windows.Forms.Button btnMantNotasDoc;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Panel panelDE;
        private System.Windows.Forms.Button btnMantAlumnosDC;
        private System.Windows.Forms.Button btnMantNotasDC;
        private System.Windows.Forms.Button btnMantenimientoDE;
        private System.Windows.Forms.Panel panelEnc;
        private System.Windows.Forms.Button btnManAlumnosEnc;
        private System.Windows.Forms.Button btnMantenimientoEnc;
    }
}