﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.Model;
using System.Reflection;

namespace SistemaGestionNotas.WinForms.Grado
{
    public partial class frmGrado : Form
    {
        bool agregar = false;
        bool editar = false;

        public frmGrado()
        {
            InitializeComponent();
            cargarFiltro();
            habilitarBotones();
            habilitarCaja(false);
            cargarDataGrid();
            llenarCmb();
        }

        private void frmGrado_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            editar = false;
            deshabilitarBotones("Agregar");
            habilitarCaja(true);
            limpiar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                editar = true;
                habilitarCaja(true);
                deshabilitarBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la eliminación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                confirmarEliminacion();
                limpiar();
            }
            else
                MessageBox.Show("Debe seleccionar un registro");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (agregar == true)
                {
                    agregarMateria();
                    agregar = false;
                    editar = false;
                }
                else if (editar == true)
                {
                    modificarMateria();
                    agregar = false;
                    editar = false;
                }
                limpiar();
                cargarDataGrid();
                habilitarCaja(false);
                habilitarBotones();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ha ocurrido el siguiente error: " + ex.Message);
            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            cargarCajas();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DaoGrado grado = new DaoGrado();
            Array lista = null;
            if (validacionesFiltro())
            {
                lista = grado.filtrarGrado(cmbFiltro.Text, txtValor.Text);
                dgvDatos.Columns.Clear();
                dgvDatos.DataSource = lista;
            }
            else
            {
                MessageBox.Show("Favor completar los datos del filtro");
            }
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            cargarDataGrid();
            limpiar();
        }

        #region

      
        public void llenarCmb()
        {
            try
            {

                using (ModeloDB modelo = new ModeloDB())
                {
                    Model.Seccion seccion = new Model.Seccion();
                    try
                    {
                        List<Model.Seccion> lista = modelo.Seccion.Where(x => x.estado == "OPEN").ToList();
                        if (lista.Count > 0)
                        {
                            cmbSeccion.DataSource = lista;
                            cmbSeccion.DisplayMember = "seccion1";
                            cmbSeccion.ValueMember = "id_seccion";
                            if (cmbSeccion.Items.Count >= 1)
                            {
                                cmbSeccion.SelectedIndex = -1;
                            }
                        }
                    }
                    catch (Exception ee)
                    {
                        MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        public bool validacionesFiltro()
        {
            bool i = false;

            if (cmbFiltro.Text.Equals("Seleccione un filtro"))
            {
                return i;
            }

            if (!txtValor.Text.Equals(""))
            {
                i = true;
            }

            return i;
        }

        public void deshabilitarBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
        }

        public void habilitarBotones()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnModificar.Enabled = true;
            btnAgregar.Enabled = true;
        }

        public void habilitarCaja(bool i)
        {
            txtIdGrado.Enabled = false;
            txtGrado.Enabled = i;
            txtCantidad.Enabled = i;
            cmbSeccion.Enabled = i;

        }

        public void confirmarEliminacion()
        {
            // Envía confirmación al usuario para eliminar el registro
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Estas seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    DaoGrado grado = new DaoGrado();
                    grado.eliminarGrado(int.Parse(txtIdGrado.Text));
                    cargarDataGrid();
                    MessageBox.Show("Se ha eliminado correctamente");
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro");
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void limpiar()
        {
            txtIdGrado.Clear();
            txtGrado.Clear();
            txtCantidad.Clear();
            cmbFiltro.SelectedIndex = 0;
            cmbSeccion.SelectedIndex = -1;
        }

        public void modificarMateria()
        {
            DaoGrado grado = new DaoGrado();
            grado.modificarGrado(int.Parse(txtIdGrado.Text), txtGrado.Text, int.Parse(cmbSeccion.SelectedValue.ToString()), int.Parse(txtCantidad.Text));
            cargarDataGrid();
        }

        public void agregarMateria()
        {
            DaoGrado grado = new DaoGrado();

            grado.agregarGrado(txtGrado.Text, int.Parse(cmbSeccion.SelectedValue.ToString()),int.Parse(txtCantidad.Text));
            cargarDataGrid();

        }

        public void cargarDataGrid()
        {
            DaoGrado grado = new DaoGrado();
            //List<Model.Grado> lista = new List<Model.Grado>();
            Array lista = null;

            lista = grado.listGrado();
            dgvDatos.Columns.Clear();

            dgvDatos.DataSource = lista;

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }

           dgvDatos.Columns["id_seccion"].Visible = false;

        }

        public void cargarCajas()
        {

            if (dgvDatos.RowCount > 0)
            {
                txtIdGrado.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_grado"].Value.ToString();
                txtGrado.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["grado"].Value.ToString();
                cmbSeccion.SelectedIndex = int.Parse(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_seccion"].Value.ToString()) - 1;
                txtCantidad.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["cantidad_alumno"].Value.ToString();
            }

        }

        public void cargarFiltro()
        {
            cmbFiltro.Items.Add("Seleccione un filtro");
            cmbFiltro.Items.Add("grado");
            cmbFiltro.Items.Add("seccion");
            cmbFiltro.Items.Add("cantidad de alumnos");
            cmbFiltro.SelectedIndex = 0;
        }

        #endregion

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            

            if (!char.IsControl(e.KeyChar)&& !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
          
        }

    }
}
