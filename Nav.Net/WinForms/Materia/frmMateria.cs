﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO;
using SistemaGestionNotas.BLO.Materia;

namespace SistemaGestionNotas.WinForms.Materia
{
    public partial class frmMateria : Form
    {
        bool agregar = false;
        bool editar = false;
        BaseBlo blo = new BaseBlo();

        private string[] Columns = new[] { "ID Materia", "Nombre de Materia"};

        public frmMateria()
        {
            InitializeComponent();
            cargarFiltro();
            habilitarBotones();
            habilitarCaja(false);
            txtIdMateria.Enabled = false;
            cargarDataGrid();
        }

        private void frmMateria_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DaoMateria materia = new DaoMateria();
            List<Model.Materia> lista = new List<Model.Materia>();
            if (validacionesFiltro())
            {
                lista = materia.filtrarMaterias(cmbFiltrar.Text, txtValor.Text);
                dgvDatos.Columns.Clear();

                dgvDatos.DataSource = blo.toDataTable(lista.Select(x => new
                {
                    x.id_materia,
                    x.nombre_materia
                }).ToList(), Columns);
            }
            else {
                MessageBox.Show("Favor completar los datos del filtro");
            }
                

            

        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            cargarDataGrid();
            limpiar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            editar = false;
            deshabilitarBotones("Agregar");
            habilitarCaja(true);
            limpiar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la modificación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                editar = true;
                habilitarCaja(true);
                deshabilitarBotones("Modificar");
            }
            else
                MessageBox.Show("Debe seleccionar un registro");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la eliminación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                confirmarEliminacion();
                limpiar();
            }
            else
                MessageBox.Show("Debe seleccionar un registro");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (agregar == true)
                {
                    agregarMateria();
                    agregar = false;
                    editar = false;
                }
                else if (editar == true)
                {
                    modificarMateria();
                    agregar = false;
                    editar = false;
                }
                limpiar();
                cargarDataGrid();
                habilitarCaja(false);
                habilitarBotones();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ha ocurrido el siguiente error: " + ex.Message);
            }
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            cargarCajas();
        }

        #region

        public void deshabilitarBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
            }
        }

        public void habilitarBotones()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnModificar.Enabled = true;
            btnAgregar.Enabled = true;
        }

        public void habilitarCaja(bool i)
        {
            txtNombreMateria.Enabled = i;

        }

        public bool validacionesFiltro()
        {
            bool i = false;

            if (cmbFiltrar.Text.Equals("Seleccione un filtro"))
            {
                return i;
            }

            if (!txtValor.Text.Equals(""))
            {
                i = true;
            }

            return i;
        }

        public void confirmarEliminacion()
        {
            // Envía confirmación al usuario para eliminar el registro
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Estas seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    DaoMateria materia = new DaoMateria();
                    materia.eliminarMateria(int.Parse(txtIdMateria.Text));
                    cargarDataGrid();
                    MessageBox.Show("Se ha eliminado correctamente");
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro");
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void limpiar() {
            txtIdMateria.Clear();
            txtNombreMateria.Clear();
            txtValor.Clear();
            cmbFiltrar.SelectedIndex = 0;
        }

        public void modificarMateria() {
            DaoMateria materia = new DaoMateria();
            materia.modificarMateria(int.Parse(txtIdMateria.Text),txtNombreMateria.Text);
            cargarDataGrid();
        }

        public void agregarMateria() {
            DaoMateria materia = new DaoMateria();

            materia.agregarMateria(txtNombreMateria.Text);
            cargarDataGrid();

        }

        public void cargarDataGrid() {
            DaoMateria materia = new DaoMateria();
            List<Model.Materia> lista = new List<Model.Materia>();

            lista = materia.listarMaterias();
            dgvDatos.Columns.Clear();

            dgvDatos.DataSource = blo.toDataTable(lista.Select(x => new
            {
                x.id_materia,
                x.nombre_materia
            }).ToList(), Columns);

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;

            }



        }

        public void cargarCajas()
        {

            if (dgvDatos.RowCount > 0)
            {
                txtIdMateria.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["ID Materia"].Value.ToString();
                txtNombreMateria.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["Nombre de Materia"].Value.ToString();
                
            }

        }

        public void cargarFiltro()
        {
            cmbFiltrar.Items.Add("Seleccione un filtro");
            cmbFiltrar.Items.Add("Nombre de materia");
            cmbFiltrar.Items.Add("Id Materia");
            cmbFiltrar.SelectedIndex = 0;
        }

        #endregion

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnModificar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click_1(object sender, EventArgs e)
        {

        }

        private void btnRecargar_Click_1(object sender, EventArgs e)
        {

        }

        private void dgvDatos_Click_1(object sender, EventArgs e)
        {
            cargarCajas();
        }


    }
}
