﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaGestionNotas.BLO.Docentes;
using SistemaGestionNotas.BLO.Grado;
using SistemaGestionNotas.BLO.Materia;
using SistemaGestionNotas.BLO.DocenteGradoMateria;

namespace SistemaGestionNotas.WinForms.Materia
{
    public partial class FrmMaterias_Profesor : Form


    {

        bool agregar = false;
        bool modificar = false;
        DaoDocente daoDocente = new DaoDocente();
        DaoGrado daoGrado = new DaoGrado();
        DaoMateria daoMateria = new DaoMateria();
        DaoDGM daoDGM = new DaoDGM();


        public FrmMaterias_Profesor()
        {
            InitializeComponent();
            cargarcmbDocente();
            cargarcmbGrado();
            cargarDatos();
            cargarcmbMateria();
            habilitarbtn();
            limpiar();
            
            deshabilitar();
            
        }

        private void cargarcmbDocente()
        {
            Dictionary<int, string> listaD = new Dictionary<int, string>();
            List<Model.Docente> listadoDocente = new List<Model.Docente>();
            try
            {
                listadoDocente = daoDocente.listDocentes();
                foreach (Model.Docente x in listadoDocente)
                {
                    listaD.Add(x.id_docente, x.nombre + " " + x.apellido);
                }

                // BindingSource ds1 = new BindingSource(listaD, null);
                if (listaD.Count > 0)
                {
                    cmbProfesor.DataSource = new BindingSource(listaD, null);
                    cmbProfesor.DisplayMember = "Value";
                    cmbProfesor.ValueMember = "Key";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error" + ex.Message);
            }
        }

        private void cargarcmbGrado()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Model.Grado> listadoGrados = new List<Model.Grado>();
            try
            {
                listadoGrados = daoGrado.listaGrado();

                foreach (Model.Grado p in listadoGrados)
                {
                    lista.Add(p.id_grado, p.grado1 + " " + p.Seccion.seccion1);
                }

                cmbGrado.DataSource = new BindingSource(lista, null);
                cmbGrado.DisplayMember = "Value";
                cmbGrado.ValueMember = "Key";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }

        }
         

        private void cargarcmbMateria()
        {
            Dictionary<int, string> lista = new Dictionary<int, string>();
            List<Model.Materia> listMateria = new List<Model.Materia>();
            try
            {
                listMateria = daoMateria.listarMaterias();
                foreach(Model.Materia m in listMateria)
                {
                    lista.Add(m.id_materia, m.nombre_materia);
                }
                
                if(lista.Count>0)
                {
                    cmbMateria.DataSource = new BindingSource(lista, null);
                    cmbMateria.DisplayMember = "Value";
                    cmbMateria.ValueMember = "Key";
                }

            }catch(Exception ex)
            {
                MessageBox.Show("Ocurrio lo siguiente" + ex.Message);
            }
        }


        private void cargarDatos()
        {
            try
            {
                using (Model.ModeloDB model = new Model.ModeloDB())
                {
                    var cargar = (from dgm in model.Docente_Materia_Grado
                                  join d in model.Docente on dgm.id_docente equals d.id_docente
                                  join g in model.Grado on dgm.id_grado equals g.id_grado
                                  join m in model.Materia on dgm.id_materia equals m.id_materia
                                  join s in model.Seccion on g.id_seccion equals s.id_seccion
                                  where dgm.estado == "OPEN" && g.estado.Equals("OPEN") && s.estado.Equals("OPEN") && m.estado.Equals("OPEN")
                                  select new
                                  {
                                      dgm.idDocente_Materia,
                                      dgm.id_docente,
                                      d.nombre,
                                      d.apellido,
                                      g.id_grado,
                                      grado = g.grado1 + " " + g.Seccion.seccion1,
                                      m.nombre_materia,
                                      dgm.fecha,

                                  }).ToList();
                    dgvDatos.DataSource = cargar;
                    dgvDatos.Columns["idDocente_Materia"].Visible = false;
                    dgvDatos.Columns["id_docente"].Visible = false;
                    dgvDatos.Columns["id_grado"].Visible = false;
                    //dgvDatos.Columns["id_seccion"].Visible = false;


                    if (dgvDatos.RowCount > 0)
                    {
                        dgvDatos.Rows[0].Selected = false;
                    }
                }
            }
            catch (Exception)
            {

            }
        }



        private void llenar()
        {
            cmbProfesor.SelectedIndex = cmbProfesor.FindStringExact(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["nombre"].Value.ToString() + " " + dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["apellido"].Value.ToString());
            //cmbGrado.SelectedIndex = cmbGrado.FindStringExact(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["grado1"].Value.ToString() + " " + dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["seccion1"].Value.ToString());
            cmbMateria.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["nombre_materia"].Value.ToString();
            //cmbGrado.SelectedIndex = int.Parse(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["id_grado"].Value.ToString());
            cmbGrado.SelectedIndex = cmbGrado.FindStringExact(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["grado"].Value.ToString());
        }


        private void habilitar()
        {
            cmbProfesor.Enabled = true;
            cmbGrado.Enabled = true;
            cmbMateria.Enabled = true;
        }

        private void deshabilitar()
        {
            cmbProfesor.Enabled = false;
            cmbGrado.Enabled = false;
            cmbMateria.Enabled = false;

        }


        private Model.Docente_Materia_Grado setear()
        {
            try
            {
                Model.Docente_Materia_Grado dgm = new Model.Docente_Materia_Grado();
                dgm.id_docente = int.Parse(cmbProfesor.SelectedValue.ToString());
                dgm.id_grado = int.Parse(cmbGrado.SelectedValue.ToString());
                dgm.id_materia = int.Parse(cmbMateria.SelectedValue.ToString());
                dgm.estado = "OPEN";
                dgm.fecha = DateTime.Now.ToString("dd/MM/yyyy");
                return dgm;
            }
            catch (Exception)
            {
                return new Model.Docente_Materia_Grado();
            }
        }



        private bool validarCampos()
        {
            bool estado = false;
            if(!cmbProfesor.Text.Equals("") && !cmbGrado.Text.Equals("") && !cmbMateria.Text.Equals(""))
            {
                estado = true;

            }
            else
            {
                estado = false;
            }
            return estado;
        }

        private void limpiar()
        {
            cmbProfesor.SelectedIndex = -1;
            cmbMateria.SelectedIndex = -1;
            cmbGrado.SelectedIndex = -1;
        }


        private void estadoBTN(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnELiminar.Enabled = false;
                btnModificar.Enabled = false;
                btnNuevo.Enabled = false;
            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnELiminar.Enabled = false;
                btnModificar.Enabled = false;
                btnNuevo.Enabled = false;

            }
            else
            {
                btnGuardar.Enabled = false;
                btnELiminar.Enabled = true;
                btnModificar.Enabled = true;
                btnNuevo.Enabled = true;
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            estadoBTN("Agregar");
            limpiar();
            habilitar();
        }

        private void dgvDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //llenar();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (validarCampos())
            {
                if (agregar == true)
                {
                    if (daoDGM.insertar(setear()))
                    
                    MessageBox.Show("Datos registrados correctamente", "Agregar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                    MessageBox.Show("Error al registrar datos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = true;
                    modificar = false;
                }
                else if (modificar == true)
                {
                    int id = int.Parse(dgvDatos.SelectedRows[0].Cells[0].Value.ToString());

                    if (daoDGM.modificar(setear(), id))
                        MessageBox.Show("Dato modificado con exito", "Modificar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al tratar de modificar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    agregar = false;
                    modificar = false;
                }
                limpiar();
                cargarDatos();
                deshabilitar();
                estadoBTN("Nuevo");
            }
            else
            {
                MessageBox.Show("Los campos son requeridos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitar();
                estadoBTN("Modificar");
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro para modificar", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnELiminar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                DialogResult dialog = MessageBox.Show("¿Estas seguro de eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialog == DialogResult.Yes)
                {
                    int id = int.Parse(dgvDatos.SelectedRows[0].Cells[0].Value.ToString());
                    if (daoDGM.eliminar(id))
                        MessageBox.Show("Registro eliminado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show("Error al eliminar registro", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("Se ha cancelado la eliminacion de registros", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                limpiar();
                cargarDatos();
            }
            else
                MessageBox.Show("Debe seleccionar un registro", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void FrmMaterias_Profesor_Load(object sender, EventArgs e)
        {

        }
        private void habilitarbtn()
        {
            btnGuardar.Enabled = false;
            btnCancelar.Enabled = true;
            btnELiminar.Enabled = true;
            btnModificar.Enabled = true;
            btnNuevo.Enabled = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            deshabilitar();
            habilitarbtn();
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            llenar();
        }
    }
}
