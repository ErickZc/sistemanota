﻿using SistemaGestionNotas.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.BLO.DocenteGradoMateria;

namespace SistemaGestionNotas.BLO.DocenteGradoMateria
{
    class DaoDGM
    {
        Model.ModeloDB model = new Model.ModeloDB();


        public bool insertar(Model.Docente_Materia_Grado dgm)
        {
            try
            {
                model.Docente_Materia_Grado.Add(dgm);
                model.SaveChanges();
                return true;

            }catch(Exception)
            {
                return false;
            }
        }


        public bool modificar (Docente_Materia_Grado dgm, int codigo)
        {
            Docente_Materia_Grado modify = new Docente_Materia_Grado();
            try
            {
                modify = model.Docente_Materia_Grado.FirstOrDefault(x => x.idDocente_Materia == codigo);
                if (modify != null) 
                { 
                
                 modify.id_docente = Convert.ToInt32(dgm.id_docente);
                 modify.id_grado = Convert.ToInt32(dgm.id_grado);
                 modify.id_materia = Convert.ToInt32(dgm.id_materia);
                 model.SaveChanges();
                return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception)
            {
                return false;
            }
        }


        public bool eliminar(int codigo)
        {
            Docente_Materia_Grado del = new Docente_Materia_Grado();
            try
            {
                del = model.Docente_Materia_Grado.FirstOrDefault(x => x.idDocente_Materia == codigo);
                del.estado = "LOCKED";
                model.SaveChanges();
                return true;
            }catch(Exception)
            {
                return false;
            }
        }

        public Docente_Materia_Grado ListarDocenteMateriaGradoById(int _id_docente)
        {

            Docente_Materia_Grado lista = new Docente_Materia_Grado();
            
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    lista = modelo.Docente_Materia_Grado.Where(x => x.id_docente == _id_docente && x.estado == "OPEN").FirstOrDefault();

                    return lista;

                }
            }catch(Exception){
                return new Docente_Materia_Grado();
            }
        }




    }
}
