﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Roles
{
    public class DaoRoles : BaseBlo
    {
        //Referencia a la entidad ModeloDB de la base de datos
        ModeloDB model = new ModeloDB();

        public List<Rol_Usuario> listRoles()
        {
            List<Rol_Usuario> listaRoles = new List<Rol_Usuario>();
            try
            {
                listaRoles = model.Rol_Usuario.ToList();

                if (listaRoles != null)
                    return listaRoles;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Rol_Usuario>();
            }
        }
        public Rol_Usuario getRol(int rol)
        {
            Rol_Usuario listaRoles = new Rol_Usuario();
            try
            {
                listaRoles = model.Rol_Usuario.FirstOrDefault(x=> x.id_rol == rol);

                if (listaRoles.id_rol != 0)
                    return listaRoles;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new Rol_Usuario();
            }
        }
        public Rol_Usuario getRolSeleccionado()
        {
            Rol_Usuario listaRoles = new Rol_Usuario();
            try
            {
                listaRoles = model.Rol_Usuario.FirstOrDefault(x => x.rol == "Docente");

                if (listaRoles.id_rol != 0)
                    return listaRoles;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new Rol_Usuario();
            }
        }
    }
}
