﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Estudiante
{
    public class DaoEstudiante : BaseBlo
    {

        ModeloDB model = new ModeloDB();
        public List<Model.Estudiante> listarWhereGrado(int? _id_grado)
        {
            List<Model.Estudiante> listarWhereGrado = new List<Model.Estudiante>();
            try 
            { 
                listarWhereGrado = model.Estudiante.Where(x => x.id_grado == _id_grado && x.estado == "OPEN" && x.Grado.estado=="OPEN" && x.Grado.Seccion.estado=="OPEN").ToList();
                return listarWhereGrado;
                

            }catch(Exception){
                return listarWhereGrado;
            }
            
        }

        public List<Model.Estudiante> listEstudiantes()
        {
            List<Model.Estudiante> listaEstudiante = new List<Model.Estudiante>();
            try
            {
                listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();
                
                if (listaEstudiante != null)
                    return listaEstudiante;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Model.Estudiante>();
            }
        }

        public Array cmbListarEstudiante(int _grado)
        {
            Array listaEstudiante = null;
            try
            {
                using(ModeloDB modelo = new ModeloDB()){
                    listaEstudiante = (from e in modelo.Estudiante
                                       where e.estado == "OPEN" && e.id_grado == _grado
                                       orderby e.nombre ascending
                                       select new { e.id_estudiante, valor = e.nombre + " " + e.apellido }).ToArray();

                    return listaEstudiante;
                }
            }
            catch (Exception)
            {
                return listaEstudiante;
            }
        }

        public List<Model.Estudiante> cmbListarEstudiantee(int _grado)
        {
            List<Model.Estudiante> listaEstudiante = new List<Model.Estudiante>();
            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    //listaEstudiante = (from e in modelo.Estudiante
                    //                   where e.estado == "OPEN" && e.id_grado == _grado
                    //                   orderby e.nombre ascending
                    //                   ).ToList();

                    listaEstudiante = model.Estudiante.Where(x => x.estado.Equals("OPEN") && x.id_grado == _grado).ToList();

                    return listaEstudiante;
                }
            }
            catch (Exception)
            {
                return listaEstudiante;
            }
        }

        public bool insertarEstudiante(Model.Estudiante estudiante)
        {
            try
            {
                model.Estudiante.Add(estudiante);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarEstudiante(Model.Estudiante estudiante, int codigo)
        {
            Model.Estudiante estudianteModificar = new Model.Estudiante();
            try
            {
                estudianteModificar = model.Estudiante.FirstOrDefault(x => x.id_estudiante == codigo);

                //Asignar nuevos valores
                estudianteModificar.nombre = estudiante.nombre;
                estudianteModificar.apellido = estudiante.apellido;
                estudianteModificar.direccion = estudiante.direccion;
                estudianteModificar.padreResponsable = estudiante.padreResponsable;
                estudianteModificar.telefono = estudiante.telefono;
                estudianteModificar.estado_promedio = estudiante.estado_promedio;
                estudianteModificar.fecha_creacion = estudiante.fecha_creacion;
                estudianteModificar.estado = estudiante.estado;
                estudianteModificar.id_grado = Convert.ToInt32(estudiante.id_grado);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool eliminarEstudiante(int codigo)
        {
            Model.Estudiante estudianteEliminar = new Model.Estudiante();
            try
            {
                estudianteEliminar = model.Estudiante.FirstOrDefault(x => x.id_estudiante == codigo);

                estudianteEliminar.estado = "LOCKED";

                ///model.Estudiante.Remove(estudianteEliminar);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Model.Estudiante> filtrarEstudiantes(string filtro, string valor)
        {
            
            List<Model.Estudiante> listaEstudiante = new List<Model.Estudiante>();
            try
            {
                if (filtro.Equals("Id de Estudiante"))
                {
                    int id = int.Parse(valor);
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.id_estudiante == id && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Nombres"))
                {
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.nombre.Contains(valor) && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Apellidos"))
                {
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.apellido.Contains(valor) && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Grado y sección"))
                {
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && (x.Grado.grado1 + " " + x.Grado.Seccion.seccion1).Contains(valor) && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Responsable"))
                {
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.padreResponsable.Contains(valor) && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Dirección"))
                {
                    listaEstudiante = model.Estudiante.Where(x => x.estado == "OPEN" && x.direccion.Contains(valor) && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN").ToList();

                    if (listaEstudiante != null)
                        return listaEstudiante;
                    else
                        throw new Exception();
                }
                else
                {
                    return new List<Model.Estudiante>();
                }
            }
            catch (Exception)
            {
                return new List<Model.Estudiante>();
            }
        }
    }
}
