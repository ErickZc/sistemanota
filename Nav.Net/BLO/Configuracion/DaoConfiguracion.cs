﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Configuracion
{
    public class DaoConfiguracion : BaseBlo
    {
        ModeloDB model = new ModeloDB();

        public ConfiguracionSistema configuraciones()
        {
            ConfiguracionSistema configuracion = new ConfiguracionSistema();
            List<ConfiguracionSistema> lista = new List<ConfiguracionSistema>();
            try
            {
                lista = model.ConfiguracionSistema.ToList();

                if (lista != null)
                {
                    foreach (var item in lista)
                    {
                        configuracion.idConfiguracion = item.idConfiguracion;
                        configuracion.institucion = item.institucion;
                        configuracion.fecha_actual = item.fecha_actual;
                        configuracion.fecha_siguiente = item.fecha_siguiente;
                        configuracion.imagen = item.imagen;
                    }
                }
                else
                    return new ConfiguracionSistema();
            }
            catch (Exception)
            {
                return new ConfiguracionSistema();
            }
            return configuracion;
        }
    }
}
