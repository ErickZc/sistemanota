﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Notas
{
    public class DaoPeriodo : BaseBlo
    {
        ModeloDB model = new ModeloDB();
        public List<Periodo> listaPerido()
        {
            List<Periodo> listaPeriodo = new List<Periodo>();

            using (ModeloDB model = new ModeloDB())
            {
                try
                {
                    listaPeriodo = model.Periodo.Where(x => x.estado.Equals("OPEN")).Select(x => x).ToList();

                    if (listaPeriodo != null)
                        return listaPeriodo;
                    else
                        throw new Exception();
                }
                catch (Exception)
                {
                    return new List<Periodo>();
                }
            }
        }

        public bool insertarPeriodo(Periodo admin)
        {
            try
            {
                model.Periodo.Add(admin);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarPeriodo(Periodo docente, int codigo)
        {
            Periodo modificar = new Periodo();
            try
            {
                modificar = model.Periodo.FirstOrDefault(x => x.id_periodo == codigo);

                //Asignar nuevos valores
                modificar.id_periodo = Convert.ToInt32(codigo);
                modificar.periodo1 = docente.periodo1;

                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool eliminarPeriodo(int codigo)
        {
            Periodo eliminar = new Periodo();
            try
            {
                eliminar = model.Periodo.FirstOrDefault(x => x.id_periodo == codigo);

                eliminar.estado = "LOCKED";

                ///model.Periodo.Remove(eliminar);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
