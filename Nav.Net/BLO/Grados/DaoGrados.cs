﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Grados
{
    class DaoGrados : BaseBlo
    {
        ModeloDB model = new ModeloDB();

        public List<Model.Grado> listGrados()
        {
            List<Model.Grado> listGrados = new List<Model.Grado>();
            try
            {
                listGrados = model.Grado.Where(x=> x.estado.Equals("OPEN")).ToList();

                if (listGrados != null)
                    return listGrados;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Model.Grado>();
            }
        }
    }
}
