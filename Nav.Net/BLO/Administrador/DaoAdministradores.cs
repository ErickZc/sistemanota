﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Administrador
{
    public class DaoAdministradores : BaseBlo
    {
        //Referencia a la entidad ModeloDB de la base de datos
        ModeloDB model = new ModeloDB();

        public Administrador_Sistema getAdministrador(string usuario, string contrasena)
        {
            Administrador_Sistema administrador = new Administrador_Sistema();

            try
            {
                administrador = model.Administrador_Sistema.FirstOrDefault(x => x.username.Equals(usuario) && x.estado.Equals("OPEN"));

                if (administrador != null)
                {
                    if (DesEncriptar(administrador.password).Equals(contrasena))
                        return administrador;
                    else
                        throw new Exception();
                }
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new Administrador_Sistema();
            }
        }

        public bool actualizarPassword(string usuario, string contrasena)
        {
            Administrador_Sistema administrador = new Administrador_Sistema();

            try
            {
                administrador = model.Administrador_Sistema.FirstOrDefault(x => x.username.Equals(usuario));

                if (administrador != null)
                {
                    administrador.password = Encriptar(contrasena);
                    model.SaveChanges();

                    return true;
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Administrador_Sistema> getAdministradores()
        {
            List<Administrador_Sistema> administrador = new List<Administrador_Sistema>();

            try
            {
                administrador = model.Administrador_Sistema.ToList();
            }
            catch (Exception)
            {
                return new List<Administrador_Sistema>();
            }
            return administrador;
        }

        public bool insertarAdministrador(Administrador_Sistema admin)
        {
            try
            {
                model.Administrador_Sistema.Add(admin);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarAdministrador(Administrador_Sistema docente, int codigo)
        {
            Administrador_Sistema modificar = new Administrador_Sistema();
            try
            {
                modificar = model.Administrador_Sistema.FirstOrDefault(x => x.id_admin == codigo);

                //Asignar nuevos valores
                modificar.id_admin = Convert.ToInt32(codigo);
                modificar.nombre = docente.nombre;
                modificar.apellido = docente.apellido;
                modificar.username = docente.username;
                modificar.password = docente.password;
                modificar.telefono = docente.telefono;
                modificar.correo = docente.correo;
                modificar.dui = docente.dui;

                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool eliminarAdministrador(int codigo)
        {
            Administrador_Sistema eliminar = new Administrador_Sistema();
            try
            {
                eliminar = model.Administrador_Sistema.FirstOrDefault(x => x.id_admin == codigo);

                eliminar.estado = "LOCKED";

                ///model.Administrador_Sistema.Remove(eliminar);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
