﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SistemaGestionNotas.BLO
{
    /// <summary>
    /// Clase base para editar y reciclar codigo entre todos los blo hereditarios
    /// </summary>
    public class BaseBlo
    {
        /// <summary>
        /// Propiedad para administrar el mecanismo de logueo de informacion y fallas
        /// </summary>
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Permite sereializar un xml a una clase entity
        /// </summary>
        /// <typeparam name="T">Tipo de entidad</typeparam>
        /// <param name="responseXml">String de xml</param>
        /// <param name="tagName">Nombre de la etiquetra que se buscara dentro del xml</param>
        /// <param name="responseClassType">Tipo de entidad a retornar</param>
        /// <returns></returns>
        public T xmlToEntity<T>(string responseXml, string tagName, T ClassType)
        {
            try
            {
                using (var memStream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(responseXml.GetType());
                    serializer.Serialize(memStream, responseXml);
                    memStream.Position = 0;
                }
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXml);
                var elemList = xmlDoc.GetElementsByTagName(tagName);

                for (int i = 0; i < elemList.Count; i++)
                {
                    var v = elemList[i].InnerXml;
                    if (v.Contains(":"))
                        if (ClassType.GetType().GetProperty(v.Split(':')[0]) != null)
                            ClassType.GetType().GetProperty(v.Split(':')[0]).SetValue(ClassType, v.Substring(v.IndexOf(':') + 1));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return ClassType;
        }

        /// <summary>
        /// convierte una lista generica a datatable
        /// </summary>
        /// <typeparam name="T">tipo de parametro generico</typeparam>
        /// <param name="l">la lista generica que se convertira a datatable</param>
        /// <param name="columns">
        /// opcional puede enviarse vacio, las cabezeras de las columnas que devolvera el datatable...
        /// <para/>
        /// ... si y solo si el tamaño del arreglo conincide con el tamaño de las columnas
        /// </param>
        /// <returns>el resultado de la conversion de lista a datatable</returns>
        public System.Data.DataTable toDataTable<T>(List<T> l, string[] columns)
        {
            DataTable dt = new DataTable(typeof(T).Name);
            try
            {
                /// obtenemos todas la propiedades
                PropertyInfo[] p = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                var r = new object[p.Length];

                /// agregamos todas las columnas de la lista
                foreach (PropertyInfo pi in p)
                    dt.Columns.Add(pi.Name, pi.PropertyType);

                foreach (T o in l)
                {
                    /// se prepara cada fila a insertar
                    for (int i = 0; i < p.Length; i++)
                        r[i] = p[i].GetValue(o, null);
                    dt.Rows.Add(r);
                }
                var c = 0;
                if (dt.Columns.Count.Equals(columns.Length))
                    foreach (DataColumn i in dt.Columns)
                    {
                        i.ColumnName = columns[c];
                        c++;
                    }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return dt;
        }

        /// Encripta una cadena
        public string Encriptar(string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        /// Esta función desencripta la cadena que le envíamos en el parámentro de entrada.
        public string DesEncriptar(string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }

        /// <summary>
        /// Permite sereializar un xml a una clase entity
        /// </summary>
        /// <typeparam name="T">Tipo de entidad</typeparam>
        /// <param name="responseXml">String de xml</param>
        /// <param name="tagName">Nombre de la etiquetra que se buscara dentro del xml</param>
        /// <param name="responseClassType">Tipo de entidad a retornar</param>
        /// <returns></returns>
        public ExpandoObject xmlToEntityObject(string responseXml, string tagName)
        {
            try
            {
                var doc = XDocument.Parse(responseXml); //or XDocument.Load(path)
                var jsonText = JsonConvert.SerializeXNode(doc);
                return JsonConvert.DeserializeObject<ExpandoObject>(jsonText);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return new ExpandoObject();
        }

        /// <summary>
        /// Permite sereializar un xml a una clase entity
        /// </summary>
        /// <typeparam name="T">Tipo de entidad</typeparam>
        /// <param name="responseXml">String de xml</param>
        /// <param name="tagName">Nombre de la etiquetra que se buscara dentro del xml</param>
        /// <param name="responseClassType">Tipo de entidad a retornar</param>
        /// <returns></returns>
        public string[] xmlToArrayString(string responseXml)
        {
            try
            {
                using (var memStream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(responseXml.GetType());
                    serializer.Serialize(memStream, responseXml);
                    memStream.Position = 0;
                }
                var xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXml);

                var a = new List<string>();
                foreach (XmlNode i in xmlDoc.GetElementsByTagName("string"))
                    a.Add(i.InnerXml);

                return a.ToArray();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return new string[] { };
        }

        /// <summary>
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Post(string url, string nombreDelMetodo, string data)
        {
            url = url + "/" + nombreDelMetodo;
            try
            {
                /// write response to xml file
                Directory.CreateDirectory(@"C:\temp\requests");
                using (var f = new StreamWriter(@"C:\temp\requests\" + nombreDelMetodo + ".xml"))
                    f.WriteLine(url + "?" + data);

                //Our postvars
                var buffer = System.Text.Encoding.ASCII.GetBytes(Uri.EscapeUriString(data));
                //Initialisation, we use localhost, change if appliable
                var WebReq = (HttpWebRequest)WebRequest.Create(url);
                //Our method is post, otherwise the buffer (postvars) would be useless
                WebReq.Method = "POST";
                //We use form contentType, for the postvars.
                WebReq.ContentType = "application/x-www-form-urlencoded";
                //The length of the buffer (postvars) is used as contentlength.
                WebReq.ContentLength = buffer.Length;
                //We open a stream for writing the postvars
                using (var PostData = WebReq.GetRequestStream())
                {
                    //Now we write, and afterwards, we close. Closing is always important!
                    PostData.Write(buffer, 0, buffer.Length);
                    PostData.Close();
                }
                var r = "";
                //Get the response handle, we have no true response yet!
                using (var WebResp = (HttpWebResponse)WebReq.GetResponse())
                //Now, we read the response (the string), and output it.
                using (var sr = new StreamReader(WebResp.GetResponseStream(), System.Text.Encoding.UTF8))
                    r = sr.ReadToEnd();

                /// write response to xml file
                Directory.CreateDirectory(@"C:\temp\responses");
                using (var f = new StreamWriter(@"C:\temp\responses\" + nombreDelMetodo + ".xml"))
                    f.WriteLine(r);

                return r;
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return string.Empty;
        }
    }
}