﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Docentes
{
    public class DaoDocente : BaseBlo
    {
        //Referencia a la entidad ModeloDB de la base de datos
        ModeloDB model = new ModeloDB();

        public List<Docente> listDocentes()
        {
            List<Docente> listaDocente = new List<Docente>();
            try
            {
                listaDocente = model.Docente.ToList();

                if (listaDocente != null)
                    return listaDocente;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Docente>();
            }
        }

        public bool insertarDocente(Docente docente)
        {
            try
            {
                model.Docente.Add(docente);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarDocente(Docente docente, int codigo)
        {
            Docente docenteModificar = new Docente();
            try
            {
                docenteModificar = model.Docente.FirstOrDefault(x => x.id_docente == codigo);

                //Asignar nuevos valores
                docenteModificar.id_docente = Convert.ToInt32(codigo);
                docenteModificar.nit = docente.nit;
                docenteModificar.nombre = docente.nombre;
                docenteModificar.password = docente.password;
                docenteModificar.id_rol = docente.id_rol;
                docenteModificar.telefono = docente.telefono;
                docenteModificar.usuario = docente.usuario;
                docenteModificar.genero = docente.genero;
                docenteModificar.fecha_creacion = docente.fecha_creacion;
                docenteModificar.estado = docente.estado;
                docenteModificar.dui = docente.dui;
                docenteModificar.Docente_Materia_Grado = docente.Docente_Materia_Grado;
                docenteModificar.Docente_Grado = docente.Docente_Grado;
                docenteModificar.direccion = docente.direccion;
                docenteModificar.correo = docente.correo;
                docenteModificar.apellido = docente.apellido;
                /*docenteModificar.Docente_Grado = docente.Docente_Grado;
                docenteModificar.Docente_Materia_Grado = docente.Docente_Materia_Grado;
                docenteModificar.Rol_Usuario = docente.Rol_Usuario;*/

                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool eliminarDocente(int codigo)
        {
            Docente docenteEliminar = new Docente();
            try
            {
                docenteEliminar = model.Docente.FirstOrDefault(x => x.id_docente == codigo);

                docenteEliminar.estado = "LOCKED";
                //model.Docente.Remove(docenteEliminar);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Docente getDocente(string usuario, string contrasena)
        {
            Docente administrador = new Docente();

            try
            {
                administrador = model.Docente.FirstOrDefault(x => x.usuario.Equals(usuario));

                if (administrador != null)
                {
                    var t = DesEncriptar(administrador.password);
                    if (DesEncriptar(administrador.password).Equals(contrasena))
                        return administrador;
                    else
                        throw new Exception();
                }
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new Docente();
            }
        }

        public bool actualizarPassword(string usuario, string contrasena, int rol)
        {
            Docente docente = new Docente();

            try
            {
                docente = model.Docente.FirstOrDefault(x => x.usuario.Equals(usuario) && x.id_rol == rol && x.estado == "OPEN");

                if (docente != null)
                {
                    docente.password = Encriptar(contrasena);
                    model.SaveChanges();

                    return true;
                }
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return false;
            }
        }


        public Docente getDocenteById(int id)
        {
            Docente administrador = new Docente();

            try
            {
                administrador = model.Docente.FirstOrDefault(x => x.id_docente==id);

                if (administrador != null)
                {
                    return administrador;
                }
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new Docente();
            }
        }

        public Array listDocente() {
            Array lista = null;
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    lista = (from d in model.Docente
                             where d.estado.Equals("OPEN")
                             select new { 
                                d.id_docente, valor = d.nombre + " " + d.apellido
                             }).ToArray();

                    return lista;
                }
            }catch(Exception){
                return lista;
            }
        }
    }
}
