﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Notas
{
    public class DaoNotas : BaseBlo
    {
        //ModeloDB model = new ModeloDB();

        public Array filtrarPor(string columna, string valor, int _periodo, int _materia, int _grado) {
            
            Array lista = null;
            try {
                using (ModeloDB model = new ModeloDB())
                {
                    if (!columna.Equals("Seleccione un filtro"))
                    {
                        if (columna.Equals("nombre"))
                        {
                            lista = (from n in model.Nota
                                        join e in model.Estudiante on n.id_estudiante equals e.id_estudiante
                                        where e.estado == "OPEN"
                                        && n.id_periodo == _periodo
                                        && n.id_materia == _materia
                                        && e.id_grado == _grado
                                        && e.nombre.Contains(valor)
                                        select new
                                        {
                                            n.id_nota,
                                            n.id_estudiante,
                                            e.nombre,
                                            e.apellido,
                                            n.actividad1,
                                            n.actividad2,
                                            n.actividad3,
                                            n.examen,
                                            n.promedio
                                        })
                                          .ToArray();
                            return lista;

                        }
                        else
                        {
                            lista = (from n in model.Nota
                                        join e in model.Estudiante on n.id_estudiante equals e.id_estudiante
                                        where e.estado == "OPEN"
                                        && n.id_periodo == _periodo
                                        && n.id_materia == _materia
                                        && e.id_grado == _grado
                                        && e.apellido.Contains(valor)
                                        select new
                                        {
                                            n.id_nota,
                                            n.id_estudiante,
                                            e.nombre,
                                            e.apellido,
                                            n.actividad1,
                                            n.actividad2,
                                            n.actividad3,
                                            n.examen,
                                            n.promedio
                                        })
                                          .ToArray();
                            
                        }
                        return lista;
                    }
                    else
                    {

                        return lista;
                    }
                }
            }catch(Exception){
                return lista;
            }

        }

        public List<Docente_Materia_Grado> listDocenteMateriaGrado()
        {
            List<Docente_Materia_Grado> listaDocenteMateriaGrado = new List<Docente_Materia_Grado>();

            using(ModeloDB model = new ModeloDB()){
                try
                {
                    listaDocenteMateriaGrado = model.Docente_Materia_Grado.ToList();

                    if (listaDocenteMateriaGrado != null)
                        return listaDocenteMateriaGrado;
                    else
                        throw new Exception();
                }
                catch (Exception)
                {
                    return new List<Docente_Materia_Grado>();
                }
            }
        }

        public void calcularPromedio() { 
        
        }
        public void agregarNota(int _id_estudiante, int _id_materia, double _actividad1, double _actividad2, double _actividad3, double _examen, int _id_periodo)
        {
            
            Nota nota = new Nota();

            try{
               using(ModeloDB modelo = new ModeloDB()){

                   double actividades;
                   double promedio;
                   double actividad1 = double.Parse(_actividad1.ToString());
                   double actividad2 = double.Parse(_actividad2.ToString());
                   double actividad3 = double.Parse(_actividad3.ToString());
                   double examen = double.Parse(_examen.ToString());

                   actividades = ((actividad1 + actividad2 + actividad3)/3) * 0.6 ;
                   promedio = actividades + (examen * 0.4);
                   
                   nota.id_estudiante = _id_estudiante;
                   nota.id_materia = _id_materia;
                   nota.actividad1 = decimal.Parse(_actividad1.ToString());
                   nota.actividad2 = decimal.Parse(_actividad2.ToString());
                   nota.actividad3 = decimal.Parse(_actividad3.ToString());
                   nota.examen = decimal.Parse(_examen.ToString());
                   nota.promedio = decimal.Parse(promedio.ToString());
                   nota.id_periodo = _id_periodo;
                   modelo.Nota.Add(nota);
                   modelo.SaveChanges();

               } 
            }catch(Exception){
            
            }

        }

        public void modificarNota(int _id_nota,int _id_estudiante, int _id_materia, double _actividad1, double _actividad2, double _actividad3, double _examen, int _id_periodo)
        {

            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {

                    Nota nota = modelo.Nota.FirstOrDefault(x => x.id_nota == _id_nota);

                    double actividades;
                    double promedio;
                    double actividad1 = double.Parse(_actividad1.ToString());
                    double actividad2 = double.Parse(_actividad2.ToString());
                    double actividad3 = double.Parse(_actividad3.ToString());
                    double examen = double.Parse(_examen.ToString());

                    actividades = ((actividad1 + actividad2 + actividad3) / 3) * 0.6;
                    promedio = actividades + (examen * 0.4);

                    nota.id_nota = _id_nota;
                    nota.id_estudiante = _id_estudiante;
                    nota.id_materia = _id_materia;
                    nota.actividad1 = decimal.Parse(_actividad1.ToString());
                    nota.actividad2 = decimal.Parse(_actividad2.ToString());
                    nota.actividad3 = decimal.Parse(_actividad3.ToString());
                    nota.examen = decimal.Parse(_examen.ToString());
                    nota.promedio = decimal.Parse(promedio.ToString());
                    nota.id_periodo = _id_periodo;
                    modelo.SaveChanges();

                }
            }
            catch (Exception)
            {

            }

        }

        public void eliminarNota(int _id) {

            try
            {
                using(ModeloDB modelo = new ModeloDB()){
                    Nota nota = modelo.Nota.FirstOrDefault(x => x.id_nota == _id);
                    modelo.Nota.Remove(nota);
                    modelo.SaveChanges();
                }    
            }
            catch (Exception) { 
            
            }
            
        }

        public Array verNotasDocente(int _periodo, int _materia, int _grado) {

            Array load = null;
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    load = (from n in modelo.Nota
                                join e in modelo.Estudiante on n.id_estudiante equals e.id_estudiante
                                where e.estado == "OPEN"
                                && n.id_periodo == _periodo
                                && n.id_materia == _materia
                                && e.id_grado == _grado
                                select new
                                {
                                    n.id_nota,
                                    n.id_estudiante,
                                    e.nombre,
                                    e.apellido,
                                    n.actividad1,
                                    n.actividad2,
                                    n.actividad3,
                                    n.examen,
                                    n.promedio
                                })
                                          .ToArray();
                    return load;
                }
            }catch(Exception){
                return load;
            }

        }

        public List<Model.Nota> ListVerNotasDocente(int _periodo, int _materia, int _grado)
        {
            List<Model.Nota> load = new List<Model.Nota>();
            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    load = modelo.Nota.Where(x => x.Estudiante.estado.Equals("OPEN")
                        && x.id_periodo == _periodo
                        && x.id_materia == _materia
                        && x.Estudiante.id_grado == _grado).ToList();
                    return load;
                }
            }
            catch (Exception)
            {
                return load;
            }
        }

        public List<Model.Nota> listarNotas() {
            List<Model.Nota> lista = new List<Model.Nota>();
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    lista = modelo.Nota.ToList();
                    return lista;
                }
            }catch(Exception){
                return lista;
            }
        }

    }
}
