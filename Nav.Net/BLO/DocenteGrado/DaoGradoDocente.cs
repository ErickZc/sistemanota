﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;
using SistemaGestionNotas.WinForms.Docente;

namespace SistemaGestionNotas.BLO.docente__grado
{

    
    public class DaoGradoDocente : BaseBlo
    {
        ModeloDB model = new ModeloDB();



        public bool insertarDocenteGrado(Docente_Grado docentegrado)
        {
            try
            {
                model.Docente_Grado.Add(docentegrado);
                model.SaveChanges();
                return true; 
            }catch(Exception )
            {
                return false;
            }
        }

        public bool modificarDocenteGrado(Docente_Grado docentegrado, int codigo)
        {
            Docente_Grado modify = new Docente_Grado();
            try
            {
                modify = model.Docente_Grado.FirstOrDefault(x => x.idDocente_Grado == codigo);
                if (modify !=null )
                { 
               
                modify.id_docente = Convert.ToInt32 (docentegrado.id_docente);
                modify.id_grado = Convert.ToInt32(docentegrado.id_grado);
                //modify.fecha = docentegrado.fecha;
                //modify.estado = docentegrado.estado;
                model.SaveChanges();
                return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception)
            {
                return false;
            }
        }


        public bool eliminarDocenteGrado(int codigo)
        {
            Docente_Grado delete = new Docente_Grado();
            try
            {
                delete = model.Docente_Grado.FirstOrDefault(x => x.idDocente_Grado == codigo);
                //model.Docente_Grado.Remove(delete);
                delete.estado = "LOCKED";
                model.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }

        }

        public List<Docente_Grado> listaDocenteGrados()
        {
            List<Docente_Grado> listadocentegrado = new List<Docente_Grado>();
            try
            {
                listadocentegrado = model.Docente_Grado.Where(x => x.estado == "OPEN").Select(x => x).ToList();

                if (listadocentegrado != null)
                    return listadocentegrado;
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return new List<Docente_Grado>();
            }
        }

        public Docente_Grado listaDocenteGradosbyId(int id)
        {
            Docente_Grado listadocentegrado = new Docente_Grado();
            try
            {
                using(ModeloDB mode = new ModeloDB()){
                    listadocentegrado = mode.Docente_Grado.FirstOrDefault(x => x.id_docente == id && x.estado == "OPEN" && x.Grado.estado == "OPEN" && x.Grado.Seccion.estado == "OPEN");

                    if (listadocentegrado != null)
                        return listadocentegrado;
                    else
                        throw new Exception();
                }
            }
            catch (Exception)
            {
                return new Docente_Grado();
            }
        }


    }

     
    
}
