﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Grado
{
    public class DaoGrado
    {

        public Array listGrado() {

            //List<Model.Grado> lista = new List<Model.Grado>();
            Array lista = null;

            try
            {
                using(ModeloDB modelo = new ModeloDB()){
                    lista =
                        (from g in modelo.Grado
                         join s in modelo.Seccion
                             on g.id_seccion equals s.id_seccion
                             where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN")
                         select new { g.id_grado, grado = g.grado1, g.id_seccion, seccion = g.Seccion.seccion1, g.cantidad_alumno }).ToArray();

                    return lista;
                }
            }
            catch(Exception) {
                return lista;
            }
        
        }

        public void agregarGrado(string _grado,int _id_seccion, int _cantidad_alumnos) {
            try
            {
                using(ModeloDB modelo = new ModeloDB()){
                    Model.Grado grado = new Model.Grado();

                    grado.grado1 = _grado;
                    grado.id_seccion = _id_seccion;
                    grado.cantidad_alumno = _cantidad_alumnos;
                    grado.estado = "OPEN";

                    modelo.Grado.Add(grado);
                    modelo.SaveChanges();

                }
            }
            catch (Exception) { 
            
            }
        }

        public void modificarGrado(int _id_grado,string _grado,int _id_seccion, int _cantidad) {
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    Model.Grado grado = modelo.Grado.FirstOrDefault(x => x.id_grado == _id_grado);

                    grado.grado1 = _grado;
                    grado.id_seccion = _id_seccion;
                    grado.cantidad_alumno = _cantidad;
                    grado.estado = "OPEN";

                    modelo.SaveChanges();

                }
            }catch(Exception){
            
            }
        }

        public void eliminarGrado(int _id_grado) {
            try {
                using(ModeloDB modelo = new ModeloDB()){
                    Model.Grado grado = modelo.Grado.FirstOrDefault(x => x.id_grado == _id_grado);

                    grado.estado = "LOCKED";

                    ///modelo.Grado.Remove(grado);
                    modelo.SaveChanges();
                }
            }
            catch (Exception)
            {

            }
        }

        public Array filtrarGrado(string columna, string valor) {

            Array lista = null;

            try
            {
                using(ModeloDB modelo = new ModeloDB()){

                    if(!columna.Equals("Seleccione un filtro")){

                        if(columna.Equals("id grado")){

                            int i = int.Parse(valor);

                            lista =
                                (from g in modelo.Grado
                                    join s in modelo.Seccion
                                    on g.id_seccion equals s.id_seccion
                                    where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN") && g.id_grado == i
                                select new { g.id_grado, grado = g.grado1, g.id_seccion, seccion = g.Seccion.seccion1, g.cantidad_alumno }).ToArray();

                        }else if(columna.Equals("grado")){

                            lista =
                                (from g in modelo.Grado
                                 join s in modelo.Seccion
                                 on g.id_seccion equals s.id_seccion
                                 where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN") && g.grado1.Contains(valor)
                                 select new { g.id_grado, grado = g.grado1, g.id_seccion, seccion = g.Seccion.seccion1, g.cantidad_alumno }).ToArray();

                        }
                        else if (columna.Equals("seccion"))
                        {
                            lista =
                                (from g in modelo.Grado
                                 join s in modelo.Seccion
                                 on g.id_seccion equals s.id_seccion
                                 where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN") && g.Seccion.seccion1 == valor
                                 select new { g.id_grado, grado = g.grado1, g.id_seccion, seccion = g.Seccion.seccion1, g.cantidad_alumno }).ToArray();
                        }
                        else {
                            int i = int.Parse(valor);

                            lista =
                                (from g in modelo.Grado
                                 join s in modelo.Seccion
                                 on g.id_seccion equals s.id_seccion
                                 where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN") && g.cantidad_alumno == i
                                 select new { g.id_grado, grado = g.grado1, g.id_seccion, seccion = g.Seccion.seccion1, g.cantidad_alumno }).ToArray();
                        }
                    }


                    return lista;

                }
            }
            catch (Exception) {
                return lista;
            }
        }

        public List<Model.Grado> listGradoById(int? gradoid)
        {
            ModeloDB modelo = new ModeloDB();
            List<Model.Grado> lista = new List<Model.Grado>();
            try
            {
                lista = modelo.Grado.Where(x => x.id_grado == gradoid && x.estado == "OPEN" && x.Seccion.estado == "OPEN").ToList();
                return lista;
            }
            catch (Exception)
            {
                return lista;
            }

        }

        public List<Model.Grado> listaGrado()
        {
            ModeloDB modelo = new ModeloDB();
            List<Model.Grado> lista = new List<Model.Grado>();
            try
            {
                lista = modelo.Grado.Where(x => x.estado == "OPEN" && x.Seccion.estado == "OPEN").ToList();
                return lista;

            }
            catch (Exception)
            {
                return lista;
            }

        }

        public Array listarGradoSeccion()
        {

            //List<Model.Grado> lista = new List<Model.Grado>();
            Array lista = null;

            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    lista =
                        (from g in modelo.Grado
                         join s in modelo.Seccion
                             on g.id_seccion equals s.id_seccion
                         where g.estado.Equals("OPEN") && g.Seccion.estado.Equals("OPEN")
                         select new { g.id_grado,grado = g.grado1 + " " + g.Seccion.seccion1 }).ToArray();

                    return lista;
                }
            }
            catch (Exception)
            {
                return lista;
            }

        }

        public List<Model.Grado> listGradoo()
        {

            List<Model.Grado> lista = new List<Model.Grado>();

            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    lista = modelo.Grado.Where(x => x.estado.Equals("OPEN")).ToList();
                    return lista;
                }
            }
            catch (Exception)
            {
                return lista;
            }

        }

        public Array cargarComboGrado(int valor, int _id_docente) {
            Array loadGrado = null;

            try {
                using (ModeloDB model = new ModeloDB())
                {
                    loadGrado = (from dmg in model.Docente_Materia_Grado
                                 join g in model.Grado on dmg.id_grado equals g.id_grado
                                 join s in model.Seccion on g.id_seccion equals s.id_seccion
                                 where dmg.id_materia == valor && dmg.id_docente == _id_docente //El id_docente debe cambiarse por medio del login
                                 orderby s.seccion1 ascending
                                 select new { dmg.id_grado, valor = g.grado1 + " | " + s.seccion1 })
                                       .ToArray();

                    return loadGrado;
                }
            }catch(Exception){
                return loadGrado;
            }

        }

    }
}
