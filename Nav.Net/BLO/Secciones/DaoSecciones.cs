﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Secciones
{
    class DaoSecciones
    {
        ModeloDB model = new ModeloDB();
        public List<Model.Seccion> listarSecciones()
        {

            List<Model.Seccion> lista = new List<Model.Seccion>();

            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {

                    lista = modelo.Seccion.ToList().Where(x => x.estado == "OPEN").ToList(); ;
                    return lista;

                }

            }
            catch (Exception)
            {
                return lista;
            }

        }
        public List<Model.Seccion> filtrarSecciones(string filtro, string valor)
        {

            List<Model.Seccion> filtrarSecciones = new List<Model.Seccion>();
            try
            {
                if (filtro.Equals("Id de Sección"))
                {
                    int id = int.Parse(valor);
                    filtrarSecciones = model.Seccion.Where(x => x.estado == "OPEN" && x.id_seccion == id).ToList();

                    if (filtrarSecciones != null)
                        return filtrarSecciones;
                    else
                        throw new Exception();
                }
                else if (filtro.Equals("Sección"))
                {
                    filtrarSecciones = model.Seccion.Where(x => x.estado == "OPEN" && x.seccion1.Equals(valor)).ToList();

                    if (filtrarSecciones != null)
                        return filtrarSecciones;
                    else
                        throw new Exception();
                }
                else
                {
                    return new List<Model.Seccion>();
                }
            }
            catch (Exception)
            {
                return new List<Model.Seccion>();
            }
        }

        public bool insertarSecciones(Model.Seccion seccion)
        {
            try
            {
                model.Seccion.Add(seccion);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarSecciones(Model.Seccion secciones, int codigo)
        {
            Model.Seccion seccModificar = new Model.Seccion();
            try
            {
                seccModificar = model.Seccion.FirstOrDefault(x => x.id_seccion == codigo);

                //Asignar nuevos valores
                seccModificar.seccion1 = secciones.seccion1;
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool eliminarSecciones(int codigo)
        {
            Model.Seccion seccionEliminar = new Model.Seccion();
            try
            {
                seccionEliminar = model.Seccion.FirstOrDefault(x => x.id_seccion == codigo);
                seccionEliminar.estado = "LOCKED";
                //model.Seccion.Remove(seccionEliminar);
                model.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
