﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SistemaGestionNotas.Model;

namespace SistemaGestionNotas.BLO.Materia
{
    public class DaoMateria
    {

        public List<Model.Materia> listarMaterias() {

            List<Model.Materia> lista = new List<Model.Materia>();

            try { 
            
                using(ModeloDB modelo = new ModeloDB()){

                    lista = modelo.Materia.Where(x => x.estado == "OPEN").ToList();
                    return lista;

                }

            }catch(Exception){
                return lista;
            }
            
        }

        public List<Model.Materia> filtrarMaterias(string columna, string valor)
        {

            List<Model.Materia> lista = new List<Model.Materia>();

            try
            {

                using (ModeloDB modelo = new ModeloDB())
                {

                    if(columna.Equals("Nombre de materia")){
                        lista = modelo.Materia.Where(x => x.estado == "OPEN" && x.nombre_materia.Contains(valor)).ToList();
                    }
                    else if (columna.Equals("Id Materia"))
                    {
                        int i = int.Parse(valor);
                        lista = modelo.Materia.Where(x => x.estado == "OPEN" && x.id_materia == i).ToList();
                    }

                    
                    return lista;

                }

            }
            catch (Exception)
            {
                return lista;
            }

        }

        public void agregarMateria(string _nombre_materia) {
            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    Model.Materia materia = new Model.Materia();

                    materia.nombre_materia = _nombre_materia;
                    materia.estado = "OPEN";
                    modelo.Materia.Add(materia);
                    modelo.SaveChanges();

                }
            }catch(Exception){
            
            }
        }

        public void modificarMateria(int _id_materia,string _nombre_materia)
        {
            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    Model.Materia materia = modelo.Materia.FirstOrDefault(x => x.id_materia == _id_materia);

                    materia.id_materia = _id_materia;
                    materia.nombre_materia = _nombre_materia;
                    //materia.estado = _estado;
                    modelo.SaveChanges();

                }
            }
            catch (Exception)
            {

            }
        }

        public void eliminarMateria(int _id_materia)
        {
            try
            {
                using (ModeloDB modelo = new ModeloDB())
                {
                    Model.Materia materia = modelo.Materia.FirstOrDefault(x => x.id_materia == _id_materia);

                    materia.id_materia = _id_materia;
                    materia.estado = "LOCKED";
                    modelo.SaveChanges();

                }
            }
            catch (Exception)
            {

            }
        }

        public Array listarMateriaById(int? _id_docente) {
            Array lista = null;

            try { 
                using(ModeloDB modelo = new ModeloDB()){
                    lista = (from dmg in modelo.Docente_Materia_Grado
                                   join m in modelo.Materia on dmg.id_materia equals m.id_materia
                                   where dmg.id_docente == _id_docente   //El id_docente debe cambiarse por medio del login
                                   select new { dmg.id_materia, m.nombre_materia })
                                       .Distinct().ToArray();

                    return lista;
                }
            }catch(Exception){
                return lista;
            }

        }


    }
}
