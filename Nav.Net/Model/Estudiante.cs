namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estudiante")]
    public partial class Estudiante
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estudiante()
        {
            Nota = new HashSet<Nota>();
        }

        [Key]
        public int id_estudiante { get; set; }

        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(50)]
        public string apellido { get; set; }

        [StringLength(50)]
        public string direccion { get; set; }

        [StringLength(100)]
        public string padreResponsable { get; set; }

        [StringLength(15)]
        public string telefono { get; set; }

        [StringLength(20)]
        public string estado_promedio { get; set; }

        [StringLength(50)]
        public string fecha_creacion { get; set; }

        [StringLength(50)]
        public string estado { get; set; }

        public int? id_grado { get; set; }

        public virtual Grado Grado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota> Nota { get; set; }
    }
}
