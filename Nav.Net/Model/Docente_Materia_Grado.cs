namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Docente_Materia_Grado
    {
        [Key]
        public int idDocente_Materia { get; set; }

        public int? id_docente { get; set; }

        public int? id_materia { get; set; }

        public int? id_grado { get; set; }

        [StringLength(10)]
        public string estado { get; set; }


        [StringLength(50)]
        public string fecha { get; set; }

        public virtual Docente Docente { get; set; }

        public virtual Grado Grado { get; set; }

        public virtual Materia Materia { get; set; }
    }
}
