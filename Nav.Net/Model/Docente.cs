namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Docente")]
    public partial class Docente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Docente()
        {
            Docente_Grado = new HashSet<Docente_Grado>();
            Docente_Materia_Grado = new HashSet<Docente_Materia_Grado>();
        }

        public void setIdDocente(int _id_docente) {
            id_docente = _id_docente;
        }

        public int getIdDocente() {
            return id_docente;
        }

        [Key]
        public int id_docente { get; set; }

        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(50)]
        public string apellido { get; set; }

        [StringLength(100)]
        public string direccion { get; set; }

        [StringLength(15)]
        public string telefono { get; set; }

        [StringLength(15)]
        public string dui { get; set; }

        [StringLength(20)]
        public string nit { get; set; }

        [StringLength(50)]
        public string correo { get; set; }

        [StringLength(50)]
        public string fecha_creacion { get; set; }

        [StringLength(25)]
        public string estado { get; set; }

        [StringLength(50)]
        public string usuario { get; set; }

        [StringLength(150)]
        public string password { get; set; }

        [StringLength(20)]
        public string genero { get; set; }

        public int? id_rol { get; set; }

        public virtual Rol_Usuario Rol_Usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Docente_Grado> Docente_Grado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Docente_Materia_Grado> Docente_Materia_Grado { get; set; }
    }
}
