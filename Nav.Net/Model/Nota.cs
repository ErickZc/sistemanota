namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Nota")]
    public partial class Nota
    {
        [Key]
        public int id_nota { get; set; }

        public int? id_estudiante { get; set; }

        public int? id_materia { get; set; }

        public decimal? actividad1 { get; set; }

        public decimal? actividad2 { get; set; }

        public decimal? actividad3 { get; set; }

        public decimal? examen { get; set; }

        public decimal? promedio { get; set; }

        public int? id_periodo { get; set; }

        public virtual Estudiante Estudiante { get; set; }

        public virtual Materia Materia { get; set; }

        public virtual Periodo Periodo { get; set; }
    }
}
