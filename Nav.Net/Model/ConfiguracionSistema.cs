namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConfiguracionSistema")]
    public partial class ConfiguracionSistema
    {
        [Key]
        public int idConfiguracion { get; set; }

        [StringLength(100)]
        public string institucion { get; set; }

        [StringLength(50)]
        public string director { get; set; }

        [StringLength(50)]
        public string fecha_actual { get; set; }

        [StringLength(50)]
        public string fecha_siguiente { get; set; }

        public string imagen { get; set; }
    }
}
