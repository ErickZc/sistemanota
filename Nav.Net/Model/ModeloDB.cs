namespace SistemaGestionNotas.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModeloDB : DbContext
    {
        public ModeloDB()
            : base("name=ModeloDB")
        {
        }

        public virtual DbSet<Docente> Docente { get; set; }
        public virtual DbSet<Docente_Grado> Docente_Grado { get; set; }
        public virtual DbSet<Docente_Materia_Grado> Docente_Materia_Grado { get; set; }
        public virtual DbSet<Estudiante> Estudiante { get; set; }
        public virtual DbSet<Grado> Grado { get; set; }
        public virtual DbSet<Materia> Materia { get; set; }
        public virtual DbSet<Nota> Nota { get; set; }
        public virtual DbSet<Periodo> Periodo { get; set; }
        public virtual DbSet<Rol_Usuario> Rol_Usuario { get; set; }
        public virtual DbSet<Seccion> Seccion { get; set; }
        public virtual DbSet<Administrador_Sistema> Administrador_Sistema { get; set; }
        public virtual DbSet<ConfiguracionSistema> ConfiguracionSistema { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Docente>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.dui)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.nit)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.fecha_creacion)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.estado)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Docente>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Docente_Grado>()
                .Property(e => e.fecha)
                .IsUnicode(false);

            modelBuilder.Entity<Docente_Materia_Grado>()
                .Property(e => e.fecha)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.padreResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.estado_promedio)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.fecha_creacion)
                .IsUnicode(false);

            modelBuilder.Entity<Estudiante>()
                .Property(e => e.estado)
                .IsUnicode(false);

            modelBuilder.Entity<Grado>()
                .Property(e => e.grado1)
                .IsUnicode(false);

            modelBuilder.Entity<Materia>()
                .Property(e => e.nombre_materia)
                .IsUnicode(false);

            modelBuilder.Entity<Materia>()
                .Property(e => e.estado)
                .IsUnicode(false);

            modelBuilder.Entity<Nota>()
                .Property(e => e.actividad1)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Nota>()
                .Property(e => e.actividad2)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Nota>()
                .Property(e => e.actividad3)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Nota>()
                .Property(e => e.examen)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Nota>()
                .Property(e => e.promedio)
                .HasPrecision(4, 2);

            modelBuilder.Entity<Periodo>()
                .Property(e => e.periodo1)
                .IsUnicode(false);

            modelBuilder.Entity<Rol_Usuario>()
                .Property(e => e.rol)
                .IsUnicode(false);

            modelBuilder.Entity<Seccion>()
                .Property(e => e.seccion1)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<Administrador_Sistema>()
                .Property(e => e.dui)
                .IsUnicode(false);
            modelBuilder.Entity<ConfiguracionSistema>()
                .Property(e => e.institucion)
                .IsUnicode(false);

            modelBuilder.Entity<ConfiguracionSistema>()
                .Property(e => e.director)
                .IsUnicode(false);

            modelBuilder.Entity<ConfiguracionSistema>()
                .Property(e => e.fecha_actual)
                .IsUnicode(false);

            modelBuilder.Entity<ConfiguracionSistema>()
                .Property(e => e.fecha_siguiente)
                .IsUnicode(false);

            modelBuilder.Entity<ConfiguracionSistema>()
                .Property(e => e.imagen)
                .IsUnicode(false);
        }
    }
}
