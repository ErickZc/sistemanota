namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Grado")]
    public partial class Grado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Grado()
        {
            Docente_Grado = new HashSet<Docente_Grado>();
            Docente_Materia_Grado = new HashSet<Docente_Materia_Grado>();
            Estudiante = new HashSet<Estudiante>();
        }

        [Key]
        [Column("id_grado")]
        public int id_grado { get; set; }

        [Column("grado")]
        [StringLength(100)]
        public string grado1 { get; set; }

        public int? id_seccion { get; set; }

        [StringLength(10)]
        public string estado { get; set; }

        public int? cantidad_alumno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Docente_Grado> Docente_Grado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Docente_Materia_Grado> Docente_Materia_Grado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estudiante> Estudiante { get; set; }

        public virtual Seccion Seccion { get; set; }
    }
}
