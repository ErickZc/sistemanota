namespace SistemaGestionNotas.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Administrador_Sistema
    {
        [Key]
        public int id_admin { get; set; }

        [StringLength(60)]
        public string nombre { get; set; }

        [StringLength(60)]
        public string apellido { get; set; }

        [StringLength(50)]
        public string username { get; set; }

        [StringLength(150)]
        public string password { get; set; }

        [StringLength(10)]
        public string telefono { get; set; }

        [StringLength(50)]
        public string correo { get; set; }

        [StringLength(10)]
        public string dui { get; set; }

        [StringLength(10)]
        public string estado { get; set; }
    }
}
