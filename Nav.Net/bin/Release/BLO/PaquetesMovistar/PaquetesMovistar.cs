﻿using Nav.Net.NavNetWs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Nav.Net.BLO.TigoPaquetes
{
    public class PaquetesMovistar : BaseBlo
    {
        public string PAQUETIGORespBLO { get; set; }

        private TigoPaquetesWS.InterfacesWcfSoapClient paquetigows;
        public PaquetesMovistar()
        {
            
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            binding.Name = "EPIN20ServicesHttpSoap12EndpointBinding";
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;


            EndpointAddress ep = new EndpointAddress(urlWS.paquetigoWS);
            paquetigows = new TigoPaquetesWS.InterfacesWcfSoapClient(binding, ep);
            
        }        

        public List<TigoPaquetesWS.ListPaquetesTigoDTO> ListadoPaquetes(string sucursal, string caja, string ticket, int subscriberId, int pin, int tipoConsulta, string parentMenu, string location = "")
        {
            List<TigoPaquetesWS.ListPaquetesTigoDTO> Listado = new List<TigoPaquetesWS.ListPaquetesTigoDTO>();
            try
            {
                TigoPaquetesWS.RespuestaListPaquetesTigoDTO Respuesta = paquetigows.GetPaquetigos(sucursal, caja, ticket, subscriberId, pin, tipoConsulta, parentMenu, location);
                Listado = Respuesta.ListadoDeObjetos.ToList();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return Listado;
        }

        public TigoPaquetesWS.RespuestaPaqueteTigoDTO RespuestaPaquetes(TigoPaquetesWS.SendPaquetesTigoDTO Paquete)
        {
            TigoPaquetesWS.RespuestaPaqueteTigoDTO TigoRespuesta = new TigoPaquetesWS.RespuestaPaqueteTigoDTO();
            try
            {
                //TigoPaquetesWS.SendPaquetesTigoDTO Paquete = new TigoPaquetesWS.SendPaquetesTigoDTO();
                TigoPaquetesWS.RespuestaPaquetesTigoDTO Respuesta = new TigoPaquetesWS.RespuestaPaquetesTigoDTO();
              
                if (VerificarReferenciaWeb(urlWS.paquetigoWS).Equals("00")) {
                    Respuesta = paquetigows.SendPaquetigo(Paquete);
                    if (Respuesta.Resultado)
                    {
                        TigoRespuesta = Respuesta.Objeto;
                        PAQUETIGORespBLO = "1";
                    }
                    else
                    {
                        PAQUETIGORespBLO = "0";
                        throw new Exception("Error: " + Respuesta.Errores);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw new Exception(ex.Message);
            }

            return TigoRespuesta;
        }
    }
}
